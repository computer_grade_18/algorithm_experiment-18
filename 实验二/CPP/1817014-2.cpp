/动态规划 -从上到下 
#include <iostream>
using namespace std;
int m[1000][1000];
void dynamic_top(int v[],int w[],int c,int n)         
{
	int i,j;
	for(i = 0;i <= n;i++)		//置边界条件dp[i][0]=0
		m[i][0] = 0;
	for (j = 0;j <= c;j++)		//置边界条件dp[0][c]=0
		m[0][j] = 0;
	for (i = 1;i <= n;i++)
	{  
		for (j = 1;j <= c;j++)
			if (j < w[i])                         //重量大于 背包容量，直接不放入 
				m[i][j] = m[i-1][j];
			else
				m[i][j] = max(m[i-1][j],m[i-1][j-w[i]]+v[i]);      //重量小于背包容量，m[i-1][j]进行比较m[i-1][j-w[i]]+v[i]选择大的 
	} 
	cout<<m[n][c]<<endl;
}
 void traceback(int w[],int c,int n,int x[])     //物品选择情况 
 {
 	int i=n,r=c;
	while (i>=0)				//判断每个物品
	{
		if (m[i][r] != m[i-1][r]) 
		{  
			x[i] = 1;		//选取物品i		//累计总价值
			r = r - w[i]; 	
		}
		else
			x[i]=0;		//不选取物品i
		i--;
	}
 }
 int main()
 {
 int n,c;
  cout<<"输入物品个数：";
  cin>>n; 
  cout<<"输入背包大小：";
  cin>>c; 
  int v[n+1]={0},w[n+1]={0};
 cout<<"输入物品价值：";
 for(int i=1;i<=n;i++)                
 {
 	cin>>v[i];
 }
  cout<<"输入物品重量："; 
 for(int i=1;i<=n;i++)
 {
 	cin>>w[i];
 }
 int x[n]={0};
 cout<<"最高价值";
dynamic_top(v,w,c,n);           //构建二维数组 
cout<<endl;
for(int i=1;i<=n;i++)                //输出二维数组 
{
	for(int j=0;j<=c;j++)
	{
		cout<<m[i][j]<<" ";
	}
	cout<<endl;
}
cout<<endl;
cout<<"物品的选择情况："<<endl;;
traceback(w,c,n,x);          //选择情况 
cout<<endl; 
for(int i=1;i<=n;i++)               //输出物品的选择情况 
{
		cout<<x[i]<<" ";
}
 }

