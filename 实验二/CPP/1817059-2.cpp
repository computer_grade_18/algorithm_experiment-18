#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int lcs( string &str1,string &str2,vector< vector<int> >& c )
{

    const int len1 = str1.length();
    const int len2 = str2.length();

    for( int i=0;i<=len1;++i )
    {
        for( int j=0;j<=len2;++j )
        {
            if( !i || !j ){
                c[i][j] = 0;
                continue;
            }

            if( str1[i-1] == str2[j-1] )
            {
                c[i][j] = c[i-1][j-1] + 1;
            }
            else
            {
                if( c[i-1][j] > c[i][j-1] )
                {
                    c[i][j] = c[i-1][j];
                }
                else
                {
                    c[i][j] = c[i][j-1];
                }
            }
        }
    }
    return c[len1][len2];   
}

void print_lcs( string &str1,string &str2,vector< vector<int> >& c )
{
    const int len1 = str1.length();
    const int len2 = str2.length();

    for( int i=len1,j=len2;i>=1&&j>=1; )
    {
        if( str1[i-1] == str2[j-1] )
        {
            cout << " " << str1[i-1];
            i--;
            j--;
            continue;
        }

        if( c[i-1][j] > c[i][j-1] )
        {
            i--;
        }
        else
        {
            j--;
        }
    }
}

int main()
{
    string str;
    vector <string> v;

    while( getline(cin,str) &&  !str.empty() )
    {
        v.push_back(str);
    }
    for( vector<string>::iterator iter = v.begin(); iter != v.end(); ++iter)
    {
        string str(*iter);
        reverse(str.begin(),str.end());
        vector< vector<int> > c( (*iter).length() + 1,vector<int>(str.length()+1,0) );
        cout << str.length() - lcs(*iter,str,c) << endl;

        print_lcs(*iter,str,c);
    }
    return 0;
} 

