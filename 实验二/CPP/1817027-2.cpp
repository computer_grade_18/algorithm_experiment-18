#include <iostream>
using namespace std;
int m[10][10];
int p[10]={4,1,4,3,4,5,7,1,3,4};//每个物品价值 
int w[10]={2,4,6,7,8,9,1,2,3,4};//每个物品重量 
void backpack(int n,int c)
{
    for(int i=0; i<10; i++)
    {
        m[0][i]=0;    
        m[i][0]=0;
    }
    for(int i=1; i<=n; i++)
    {
        for(int j=1; j<=c; j++)
        {
            if (j<w[i])
                m[i][j]=m[i-1][j];
            if (j>=w[i])
            {
                if (m[i-1][j]>=m[i-1][j-w[i]]+p[i])
                    m[i][j]=m[i-1][j];
                else
                {
                    m[i][j]=m[i-1][j-w[i]]+p[i];
                }
            }
        }
    }
}
int main()
{
    int n=10;//物品数量 
    int c=15;//背包容量
    backpack(n,c);
    cout<<" 最大价值为 "<<m[n][c];
    return 0;
}

