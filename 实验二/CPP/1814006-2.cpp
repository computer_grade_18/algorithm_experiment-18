#include <iostream>
#include <algorithm>
#include <cstring> 
#include<stdlib.h>
#include <time.h>
#define random(x) (rand()%x)
using namespace std;
const int N=150;
int n,m; 
int dp[N][N], w[N],v[N];
int dp1(int a[],int n,int w[],int v[],int m)
{
	int t=0;
	memset(dp,0,sizeof(dp));
	for(int i=1;i<=n;i++)            //填表法，可以使用两个数组填表更省时间但是不能知道装进包的物品是哪些 
	{
		for(int j=1;j<=m;j++)
		{
			t++;
			if(w[i]>j)  //比较当前背包容量能不能装进该物品的大小 
			{
				dp[i][j]=dp[i-1][j];
			}
			else
			{
				dp[i][j]=max(dp[i-1][j],dp[i-1][j-w[i]]+v[i]);  //在能装进背包的情况下，选择不装进和装进价值更大的那个 
			}
		}
	}
	int count=0,i=n,j=m;
	while(j!=0)
	{
		if(dp[i][j]!=dp[i-1][j])   //与上一行数组相比，如果不同，代表该行是 被装进包的 
		{
		    count++;
			a[count]=i;
			j-=w[i];
			i-=1;
		}
		else
		{
			i=i-1;
		}
	}
	a[0]=count;
	return t;
}

int yz(int i, int j) {
    int res;
    if (i == (n+1)) {
        // 已经没有剩余物品了
        res = 0;
    } else if (j < w[i]) {
        // 无法挑选这个物品
        res = yz(i + 1, j);
    } else {
        // 挑选和不挑选的两种情况都尝试一下
        res = max(yz(i + 1, j), yz(i + 1, j - w[i]) + v[i]);
    }
    return res;
}
int main(){
	cout<<"输入有几个物品：";
	cin>>n;
	if(n==0){
		cout<<endl<<"没有物品可以装入背包";
		return 0;
	} 
    int times,a[n+1],x;
    string k;
	cout<<endl<<"输入每个物品的质量和价值：";
	w[0]=0;v[0]=0;
	for(int i=1;i<=n;i++)
	{
		cin>>w[i]>>v[i];
	}
	cout<<endl<<"输入背包的总量：";
	cin>>m;
	if(m==0){
		cout<<endl<<"背包容量不够，不能装下任一物品";
		return 0; 
	}
	
    time_t start,end;
    double Time;
    start=(double)clock();
	times=dp1(a,n,w,v,m);
	cout<<endl<<"能装下背包的最大值为:"<<dp[n][m];
	cout<<endl<<"物品："; 
	for(int i=1;i<=a[0];i++)
	cout<<a[i]<<" ";
	x=yz(1,m);
	if(x==dp[n][m]) k="yes";
	else k="false";
    cout<<endl<<"验证结果："<<k; 
    end=(double)clock();
    Time=(double)(end-start)/CLK_TCK;
    cout<<endl<<"01背包所用时间:"<<Time<<endl; 
    cout<<"动态规划的比较次数:"<<times; 
}
