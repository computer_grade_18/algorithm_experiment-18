#include <iostream>
#include <vector>
using namespace std;
class Solution {
public:
    int beibao(int w[],int v[], int C,int m) 
	{
		int x[m];
		if(m==0) return 0;
	int dp[m][C];
	for(int i=0;i<=m;i++)
		dp[i][0]=0;
	for(int j=0;j<=C;j++)
		dp[0][j]=0;	
			
		for(int i=1; i<=m; i++)
			for(int j=1; j<=C; j++)
				if(j<w[i-1])
					dp[i][j] = dp[i-1][j];
				else 								
					dp[i][j] = max(dp[i-1][j],dp[i-1][j-w[i-1]]+v[i-1]); 		
		
		for(int i=m,j=C;i>0;i--)
		{
			if(dp[i][j]>dp[i-1][j])
			{
				x[i-1]=1;
				j=j-w[i-1];
			}
			else
			x[i-1]=0;
		}
		cout<<"物品1-"<<m<<"的拿取情况为"<<endl; 
		for(int i=0;i<m;i++)cout<<x[i]<<" ";
		cout<<endl;
		return dp[m][C]; 
    }
};
int main()
{
	int C,i,n,s,y;
	cout<<"背包容量："<<endl; 
	cin>>C;
	cout<<"物品数量"<<endl;
	cin>>n;
	int w[n];
	int v[n];
	
	for(i=0;i<n;i++)
	{
	cout<<"输入物品"<<i+1<<"的物品重量和价值"<<endl;
		cin>>w[i]; 
		cin>>v[i];
	} 
	cout<<"能放入物品产生的最大价值为"<<Solution().beibao(w,v,C,n)<<endl;
	return 0;
}

