#include <iostream>
#include <vector> 
using namespace std;
class longest {
public:
    int longestCommonSubsequence(string s1, string s2) {
        int m = s1.size(), n = s2.size();
        vector<vector<int> > dp(m + 1, vector<int>(n + 1, 0));
        for(int i = 0; i < m; i++){
            for(int j = 0; j < n; j++){
                if(s1[i] == s2[j]) dp[i + 1][j + 1] = dp[i][j] + 1;//当s1[i]==s2[j]s1[i]==s2[j],说明这两个字符是公共的字符，只要考察其子问题，dp[i-1][j-1]dp[i?1][j?1]的长度即可，在此基础上+1,

                else dp[i + 1][j + 1] = max(dp[i][j + 1], dp[i + 1][j]);//不相等就只考虑 子问题 ，取max 
            }
        }
        return dp[m][n];
    }
};
int main()
{
	
	string str1="cdadadb",str2="abd";longest solution;
	cout<<"字符串1:   "<<str1<<endl;
	cout<<"字符串2:   "<<str2<<endl;
	cout<<"最长公共子序列为：";
	cout<<solution.longestCommonSubsequence(str1,str2);
	return 0;
}
