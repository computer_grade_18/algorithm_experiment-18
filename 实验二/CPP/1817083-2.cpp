#include<iostream>
using namespace std;
#define num 1000
int c[num][num];
int b[num][num];
void LCSLength(int m,int n,char x[],char y[]){
    int i,j;
    for(i=1;i<=m;i++){
        c[i][0]=0;
    }
    for(i=1;i<n;i++){
        c[0][i]=0;
    } 
    for(i=1;i<=m;i++){
        for(j=1;j<=n;j++){
            if(x[i]==y[j]){
                c[i][j]=c[i-1][j-1]+1;
                b[i][j]=1;
            }
            else if(c[i-1][j]>=c[i][j-1]){
                    c[i][j]=c[i-1][j];
                    b[i][j]=2; 
                }
            else{
                c[i][j]=c[i][j-1];
                b[i][j]=3;
            }
        }
    }
}
void LCS(int i,int j,char x[])
{

    if(i==0||j==0)
	return;
    if(b[i][j]==1)
    {
    LCS(i-1,j-1,x);
    cout<<x[i];
    }
    else if(b[i][j]==2) 
    LCS(i-1,j,x);
    else
    LCS(i,j-1,x);
}
int main()
{
    char x[num];
    char y[num];
    int m,n;
    cout<<"输入数组x的长度"<<endl; 
    cin>>m;
    cout<<"输入数组x"<<endl; 
    for(int i=1;i<=m;i++){
        cin>>x[i];
    }
    cout<<"输入数组y的长度"<<endl; 
    cin>>n;
    cout<<"输入数组y"<<endl; 
    for(int i=1;i<=n;i++){
        cin>>y[i];
    }
    LCSLength(m,n,x,y);
    cout<<"最长公共子序列长度为"<<c[m][n]<<endl;
    cout<<"最长公共子序列为" ; 
    LCS(m,n,x);
    return 0;
}

