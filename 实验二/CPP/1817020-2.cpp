#include<stdio.h>
#include<iostream>
using namespace std;

void solve(int v[],int w[],int c,int n)//物价v ,物重w，背包容量c，物品个数n
{
	int m[100][100];                    //用一个二维数组来存放结果 
	int i=0,j=0;                        //f[i][j] i->n(物品个数)；j->c(背包容量) 
	for(i=1;i<=n;i++)                   //两层for循环(遍历n，再嵌套遍历c) 
	{
		for(j=1;j<=c;j++)
		{
			m[i][j]=m[i-1][j];
			if(j>=w[i])              //背包剩余容量必须大于物品的重量 
			{
				m[i][j]=max(m[i-1][j],m[i-1][j-w[i]]+v[i]);
			}
		}
	}
	cout<<"背包能装的最大价值是：" << m[i-1][j-1] <<endl;
	/*
	int jMax = min(w[n] - 1,c);
		//处理边界问题 当w[n]-1 > c 时，会导致数组越界 
		
	for (int j = 0; j <= jMax ; j++)	//当 0<=j<w[n]时，m[n,j]=0
		m[n][j] = 0;
		
	for (int j = 0; j <= jMax ; j++)	//当 j >= w[n]时，m[n,j]=v[n]
		m[n][j] = v[n];
	
	for(int i = n - 1 ; i >= 1 ; i--)	//DP
	{
		int jMax = min(w[i] - 1 , c);
		for(int j = 0 ; j <= jMax ; j++){
			//0<=j<w[n]时，m[n,j]=0
			m[i][j] = m[i+1][j];
		
		if(j >= w[n]){
				//m(i,j) = v[n] 当 j >= w[n]
			m[i][j] = max(m[i+1][j],m[i+1][j-w[i]+v[i]]);
		}
	}
	*/

 } 
 
 /* 
void trassback(int m[][11],int w[],int c,int n,int x[])
{
	for(int i = 1;i <= n;i++)
	{
		if(m[i][c] == m[i+1][c])
		x[i] = 0;
 	}
 	else
 	{
 		x[i] = 1;c = c - w[i];
	 }
	 x[n] = (m[n][c] >0?11:0);	//最后一行 
}
*/ 
int main(){
	int n;	//物品的个数
	int c;	//背包的总容量 
	int w[100];	//存放物品的重量 
	int v[100];	// 存放物品的价值 
	cout<<"请输入物品的个数：";
	cin>>n; 
	cout<<endl;
	cout<<"请输入背包的容量：";
    cin>>c;
    cout<<endl;
    cout<<"请依次输入物品的重量：";
    for(int i=0;i<n;i++) cin>>w[i];
    cout<<endl;
    cout<<"请依次输入物品的价值：";
    for(int i=0;i<n;i++) cin>>v[i];
	cout<<endl;
    solve(v,w,c,n);
    return 0;
}

 
 
 /*
 #include<iostream>
using namespace std;
#include <algorithm>
 
int main()
{
	int w[5] = { 0 , 2 , 3 , 4 , 5 };			//商品的体积2、3、4、5
	int v[5] = { 0 , 3 , 4 , 5 , 6 };			//商品的价值3、4、5、6
	int bagV = 8;					        //背包大小
	int dp[5][9] = { { 0 } };			        //动态规划表
 
	for (int i = 1; i <= 4; i++) {
		for (int j = 1; j <= bagV; j++) {
			if (j < w[i])
				dp[i][j] = dp[i - 1][j];
			else
				dp[i][j] = max(dp[i - 1][j], dp[i - 1][j - w[i]] + v[i]);
		}
	}
 
	//动态规划表的输出
	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < 9; j++) {
			cout << dp[i][j] << ' ';
		}
		cout << endl;
	}
 
	return 0;
}
 */
 /*
 #include<iostream>
#include<algorithm>
using namespace std;
int N,W;
const int maxn=105;
int v[maxn],w[maxn];
int rec(int i,int j){//从第i个下标开始，计算剩余j重量怎么挑选商品 
    int ans;
    if(i==N) ans=0;//已经没有商品可以选择 
    else if(j<w[i]) ans=rec(i+1,j);//如果背包容量装不下下标为i的商品 
    else ans=max(rec(i+1,j),rec(i+1,j-w[i])+v[i]);
    return ans;
}
int main(){
    cin>>N>>W;
    for(int i=0;i<N;i++) cin>>w[i];
    for(int i=0;i<N;i++) cin>>v[i];
    int res=rec(0,W);
    cout<<res<<endl;
    return 0;
}
 */
 
