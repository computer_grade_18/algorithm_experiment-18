#include<bits/stdc++.h>
using namespace std;

const int num = 5;
void shuchu(vector<int> tmp, int n)
{
	for(int i = 0; i < n; i++)
	{
		cout<<tmp[i]<<" ";
	}
	cout<<endl;
}
void paixu(vector<int>& tmp1, vector<int>& tmp2, int n)
{
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			if(tmp1[j] > tmp1[j+1])
			{
				tmp2[j+1] = j+2;
				tmp2[j+2] = j+1;
			}
		}
	}
}
int jobScheduling(vector<int>& startTime, vector<int>& endTime, vector<int>& profit)
{
    int n = startTime.size();
    vector<int> job( n+1 );						//job[i]表示i号工作
												//0的位置作为哨兵，应为"第一个"工作之前没有可比较的工作
    for(int i = 0; i < n+1; i++)
	{
		job[i] = i;
		cout<<job[i]<<" ";
	}											//初始化为从0开始连续递增的数列,增量为1
	cout<<endl;
	
	shuchu(startTime, n);
	shuchu(endTime, n);
	shuchu(profit, n);
	
	paixu(endTime, job, n);                        //按照截止时间对工作进行排序
	
	shuchu(startTime, n);
	shuchu(endTime, n);
	shuchu(profit, n);
	
    vector<int> prev( n+1 );                    //prev[i]表示i号工作之前最近能做的工作
    for ( int i = 1; i <= n; i++ )
        for ( int j = i-1; j >= 1; j-- )
            if ( endTime[ job[j]-1 ] <= startTime[ job[i]-1 ] )
            {
                prev[i] = j;
                break;
            }
    vector<int> dp( n+1 );                       //dp[i]表示做包括i号工作之前的所有工作能取得的最大收益
    dp[1] = profit[ job[1]-1 ];
    for ( int i = 1; i <= n; i++ )
        dp[i] = max( dp[ i-1 ], profit[ job[i]-1 ] + dp[ prev[i] ] );
    return dp[n];
}

int main(){
	//int start[num] = {1,2,3,3}, end[num] = {3,4,5,6}, money[num] = {50,10,40,70};
	/*cout<<"请依次输入工作的开始时间（小时即可），用空格隔开，按升序排列："<<endl;
	for(int i = 0; i < num; i++)
	{
		cin>>start[i];
	}
	
	cout<<"请按照相同顺序输入工作的结束时间："<<endl;
	for(int i = 0; i < num; i++)
	{
		cin>>end[i];
	}
	
	cout<<"请按照相同顺序输入每份工作的报酬："<<endl;
	for(int i = 0; i < num; i++)
	{
		cin>>money[i];
	}*/
	int a[]={1,2,3,4,6, 3,5,10,6,9, 20,20,100,70,60};
	vector<int> startTime( a, a+5 );
	vector<int> endTime ( a+6, a+10 );
	vector<int> profit( a+11, a+15 );
	cout<<"最大获利是："<<jobScheduling(startTime, endTime, profit)<<endl;
	return 0;
}

