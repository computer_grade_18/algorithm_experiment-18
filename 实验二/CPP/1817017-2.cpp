#include <iostream>
using namespace std;
int sort(char x[],char y[],char z[],int m,int n)
{
	int i,j,k;
	int L[100][100];
	int S[100][100];
	for(i=0;i<=m;i++)                            //初始化数组L和S 
		for(j=0;j<=n;j++)
		{
			L[i][j]=0;
			S[i][j]=0;
		}
	for(i=1;i<=m;i++)                           //为长度矩阵L和状态矩阵S赋值,L[m][n]为最长公共子序列的长度 
		for(j=1;j<=n;j++)
		{
			if(x[i]==y[j])                        
			{
				L[i][j]=L[i-1][j-1]+1;
				S[i][j]=1;
			}
			else
			{
				if(L[i][j-1]>=L[i-1][j])
				{
					L[i][j]=L[i][j-1];
					S[i][j]=2;
				}
				else
				{
					L[i][j]=L[i-1][j];
					S[i][j]=3;
				}
			}
		}	
	i=m;
	j=n;
	k=L[m][n];
	while(i>0&&j>0)                           //把最长公共子序列储存到数组z中 
	{
		if(S[i][j]==1)
		{
			z[k]=x[i];
			k--;
			i--;
			j--;
		}
		else if(S[i][j]==2)
		{
			j--;
		}
		else i--;
	}
    for(k=1;k<=L[m][n];k++)                     //输出最长公共子序列 
	{
		cout<<z[k]<<' '; 
	} 
	return L[m][n]; 
}
int main()
{
	char x[7]={'\0','a','b','c','b','d','b'} ;
	char y[10]={'\0','a','c','b','b','a','b','d','b','b'};
	char z[100];
	sort(x,y,z,6,9);
	return 0;
} 


 
