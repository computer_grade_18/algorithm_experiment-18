#include"stdio.h"

int n=5;
int w[]={0,3,5,4,1,2};
int v[]={0,12,30,14,20,24};
int x[5];
int V[6][7];
int C=6;
void main(void)
{
	int i,j;
	for(i=0;i<=n;i++)
		V[i][0]=0;

	for(j=0;j<=C;j++)
		V[0][j]=0;

	for(i=1;i<=n;i++)
	{
		for(j=1;j<=C;j++)
		{
			if(j<w[i])
				V[i][j]=V[i-1][j];
			else
			{
				if(V[i-1][j]>V[i-1][j-w[i]]+v[i])
					V[i][j]=V[i-1][j];
				else
					V[i][j]=V[i-1][j-w[i]]+v[i];
			}
		}
	}
	
//以上构造动态规划表


	j=C;
	for(i=n;i>0;i--)
	{
		if(V[i][j]>V[i-1][j])
		{
			x[i]=1;
			j=j-w[i];

		}
		else 
			x[i]=0;
	}

	printf("动态规划表：\n");
	for(i=0;i<6;i++)	
	{
		for(j=0;j<7;j++)
		{
			printf("%8d",V[i][j]);
		}
		printf("\n");
	}
	printf("物品是否装入背包：\n");
	for(i=0;i<6;i++)
		printf("%4d",x[i]);
	printf("\n背包最大值：\n");
	printf("%4d\n",V[n][C]);
}
