#include<iostream> 
using namespace std;
int m[2000][2000];
void sovle(int v[],int w[],int c,int n)             //计算最大价值 
{
  
   int jmax=min(w[n]-1,c);                          //构建二维数组 
   for (int j=0;j<=jmax;j++)        
      m[n][j]=0; 
   for(int j=w[n];j<=c;j++)
      m[n][j]=v[n]; 
    
    for(int i=n-1;i>=1;i--)                       //自底向上 
    { int jmax=min(w[i]-1,c);
	  for(int j=0;j<=jmax;j++)                   //装不下情况,到下个物品,价值和重量不变 
	      m[i][j]=m[i+1][j];                      
	  for(int j=w[i];j<=c;j++)                   //装的下情况,m[i+1][j]和m[i+1][j-w[i]]+v[i]两值比较 
	       m[i][j]=max(m[i+1][j],m[i+1][j-w[i]]+v[i]);
	}
	
	  	cout<<m[1][c]<<endl;
}
void traceback(int w[],int c,int n,int x[])   //最优选择显示 
{
for(int i=1;i<n;i++)                 //从数组右上角开始倒推 
  {
     if(m[i][c]==m[i+1][c])          //如果物品不被选择,则它的容量不变,则m[i][c]和m[i+1][c]两值相等,输出0 
       x[i]=0;
     else
       {                          //反之如果物品被选.m[i][c]和m[i+1][c]两值不相等,并且它的容量要减去该物品容量,输出1 
	   x[i]=1;
	   c=c-w[i];
	   }
x[n]=(m[n][c]>0?1:0);           // 对于最后一行只需要判断最后一行的值是否大于0就行,大于0则为装1,等于0则为不装0 
  }
for(int j=0;j<n;j++)
{
cout<<x[j]<<" ";           //输出最优选择数组 
}
} 
int main()
{
int n,c;
cout<<"输入物品个数：";
cin>>n; 
cout<<endl; 
cout<<"输入背包容量：";
cin>>c;
cout<<endl;
int w[n+1]={0};
int v[n+1]={0};
int x[n]={0};
cout<<"输入背包各物品重量";
for(int i=1;i<=n;i++)
  {
    cin>>w[i];
    cout<<" "; 
  }
cout<<"输入背包各物品价值";
for(int i=1;i<=n;i++)
  {
    cin>>v[i];
    cout<<" "; 
  }
cout<<endl;
cout<<"最大价值为:";
sovle(v,w,c,n);
cout<<"其二维数组为:"<<endl; 
for(int i=1;i<=n;i++)                 
  {
    for(int j=0;j<=c;j++)
      {
         cout<<m[i][j]<<" ";
      }
    cout<<endl;
  }
cout<<"物品最优选择情况："<<endl;
traceback(w,c,n,x);
}
 
 
