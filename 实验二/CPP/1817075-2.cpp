#include<iostream>   
using namespace std;
void solve(int v[],int w[],int c,int n,int m[][11])
{
	int jMax=min(w[n]-1,c);      //防止数组越界 
	for(int j=0;j<=jMax;j++){    //当0=<j<w[n]时,m(n,j)=0 		
	m[n][j]=0;}       
	for(int j=w[n];j<=c;j++){    //当j>=w[n]时,m(n,j)=v(n) 
	   m[n][j]=v[n];}
	for(int i=n-1;i>=1;i--){
	int jMax=min(w[i]-1,c);
	  for(int j=0;j<=jMax;j++)       //m(i,j)=m(i+1,j)   当0=<j<w[i] 
	  {                                            
	   m[i][j]=m[i+1][j];
	  }
	  for(int j=w[i];j<=c;j++){      //m(n,j)=v[n]   当j>=w[n] 
	   m[i][j]=max(m[i+1][j],m[i+1][j-w[i]]+v[i]); 
	  }
	}
   cout<<"可获得的最大容量:"<<m[1][c]<<endl;
   cout<<"二维数组:"<<endl;
     for(int i=1;i<=n;i++){  
  	   for(int j=0;j<=c;j++)  
  	   {  
		 cout<<m[i][j]<<" ";
		 }
		 cout<<endl;
  }
} 
void traceback(int m[][11],int w[],int n,int c,int x[])
{
   for(int i=1;i<n;i++)           
   {                              //判断物品是否被装入  
   	 if(m[i][c]==m[i+1][c])    
   	 x[i]=0;                 
   	 {                           //0表示不装 
		x[i]=1;                  //1表示装 
   	    c=c-w[i];
	 }
   }	
   x[n]=(m[n][c]>0?1:0);          //最后一行 
   cout<<"0代表不放入背包(1放入):"<<endl;
   for(int n=1;n<=5;n++)
   cout<<x[n]<<" ";               //输出装入的情况 
   cout<<endl;
}
int main()
{  
  int n=5;
  int c=10;
  int v[6]={0,6,3,5,4,6};
  int w[6]={0,2,2,6,5,4};
  int m[6][11];
  int x[6];
  solve(v,w,c,n,m);
  traceback(m,w,n,c,x);
  return 0;
 } 
 
