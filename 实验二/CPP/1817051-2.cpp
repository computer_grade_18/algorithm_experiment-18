/*
	实验名称：动态规划应用
	实验要求： 
		1. 认真学习动态规划方法基本思想、方法步骤,练习利用动态规划法分析问题解决问题,加深动态规划类程序的理解。
		2. 阅读经典问题的关键代码段,仔细领会动态规划算法的精要。
		3. 选定实验题目，仔细阅读实验要求，设计好输入输出，按照分治法的思想构思算法，选取合适的存储结构实现应用的操作。
		4. 实验要有详细的测试记录，包括各种可能的测试数据。
	选择问题：0-1背包问题 
*/
#include<bits/stdc++.h>
using namespace std;
class Bag_problem{
	private:
		vector<int> weight;//物品重量 
		vector<int> price;//物品价值 
		int bag_weight;//背包总量 
		int thing_number;//物品数量
		vector<bool> Is_have;//表示该位置物品是否放入 
		int result; 
	public:
		//初始化参数 
		void load(){
			weight={2,2,6,5,4};
			price={6,3,5,4,6};
			bag_weight=10;
			thing_number=weight.size();
			Is_have.resize(weight.size());
		} 
		//参数展示 
		void show(){
			int i,j;
			cout << "bag_weight=" << bag_weight << endl;
			cout << "thing_number=" << thing_number << endl;
			cout << "the result:" << result << endl;
		}
		//使用动态规划解决问题 
		void slove(){
			vector<vector<int>> mid(100,vector<int>(100, 0));//dp数组
			int i,j;
			//动态规划核心，判断选取和不选取的价值差距 
			for(int i=1;i<=thing_number;i++){
				for(int j=1;j<=bag_weight;j++){
					if(j<weight[i-1]){
						mid[i][j]=mid[i-1][j];
					}
					else{
						mid[i][j]=max(mid[i-1][j],mid[i-1][j-weight[i-1]]+price[i-1]);
					}
				}
			}
		//判断是否加入背包 
		for(i=thing_number,j=bag_weight; i > 0; i--)
	    {
	        if(mid[i][j] > mid[i-1][j])
	        {
	            Is_have[i-1] = 1;
	            j = j - weight[i-1];
	        }
	        else
	            Is_have[i] = 0;
	    }
	    result = mid[thing_number][bag_weight];
	}
};
int main(){
	Bag_problem b1;
	b1.load();
	b1.slove();
	b1.show();
	return 0;
} 
