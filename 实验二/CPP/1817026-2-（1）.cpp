#include <iostream>
using namespace std; 
void knacksack(int m[][1000],int n,int c,int w[],int v[])
{
	for(int j=0;j<=c;++j)
		if(j>=w[n]) m[n][j]=v[n]; 
		else m[n][j]=0;
	for(int i=n-1;i>=1;--i) 
		for(int j=0;j<=c;++j) 
		if(j<w[i]) m[i][j]=m[i+1][j];
		else m[i][j]=max(m[i+1][j],m[i+1][j-w[i]]+v[i]);
	
} 

void traceback(int m[][1000],int n,int c,int w[],int v[],int x[])
{
		for(int i=1;i<n;++i)
			if(m[i][c]==m[i+1][c])
				x[i]=0;
			else{
				x[i]=1;
				c=c-w[i]; 
			}
		if(m[n][c]>0) x[n]=1;
		else x[n]=0;
}
int main()
{
	int n;cin>>n;
	int c;cin>>c;
	int m[n+1][1000];
	int w[n+1],v[n+1];
	for(int i=1;i<=n;++i)
		cin>>w[i];     
	for(int j=1;j<=n;++j)
		cin>>v[j]; 
	int x[n+1];
	knacksack(m,n,c,w,v);	
	traceback(m,n,c,w,v,x);
	cout<<"最大价值："+m[1][c]<<endl;
	cout<<"背包放入情况："; 
	for(int i=1;i<=n;++i)
		cout<<x[i]<<' ';
	return 0;
}



