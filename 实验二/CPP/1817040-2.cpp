//0/1背包问题-动态规划（自上而下，最优解在右下角） 
#include <iostream>
using namespace std;
int m[9999][9999];
void findmax(int v[],int w[],int c,int n)   //填二维数组表格    
{
	int i,j;
	for(i = 0;i <= n;i++)		//边界置0
		m[i][0] = 0;
	for (j = 0;j <= c;j++)		//边界置0
		m[0][j] = 0;
	for (i = 1;i <= n;i++)
	{  
		for (j = 1;j <= c;j++)
			if (j < w[i])      //若重量大于背包容量，不放入 
				m[i][j] = m[i-1][j];//重量就等于上一行的重量 
			else
				m[i][j] = max(m[i-1][j],m[i-1][j-w[i]]+v[i]); 
	}           //若重量小于背包容量 将m[i-1][j]进行比较m[i-1][j-w[i]]+v[i]选择价值较大的 
	cout<<m[n][c]<<endl;  //输出最优的价值 
}
 void findwhat(int w[],int c,int n,int x[])     //最优解回溯 
 {
 	int i=n,r=c;                 //r作为临时变量 
	while (i>0)				    //判断每个物品
	{
		if (m[i][r] != m[i-1][r]) 
		{ 
			x[i] = 1;		 //选取物品i，累计总价值		
			r = r - w[i]; 	//剩下背包容量 
		}
		else
			x[i]=0;		 //不选取物品i
		    i--;        //回到上一行 
	}
 }
int main()
 {
 int n,c;
  cout<<"物品个数n：";
  cin>>n; 
  cout<<"背包容量c：";
  cin>>c; 
  int v[n+1]={0},w[n+1]={0};
 cout<<"物品价值v：";
 for(int i=1;i<=n;i++)                
 {
 	cin>>v[i];
 }
  cout<<"物品重量w："; 
 for(int i=1;i<=n;i++)
 {
 	cin>>w[i];
 }
 int x[n]={0};
 cout<<"最优价值为：";
findmax(v,w,c,n);            
cout<<endl;
for(int i=1;i<=n;i++)                 
{
	for(int j=0;j<=c;j++)
	{
		cout<<m[i][j]<<" ";   //输出表格
	}
	cout<<endl;
}
cout<<endl;
cout<<"物品的选择："<<endl;;
findwhat(w,c,n,x);          
cout<<endl; 
for(int i=1;i<=n;i++)              
{
		cout<<x[i]<<" ";     //输出物品的选择，1为选择，0为未选择 
}
 }
