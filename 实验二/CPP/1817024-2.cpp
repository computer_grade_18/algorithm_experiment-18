
#include<stdio.h>
#include<string.h>
#define max(a,b) a>b?a:b
int n;
int k;//前进的步数
int map[105][105];
int ans[105][105];//记忆化搜索，保存中间搜索结果
int search(int x,int y)
{
    int dx,dy;//要去的下一个位置
    int i,maxx = 0;
    if(ans[x][y] != -1) return ans[x][y];//已经搜索过，直接返回结果
    for(i = 1 ; i <= k ; i ++)//向前走的步数
    {
        dx = x - i;//向上
        printf("%d\n",dx);
        if(dx >= 0 && dx < n && map[dx][y] > map[x][y])
        {
            ans[dx][y] = search(dx,y);
            maxx = max(maxx,ans[dx][y]);
        }
        dx = x + i;//向下
        if(dx >= 0 && dx < n && map[dx][y] > map[x][y])
        {
            ans[dx][y] = search(dx,y);
            maxx = max(maxx,ans[dx][y]);
        }
        dy = y - i;//向左
        if(dy >= 0 && dy < n && map[x][dy] > map[x][y])
        {
            ans[x][dy] = search(x,dy);
            maxx = max(maxx,ans[x][dy]);
        }
        dy = y + i;//向右
        if(dy >= 0 && dy < n && map[x][dy] > map[x][y])
        {
            ans[x][dy] = search(x,dy);
            maxx = max(maxx,ans[x][dy]);
        }
    }
    return maxx + map[x][y];
}
int main()
{
    int i,j;
    while(scanf("%d%d",&n,&k) && (n != -1 && k != -1))
    {
        for(i = 0 ; i < n ; i ++)
         for(j = 0 ; j < n ; j ++)
          scanf("%d",&map[i][j]);
        memset(ans,-1,sizeof(ans));
        
        printf("%d\n",search(0,0));
    }
    return 0;
}
