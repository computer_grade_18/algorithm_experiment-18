#include <iostream>
#include <cstdio>
#include <cstring>


using namespace std;
const int maxn = 105;
int v[maxn], w[maxn], dp[maxn];

int main()
{
    int n; cin >> n;
    int W; cin >> W;
    memset(v, 0, sizeof(v));
    memset(w, 0, sizeof(w));
    memset(dp, 0, sizeof(dp));
    for(int i=1; i<=n; i++)
        cin >> w[i] >> v[i];
    for(int i=1; i<=n; i++){
        int ww = W;
        for(; ww>=w[i]; ww--){ //注意是从大到小的顺序
            dp[ww] = max(dp[ww], dp[ww-w[i]]+v[i]);  //递推公式
        }
    }
    cout << dp[W];
    return 0;
}
