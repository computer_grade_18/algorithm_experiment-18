#include<stdio.h>
   #include<string.h>
   #define MINUSINF 0x80000000
   #define MAXN 100
   #define MAXV 1000
   int max(int a,int b)
   {
       return a>b?a:b;
   } 
  //n件物品和一个容量为v的背包。第i件物品的费用是c[i]，价值是w[i]，装满与否要求为full
  //算法1：经典DP二维数组解法，时间复杂度及空间复杂度均为O(nv) 
  int ZeroOnePack1(int n,int v,int c[],int w[],int full)
  {
      int i,j;
      int f[MAXN][MAXV];
      if(full)
      {
          for(i=0;i<=n;i++)
              for(j=0;j<=v;j++) 
                  f[i][j]=MINUSINF;
          f[0][0]=0;
      } 
      else memset(f,0,sizeof(f));
      for(i=1;i<=n;i++)
      {
          for(j=0;j<=v;j++)
          {
              if(j>=c[i])
                  f[i][j]=max(f[i-1][j],f[i-1][j-c[i]]+w[i]);
              else f[i][j]=f[i-1][j];
      }
      if(f[n][v]<0) return -1;
      else return f[n][v];
  }
  //算法2：算法1的一维数组解法，时间复杂度为O(nv)，空间复杂度为O(v)
  int ZeroOnePack2(int n,int v,int c[],int w[],int full)
  {
      int i,j;
      int f[MAXV];
      if(full)
      {
          f[0]=0;
         for(i=1;i<=v;i++) 
              f[i]=MINUSINF;
      }
      else memset(f,0,sizeof(f));
      for(i=1;i<=n;i++)
      {
          for(j=v;j>=0;j--)
          {
              if(j>=c[i])
                  f[j]=max(f[j],f[j-c[i]]+w[i]);
          }
      }
      if(f[v]<0) return -1;
      else return f[v];
  }
  //算法3：算法2的优化，去掉了无必要的判断，时间复杂度为O(nv)，空间复杂度为O(v)
  int ZeroOnePack3(int n,int v,int c[],int w[],int full)
 {
     int i,j;
     int f[MAXV];
    if(full)
    {
        f[0]=0;
        for(i=1;i<=v;i++) 
            f[i]=MINUSINF;
    }
     else memset(f,0,sizeof(f));
     for(i=1;i<=n;i++)
    {
         for(j=v;j>=c[i];j--)
        {
           f[j]=max(f[j],f[j-c[i]]+w[i]);
         }
   }
    if(f[v]<0) return -1;
    else return f[v];
 }
 //算法4：算法3的常数优化，在v较大时优势明显，时间复杂度为O(nv)，空间复杂度为O(v)
 int ZeroOnePack4(int n,int v,int c[],int w[],int full)
 {
    int i,j,sum=0,bound;
     int f[MAXV];
     if(full)
    {
        f[0]=0;
         for(i=1;i<=v;i++) 
           f[i]=MINUSINF;
    }
   else memset(f,0,sizeof(f));
     for(i=1;i<=n;i++) sum+=w[i];
     for(i=1;i<=n;i++)
      {
         if(i>1) sum-=w[i-1];
         bound=max(v-sum,c[i]);
       for(j=v;j>=bound;j--)
          {
             f[j]=max(f[j],f[j-c[i]]+w[i]);
         }
     }
     if(f[v]<0) return -1;
     else return f[v];
 }
 int main()
 {
     int i,j;
     int n,v,c[MAXN],w[MAXN];
     while(scanf("%d %d",&n,&v)!=EOF)
     {
       for(i=1;i<=n;i++) scanf("%d %d",&c[i],&w[i]);
         printf("%d\n",ZeroOnePack1(n,v,c,w,0));
     }
    return 0;
 }
