#include<iostream>
#include<cstdio>
#include<string>
using namespace std;
int dp[100][100];
int main()
{
    string s="ABCDEFGHIJKLMN"; 
    string t="BCDAFEIHGJLMNK"; 
    int len1=s.length();
    int len2=t.length();
    for(int i=1;i<len1;i++)
    {
        for(int j=1;j<len2;j++)
        {
            if(s[i]==t[j])
                dp[i][j]=dp[i-1][j-1]+1;
            else
                dp[i][j]=max(dp[i-1][j],dp[i][j-1]);
        }
    }
    cout<<dp[len1-1][len2-1]<<endl;
    int p=len1-1,q=len2-1;
    while(p>=1&&q>=1)
    {
        if(s[p]==t[q])
        {
            cout<<s[p]<<" ";
            p-=1;
            q-=1;
        }
        else if(dp[p-1][q]>=dp[p][q-1])
        {
            p-=1;
        }
        else
            q-=1;
    }
}
