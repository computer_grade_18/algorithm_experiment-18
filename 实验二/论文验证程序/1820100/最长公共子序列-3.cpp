#include<iostream>
#include<string>
#include<math.h>
#include<vector> 
using namespace std;
class Subsequences {
public:
    int css(string str1, string str2) 
    { 
		if(str1.empty() || str2.empty()) return 0;  //判断两个待比较序列是否为空，若为空，则最大公共子序列为0 
		vector <int> dp(str2.size() + 1,0);		//定义一个 随算法不断更新的一维vector容器 
		for(int i = 0;i < str1.size();i++){
			int k = 0;
			for(int j = 1;j < dp.size();j++){	//每执行过一次，更新后的vector中存放的，都是下一次计算的上一行 
				if(str1[i] == str2[j-1]){		//比较两个字符串中的字符 
					int temp = dp[j];			
					dp[j] = k + 1;
					k = temp;					//始终至k为待求解值的上一行所对应的值 
				}else{
					k = dp[j];
					dp[j] = max(dp[j],dp[j-1]);	//dp[j]表示待求解值的上一行对应的值，dp[j-1]表示待求解值的前一个值，因为此时dp[j-1]已更新为当前求解行的值 
				}
			}
		
		} 
		return dp.back();
	}
};
int main()
{
	Subsequences C;
	string a,b;
	a = "abcdef";
	b = "acef";
	a.append(25000,'a'); 
	b.append(25000,'a');
	int c;
	c = C.css(a,b);
	cout<<"abcdef"<<endl<<"abcf"<<endl<<c<<endl;
	return 0;
}
