#include<iostream>
#include<string>
#include<math.h>
#include<vector> 
using namespace std;
class Subsequences {
public:
    int css(string str1, string str2) 
    { 
		if(str1.empty() || str2.empty()) return 0;
		vector<vector<int> > dp(str1.size() + 1,vector<int>(str2.size() + 1,0));//用vector容器存放二维数组，初试化为0 
    	for(int i = 1;i < dp.size();i++){									//下标从1开始 
    		for(int j = 1;j < dp[0].size();j++){							//下标从1开始 
    			if(str1[i-1] == str2[j-1])
    				{dp[i][j] = dp[i-1][j-1] + 1;}					//str1与str2中的字符相同，则LCS的长度加一 
				else
				{
					dp[i][j] = max(dp[i-1][j],dp[i][j-1]);			//str1与str2中的字符不相同，则判断dp[i-1][j]与dp[i][j-1]表示的两个子序列 
				}												//谁能时最大子序列长度最大，则保留最大值 
			}
    			
		}
		return dp.back().back();					//返回容器最后的值 
	}
};
int main()
{
	Subsequences C;
	string a,b;
	a = "abcdef";
	b = "acef";
	a.append(25000,'a'); 
	b.append(25000,'a');
	int c;
	c = C.css(a,b);
	cout<<"abcdef"<<endl<<"abcf"<<endl<<c<<endl;
	return 0;
}
