#include<iostream>
#include<string>
#include<math.h>
using namespace std;
class Subsequences {
public:
    int css(string str1, string str2) 
    { 
        int length1 = str1.size();
        int length2 = str2.size();
        int dp[length2+1][length1+1];
        int i, j;
        //使两个字符串的下标从1开始 
        for(i = 0; i <= length2 + 1; i++){   //初始化二维数组dp 将二维数组第一行与第一列的值初始化为0 
            dp[i][0] = 0;
        }
        for(i = 0; i <= length1 + 1; i++){
            dp[0][i] = 0;
        }
        for(i = 1; i <= length2 + 1; i++){     //下标从1开始  
            for(j = 1; j <= length1 + 1; j++){
                if(str1[j-1] == str2[i-1]){			//字符串中字符相同则属于LCS 
                    dp[i][j] = dp[i-1][j-1] + 1;  
                }
                else{								
				//字符不相同则判断：str2的前i-1个字符与str1的前j个字符的最长公共子段长度a
				//				    str2的前i个字符与str1的前j-1个字符的最长公共子段长度b
				//					谁能使公共子段更长，则将谁赋值给dp[i][j] 
                    dp[i][j] = max(dp[i-1][j], dp[i][j-1]);
                }
            }
        }
        return dp[length2][length1];
    }
};
int main()
{
	Subsequences C;
	string a,b;
	a = "abcdef";
	b = "acf";
	a.append(25000,'a'); 
	b.append(25000,'a');
	int c;
	c = C.css(a,b);
	cout<<"abcdef"<<endl<<"abcf"<<endl<<c<<endl;
	return 0;
}
