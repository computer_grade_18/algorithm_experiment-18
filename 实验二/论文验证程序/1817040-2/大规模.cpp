#include<iostream>
#include<cstdlib>
#include<math.h>
#include<windows.h>
#include<time.h>
#include <stack>
#include"stdio.h"
#define Maxsize 10000 
#include <queue> 
#include<algorithm>
#include <stdlib.h> 
#define V 201         //物容量大小+1 
#define N2 2001           //物品个数+1 
using namespace std;
int n=N2-1;//物品数量
int c=V-1;//背包容量
int w[N2]={0};		//重量，下标0不用
int v[N2]={0}; 
int bag[N2];
int count1=0;//记录该装载方案的总价值
int best=0;//记录最佳装载方案的总价值
int wei=V-1;
int y[N2];//记录最佳装载的方案
int maxv=-9999;				//存放最大价值,初始为最小值
int bestx[N2];			//存放最优解,全局变量
int total=1;
int m[9999][9999];
void findmax(int v[],int w[],int c,int n)   //填二维数组表格    
{
	int i,j;
	for(i = 0;i <= n;i++)		//边界置0
		m[i][0] = 0;
	for (j = 0;j <= c;j++)		//边界置0
		m[0][j] = 0;
	for (i = 1;i <= n;i++)
	{  
		for (j = 1;j <= c;j++)
			if (j < w[i])      //若重量大于背包容量，不放入 
				m[i][j] = m[i-1][j];//重量就等于上一行的重量 
			else
				m[i][j] = max(m[i-1][j],m[i-1][j-w[i]]+v[i]); 
	}           //若重量小于背包容量 将m[i-1][j]进行比较m[i-1][j-w[i]]+v[i]选择价值较大的 
	cout<<m[n][c]<<endl;  //输出最优的价值 
}
 void findwhat(int w[],int c,int n,int x[])     //最优解回溯 
 {
 	int i=n,r=c;                 //r作为临时变量 
	while (i>0)				    //判断每个物品
	{
		if (m[i][r] != m[i-1][r]) 
		{ 
			x[i] = 1;		 //选取物品i，累计总价值		
			r = r - w[i]; 	//剩下背包容量 
		}
		else
			x[i]=0;		 //不选取物品i
		    i--;        //回到上一行 
	}
 }
class object                                                //分支限界法 
{
	friend int knapsack(int *,int *,int ,int);
public:
	int operator < (object a)const
	{
		return (ratio>a.ratio);
	}
private:
	int ID;
	double ratio;
};

class node
{
	friend class knap;
	friend int knapsack(int *,int *,int ,int);
private:
	node *parent;
	bool lchild;
};

class queueNode
{
	friend class knap;
public:
	friend bool operator < (queueNode a,queueNode b)
	{
		if(a.uprofit<b.uprofit) return true;
		else return false;
	}
private:
	int uprofit;
	int profit;
	int weight;
	int level;
	node *ptr;
};

class knap
{
	friend int knapsack(int *,int *,int ,int);
public:
	int maxKnapsack();
private:
	priority_queue <queueNode> H;
	int bound(int i);
	void addLiveNode(int up,int cp,int cw,bool ch,int level);
	node *E;
	int c;
	int n;
	int w[N2];
	int p[N2];
	int cw;
	int cp;
	int bestx[N2];
};

int knap::bound(int i)
{
	int cleft = c-cw;
	int b = cp;
	while(i<=n&&w[i]<=cleft)
	{
		cleft -= w[i];
		b += p[i];
		i++;
	}
	if(i<=n) b += 1.0*cleft*p[i]/w[i];
	return b;
}

void knap::addLiveNode(int up,int cp,int cw,bool ch,int lev)
{
	node *b = new node;
	b->parent = E;
	b->lchild = ch;
	queueNode N;
	N.uprofit = up;
	N.profit = cp;
	N.weight = cw;
	N.level = lev;
	N.ptr = b;
	H.push(N);
}

int knap::maxKnapsack()
{	
	int i = 1;
	E = 0;
	cw = cp = 0;
	int bestp = 0;
	int up = bound(1);
	while(i!=n+1)
	{
		int wt = cw+w[i];
		if (wt<=c)
		{
			if(cp+p[i]>bestp) bestp = cp+p[i];
			addLiveNode(up,cp+p[i],cw+w[i],true,i+1);
		}
		up = bound(i+1);
		if(up>=bestp)
			addLiveNode(up,cp,cw,false,i+1);
		queueNode N;
		N = H.top();
		H.pop();
		E = N.ptr;
		cw = N.weight;
		cp = N.profit;
		up = N.uprofit;
		i = N.level;
	}
	for(int j = n;j>0;j--)
	{
		bestx[j] = E->lchild;
		E = E->parent;
	}
	return cp;
}

int knapsack(int v[],int w[],int c,int n)    //分支限界法 
{
	int i;
	int bestx[N2];
	object *Q = new object[n];
	for(i=0; i<n; i++)
	{
		Q[i].ID = i+1;
		Q[i].ratio = 1.0*v[i]/w[i];
	}
	stable_sort(Q,Q+n);
	knap K;
	for(i=1; i<=n; i++)
	{
		K.p[i] = v[Q[i-1].ID-1];
		K.w[i] = w[Q[i-1].ID-1];
	}
	K.cp = 0;
	K.cw = 0;
	K.c = c;
	K.n = n;
	int bestp = K.maxKnapsack();
	for(i=1; i<=n; i++)
		bestx[Q[i-1].ID] = K.bestx[i];
	for(i=1; i<=n; i++)
		if(bestx[i]==1) printf("%d ", i);
	printf("\n");
	delete [] Q;
	return bestp;
} 
 
int main(void) 
{   srand((unsigned)time(NULL)); 
    LARGE_INTEGER frequency,start,end;
 QueryPerformanceFrequency(&frequency);
    double  d;
 int x[n]={0};
 for(int i=1;i<n;i++)                    //随机元素1-20 
 {
 	v[i]=(rand() % 19)+1;           
 }
  for(int i=1;i<n;i++)
 {
 	w[i]=(rand() % 19)+1;
 }
  for(int i=0;i<=n;i++)
{
	for(int j=0;j<=c;j++)  //初始化m[][]数组 
	{
		m[i][j]=0;
	}
}
  cout<<"0-1背包-动态规划-从上而下的最高价值："; 
   QueryPerformanceCounter(&start);
 findmax(v,w,c,n);   //动态规划-从上而下 
 findwhat(w,c,n,x);
 QueryPerformanceCounter(&end);
 d = (double)(end.QuadPart- start.QuadPart) /(double)frequency.QuadPart* 1000.0; 
 cout<<"0-1背包-动态规划-从上而下："<<d<<"毫秒"<<endl;
cout << endl << "0-1背包-分支限界法的物品选择情况：" ;
QueryPerformanceCounter(&start);    
cout<<"最终结果"<<knapsack(v,w,c,n)<<endl;          //分支限界法 
QueryPerformanceCounter(&end);          
d = (double)(end.QuadPart- start.QuadPart) /(double)frequency.QuadPart* 1000.0; 
 cout<<"0-1背包-分支限界法："<<d<<"毫秒"<<endl; 
  return 0;
}
