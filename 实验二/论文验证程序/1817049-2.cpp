#include <iostream>
#include <vector> 
using namespace std;
class longest {
public:
    int longestCommonSubsequence(string s1, string s2) {
        int m = s1.size(), n = s2.size();
        vector<vector<int> > dp(m + 1, vector<int>(n + 1, 0));
        for(int i = 0; i < m; i++){
            for(int j = 0; j < n; j++){
                if(s1[i] == s2[j]) dp[i + 1][j + 1] = dp[i][j] + 1;
                else dp[i + 1][j + 1] = max(dp[i][j + 1], dp[i + 1][j]);
            }
        }
        return dp[m][n];
    }
};
int main()
{
	
	string str1="cdadadb",str2="acd";Solution longest;
	cout<<"字符串1:   "<<str1<<endl;
	cout<<"字符串2:   "<<str2<<endl;
	cout<<"最长公共子序列为：";
	cout<<solution.longestCommonSubsequence(str1,str2);
	return 0;
}
