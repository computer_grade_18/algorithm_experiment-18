#include <iostream>
#include <string.h>
#include <algorithm>
using namespace std;

int lcs(string str1,string str2)
{
	int len1=str1.size(),len2=str2.size();
	int len[len1+1][len2+1];
	memset(len,0,sizeof(len));
	for(int i=1;i<=len1;i++)
		for(int j=1;j<=len2;j++)
		if (str1[i-1]==str2[j-1])  
		
		len[i][j]=len[i-1][j-1]+1;
		else
	
			len[i][j]=max(len[i-1][j],len[i][j-1]);
			
	return len[len1][len2];
}

int main()
{
	string str1,str2;
	while(cin>>str1>>str2)
	cout<<lcs(str1,str2)<<endl;
	return 0;
}

