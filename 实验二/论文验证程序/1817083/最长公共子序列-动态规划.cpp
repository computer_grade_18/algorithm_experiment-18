#include<iostream>
using namespace std;
#define num 1000
int c[num][num];
int b[num][num];
void LCSLength(int m,int n,char x[],char y[]){
    int i,j;
    for(i=1;i<=m;i++){//初始化第0行
        c[i][0]=0;
    }
    for(i=1;i<n;i++){ //初始化第0列
        c[0][i]=0;
    } 
    for(i=1;i<=m;i++){//对两个序列的最长公共子序列由哪个情况所得的，用b数组进行标志
        for(j=1;j<=n;j++){
            if(x[i]==y[j]){
                c[i][j]=c[i-1][j-1]+1;
                b[i][j]=1;
            }
            else if(c[i-1][j]>=c[i][j-1]){
                    c[i][j]=c[i-1][j];
                    b[i][j]=2; 
                }
            else{
                c[i][j]=c[i][j-1];
                b[i][j]=3;
            }
        }
    }
}
void LCS(int i,int j,char x[])
{

    if(i==0||j==0)
	{
	return;
	} 
    if(b[i][j]==1)//b[i][j]=1时，搜索方向为（i-1，j-1）
    {
    LCS(i-1,j-1,x);
    cout<<x[i];
    }
    else if(b[i][j]==2) //b[i][j]=2时，搜索方向为（i-1，j）
    LCS(i-1,j,x);
    else//b[i][j]=3时，搜索方向为（i，j-1）
    LCS(i,j-1,x);
}
int main()
{
    char x[num];
    char y[num];
    int m,n;
    cout<<"输入数组x的长度"<<endl; 
    cin>>m;
    if(m!=0)
    {
    cout<<"输入数组x"<<endl; 
    for(int i=1;i<=m;i++){
        cin>>x[i];
    }
    }
    cout<<"输入数组y的长度"<<endl; 
    cin>>n;
    if(n!=0)
    {
    cout<<"输入数组y"<<endl; 
    for(int i=1;i<=n;i++){
        cin>>y[i];
    }
    }
    LCSLength(m,n,x,y);
    cout<<"最长公共子序列长度为"<<c[m][n]<<endl;
    cout<<"最长公共子序列为" ; 
    LCS(m,n,x);
    return 0;
}

