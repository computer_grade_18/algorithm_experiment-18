//分治法-快速排序 
#include<iostream>
#include<cstdlib>
#include<math.h>
#include<windows.h>
#include<time.h>
#include <stack>
#include"stdio.h"
#define Maxsize 100
#include <queue> 
#include<algorithm>
#define V 12 
#define N2 10 
using namespace std;
int n=N2-1;//物品数量
int c=V-1;//背包容量
int w[N2]={0,2,2,6,5,4,0,0,0,0};		//重量，下标0不用
int v[N2]={0,6,3,5,4,6,0,0,0,0};
int X=0,Y=0; 
int bag[N2*10];
int count1=0;//记录该装载方案的总价值
int best=0;//记录最佳装载方案的总价值
int wei=V-1;
int y[5000];//记录最佳装载的方案
int maxv=-9999;				//存放最大价值,初始为最小值
int bestx[N2];			//存放最优解,全局变量
int total=1;
int z=0;
void solve(int v[],int w[],int c,int n,int m[][V]) //从下而上动态 
{ 
	int jmax=min(w[n]-1,c);
	for(int j=0;j<=jmax;j++)
	{
		m[n][j]=0;
	}
	for(int j=w[n];j<=c;j++)
	{
		m[n][j]=v[n];
	}
	for(int i=n-1;i>=1;i--)
	{
		int jmax=min(w[i]-1,c);
		for(int j=0;j<=jmax;j++)
	{
		m[i][j]=m[i+1][j];
	}
	for(int j=w[i];j<=c;j++)
	{
		m[i][j]=max(m[i+1][j],m[i+1][j-w[i]]+v[i]);
	}
	}
	cout<<m[1][c];
 } 
  void traceback1(int m[][V],int w[],int c,int n,int x[])
 {
 	for(int i=1;i<n;i++)
 	{
 		if(m[i][c]==m[i+1][c])
 		x[i]=0;
 		else
 		{
 			x[i]=1;c=c-w[i];
		}
	 }
	 x[n]=(m[n][c]>0?1:0);
 }
 void dynamic_top(int v[],int w[],int c,int n,int m[][V])      //从上而下动态 
{
	int i,j;
	for(i = 0;i <= n;i++)		//置边界条件m[i][0]=0
		m[i][0] = 0;
	for (j = 0;j <= c;j++)		//置边界条件m[0][r]=0
		m[0][j] = 0;
	for (i = 1;i <= n;i++)
	{  
		for (j = 1;j <= c;j++)
			if (j < w[i])
				m[i][j] = m[i-1][j];
			else
				m[i][j] = max(m[i-1][j],m[i-1][j-w[i]]+v[i]);
	}
	cout<<m[n][c]<<endl;
}
void traceback2(int m[][V],int w[],int c,int n,int x[])
 {
 	int i=n,r=c;

	while (i>0)				//判断每个物品
	{
		if (m[i][r] != m[i-1][r]) 
		{  
			x[i] = 1;		//选取物品i		//累计总价值
			r = r - w[i];
		}
		else
			x[i]=0;		//不选取物品i
		i--;
	}
 }
void procedure(int k,int wei,int n)
 {  
     for(int i=0;i<=1;i++){//0为装入1位不装入
           if(wei-w[k]*i>=0){//如果还有剩余空间,实现了剪枝
             bag[k]=i;//记录该物品是否被装入
             count1=count1+v[k]*i;//总价值
             if(k==n && count1>best){//此时装载方案是目前已知的装载方案最优
                 X=0;
                 best=count1;//更新这个最优总价值
                 for(int j=1;j<=n;j++){//记录此时背包装入状态
                     y[j]=bag[j];
                 }
             }
             if(k==n && count1 ==best){//此时装载方案是目前已知的装载方案最优
                 
                 for(int i=1,j=1;i<=n*(X+1);i++,j++)
                 {  Y=0;
                 	if(bag[j]==y[i])
                 	z++;
                 
                 	if(z==n)
                 	{   
                 		Y=1;
                 		break;
					}
					if(i%n==0)
					{
						j=1;
						z=0;
					}	 
				 }
				 if(Y==0)
			 {    X=X+1;
                 for(int j=1;j<=n;j++){//记录此时背包装入状态
                     y[X*n+j]=bag[j];
                 }
             }
         }
            if(k<n){//k==n递归
                 procedure(k+1,wei-w[k]*i,n);
             }
             count1-=v[k]*i;//k<n回溯上一步
           }
     }
 }
int main(void) 
{   
    LARGE_INTEGER frequency,start,end;
 QueryPerformanceFrequency(&frequency);
    double d;
 int m[N2][V]={0};
 int x[n]={0};
 cout<<"0-1背包-动态规划-从下而上的最高价值："; 
 QueryPerformanceCounter(&start);
 solve(v,w,c,n,m);   //动态规划  
 traceback1(m,w,c,n,x);
 QueryPerformanceCounter(&end);
 d = (double)(end.QuadPart- start.QuadPart) /(double)frequency.QuadPart* 1000.0; 
	cout<<endl;
 cout<<"0-1背包-动态规划-从下而上："<<d<<"毫秒"<<endl;
 /*for(int i=1;i<=n;i++)
{
	for(int j=0;j<=c;j++)
	{
		cout<<m[i][j]<<" ";
	}
	cout<<endl;
}*/
  for(int i=1;i<=n;i++)
{

		cout<<x[i]<<" ";
}
  cout<<endl;
  cout<<endl;
  
 
  for(int i=0;i<=n;i++)
{
	for(int j=0;j<=c;j++)  //初始化m[][]数组 
	{
		m[i][j]=0;
	}
}
  cout<<"0-1背包-动态规划-从上而下的最高价值："; 
   QueryPerformanceCounter(&start);
 dynamic_top(v,w,c,n,m);   //动态规划 
 traceback2(m,w,c,n,x);
 QueryPerformanceCounter(&end);
 d = (double)(end.QuadPart- start.QuadPart) /(double)frequency.QuadPart* 1000.0; 
 cout<<"0-1背包-动态规划-从上而下："<<d<<"毫秒"<<endl;
/* for(int i=1;i<=n;i++)
{
	for(int j=0;j<=c;j++)
	{
		cout<<m[i][j]<<" ";
	}
	cout<<endl;
}*/
 for(int i=1;i<=n;i++)
{

		cout<<x[i]<<" ";
}
  cout<<endl;
 

QueryPerformanceCounter(&start);
procedure(0,wei,n);   //回溯法
 QueryPerformanceCounter(&end);
 d = (double)(end.QuadPart- start.QuadPart) /(double)frequency.QuadPart* 1000.0; 
   cout <<"0-1背包-回溯法的最高价值：" ;
   cout << best << endl;       
  cout<<"0-1背包回溯法："<<d<<"毫秒"<<endl;       
  for(int i=1;i<=n*(X/2+1);i++){
        cout<<y[i]<<" ";
        if(i%n==0)
        cout<<endl;
         }
         cout<<endl; 
  return 0;
}
