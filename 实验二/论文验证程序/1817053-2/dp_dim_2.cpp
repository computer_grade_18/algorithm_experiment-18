#include<iostream>
#define MAXN 100

using namespace std;

int dp[2][MAXN] = {0};
int W[MAXN] = {0,3,4,5,6,7}; // 物体重量
int V[MAXN] = {0,10,20,30,40,50}; // 物体价值
int N = 5; // 物体数量
int bag = 20; // 背包容量
int flag = 0;
int main(){
    for(int i = 1; i <= N;i++){ //表示i个物品
        for(int j = 1; j <= bag;j++){ // 表示背包的容量
            if(W[i] > j){ // 放不进去
                dp[!flag][j] = dp[flag][j];
            }else{ // 比较放与不放
                dp[!flag][j] = __max(dp[flag][j - W[i]] + V[i],dp[flag][j]);
            }
        }
        flag = !flag;
    }    
    cout << __max(dp[0][bag],dp[1][bag]);
    return 0;
}