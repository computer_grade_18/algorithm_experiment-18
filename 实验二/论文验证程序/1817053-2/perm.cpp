#include<iostream>
using namespace std;
#define SWAP(a,b) {int t = a;a = b;b = t;}

const int MAXN = 100005;
int data[MAXN] = {1,2,3,4};
int nums = 0;
void perm(int begin,int end){
    if(begin == end){
        ++nums;
        printf("%d,%d,%d,%d\n",data[0],data[1],data[2],data[3]);
    }else{
        for (int i = begin; i <= end; i++)
        {
            /* code */
            SWAP(data[begin],data[i]);
            perm(begin + 1,end);
            SWAP(data[begin],data[i]);
        }
        
    }


}

int main(){
    perm(0,3);
    return 0;
}