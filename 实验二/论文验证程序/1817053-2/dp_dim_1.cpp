#include<iostream>
#define MAXN 100

using namespace std;

int dp[MAXN] = {0};
int W[MAXN] = {0,3,4,5,6,7}; // 物体重量
int V[MAXN] = {0,10,20,30,40,50}; // 物体价值
int N = 5; // 物体数量
int bag = 20; // 背包容量

int main(){
    for(int i = 1; i <= N;i++){ //表示i个物品
        for(int j = bag; j >= 1;j--){ // 表示背包的容量
            if(W[i] > j){ // 放不进去
                continue;
            }else{ // 比较放与不放
                dp[j] = __max(dp[j - W[i]] + V[i],dp[j]);
            }
        }
    }    
    cout << dp[bag];
    return 0;
}
