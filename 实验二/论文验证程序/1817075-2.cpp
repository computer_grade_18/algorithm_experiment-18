#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <ctime>
#include <windows.h>
#include <queue>
#include <time.h>
#include <cmath>
#define eps 1e-7
#define nMax 1000
using namespace std;
int n;        //总共的物品数量
int W;        //背包容量
int cw;        //背包中物品总容量
int cp;    //背包中物品总价值
double bestP;//0-1背包中最大价值
int record[nMax][nMax];    //0-1背包中当前最大值
bool x[nMax];
bool y[nMax];

struct Node
{
    int cw;     //背包体积
    int cv;     //背包总价值
    int cnt;    //背包中试过多少种物品，当cnt==n时为叶子结点
    bool x[nMax]; //选中的物品
    Node()
    {
        memset(x, 0, sizeof(x));
        cnt=1;
        cw=0;
        cv=0;
    }
    double ub;  //启发函数的结果
    friend bool operator< (Node n1, Node n2)    //按启发函数建立大顶堆
    {
        return n1.ub < n2.ub;
    }
}node[nMax], maxn;

struct Item
{
    int w;  //物品的重量
    int v;  //物品的价值
    int i;  //物品的编号
    double p;   //物品的单位价值
}item[nMax];

priority_queue<Node> Q; //优先队列
double Max_ub;
double Min_ub;

int Rand(int x);    //随机数生成函数
void input();   //读入数据
bool cmp1(Item a, Item b);  //按密度排序
bool cmp2(Item a, Item b);  //按体积排序
//动态规划法
void DP();  //动态规划
void get_DP();  //找路径
//回溯法
void BackTrack2(int i);     //回溯
//输出函数
void output_way(bool x[]);
int main()
{
    input();
    LARGE_INTEGER BegainTime ;
    LARGE_INTEGER EndTime ;
    LARGE_INTEGER Frequency ;
    QueryPerformanceFrequency(&Frequency);
    QueryPerformanceCounter(&BegainTime) ;
    DP();
    QueryPerformanceCounter(&EndTime);
    cout<<"动态规划： "<<record[n][W]<<endl;
    cout<<"DP背包选择的物品编号：     "<<endl;
    get_DP();
    output_way(x);
    //输出运行时间（单位：s）
    cout<<"运行时间（单位：s）："<<(double)( EndTime.QuadPart - BegainTime.QuadPart )/ Frequency.QuadPart<<endl<<endl;
    maxn.cv=0;
    QueryPerformanceFrequency(&Frequency);
    QueryPerformanceCounter(&BegainTime) ;
    BackTrack2(1);
    QueryPerformanceCounter(&EndTime);
    cout<<"回溯法："<<bestP<<endl;
    cout<<"回溯法选择的物品编号：  "<<endl;
    output_way(x);
    //输出运行时间（单位：s）
    cout<<"运行时间（单位：s）："<<(double)( EndTime.QuadPart - BegainTime.QuadPart )/ Frequency.QuadPart<<endl<<endl;

    return 0;
}
//随机数，比较函数和读入数据
int Rand(int x)
{
    int t=x,sum=0;
    while (t>0)
    {
        sum+=rand()%t;
        t-=32767;
    }
    return sum;
}
bool cmp1(Item a, Item b)
{
    return a.p>b.p;
}

bool cmp2(Item a, Item b)
{
    return a.w<b.w;
}
void input()
{
    n=40; W=400;
    cout<<"物品数目为："<<n<<endl;
    cout<<"背包容量为："<<W<<endl;
    cout<<"                                "<<endl;
    double Min=1e20;
    double Max=-1;
    for(int i=1; i<=n; i++)
    {
        //每个物品的状态
        item[i].w=Rand(100);
        item[i].v=Rand(1000);
        item[i].p=1.0*item[i].v/item[i].w;
        item[i].i=i;
        Min=min(Min, item[i].p);
        Max=max(Max, item[i].p);
    //启发值的边界
    }
    //for(int i=1; i<=n; i++)
        //cout<<item[i].w<<" "<<item[i].v<<endl;
    Max_ub=W*Max;
    Min_ub=W*Min;
}
//动态规划法
void DP()
{
    for(int i=0; i<=n; i++)
        record[i][0]=0;
    for(int j=0; j<=W; j++)
        record[0][j]=0;
    for(int i=1; i<=n; i++)
        for(int j=1; j<=W; j++)
        {
            if(j<item[i].w)
                record[i][j]=record[i-1][j];
            else
                record[i][j]=max(record[i-1][j], record[i-1][j-item[i].w]+item[i].v);
        }
}
void get_DP()
{
    int j=W;
    for(int i=n; i>0; i--)
    {
        if(record[i][j]>record[i-1][j])
        {
            x[i]=1;
            j=j-item[i].w;
        }
        else
            x[i]=0;
    }
}
//回溯法
void BackTrack2(int i)
{
    if(i>n)
    {
        if(bestP<cp)    //更新
        {
            bestP=cp;
            for(int i=1; i<=n; i++)
                x[i]=y[i];
        }
        return ;
    }

    //cout<<cw<<" "<<item[i].c<<" "<<item[i].w<<" "<<cp<<endl;
    if(W>=cw+item[i].w)   //判断背包是否能放下
    {
        cw=cw+item[i].w;    //搜索
        cp=cp+item[i].v;
        y[item[i].i]=1;
        BackTrack2(i+1);     //左子树
        y[item[i].i]=0;
        cw=cw-item[i].w;    //回溯
        cp=cp-item[i].v;
    }
    BackTrack2(i+1);     //右子树
}
void output_way(bool x[])
{
    for(int i=1; i<=n; i++)
    {
        if(x[i])
            cout<<i<<" ";
    }
    cout<<endl;
}
