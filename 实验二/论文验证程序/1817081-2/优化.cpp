#include <iostream>
#include <iomanip>
#include<algorithm>
#include<queue>
#include<time.h>
#include<math.h>
#include<windows.h>
#define N 10
#define V 50
using namespace std;
int c=V-1,n=N-1;//背包容量 物品数量 
//int w[N]={0,2,2,6,5,4,7,5,14,13,9,6,3,5,3,4,6,10,5,6};
//int v[N]={0,6,3,5,4,6,5,5,5,15,7,5,3,4,5,6,8,11,1,4};
int a[N][V],b[N][V]; 
int CurWeight = 0;  //当前放入背包的物品总重量
int CurValue = 0;   //当前放入背包的物品总价值
int BestValue = 0;  //最优值；当前的最大价值，初始化为0
int BestX[N];       //最优解；BestX[i]=1代表物品i放入背包，0代表不放入
int m[N];
int  w[N]={0},v[N]={0}; 
void way1(int a[][V],int w[],int v[],int c,int n){//自上而下的动态规划 
	int x[N];
	int k=c;
	for(int j=0;j<=c;j++)
	 a[0][j]=0;
	for(int j=0;j<=c;j++){
		for(int i=1;i<=n;i++){
			if(j<w[i]){
				a[i][j]=a[i-1][j];
				continue;
			}
			else if(a[i-1][j-w[i]]+v[i]>a[i-1][j])
				a[i][j]=a[i-1][j-w[i]]+v[i];
				else
				a[i][j]=a[i-1][j];
		}
	}
		for(int i=n;i>=1;i--){
		if(a[i][k]!=a[i-1][k]){
//		cout<<i<<endl;
//		cout<<a[i][k]<<" "<<a[i-1][k]<<endl;
			x[i]=1;
			k=k-w[i];	
		}
		else
		{	
//		cout<<i<<endl;
//		cout<<a[i][k]<<" "<<a[i-1][k]<<endl;
		x[i]=0;
		}
	}
	for(int i=1;i<=n;i++)
	cout<<x[i]<<" ";
	cout<<endl;
	
//	for(int i=1;i<=n;i++){
//		if(x[i]==1)
//		cout<<"选择了物品标号为"<<i<<"的物品"<<endl;
//	}
	cout<<"该背包所能装载的物品的最高价值为"<<a[n][c]<<endl;	
//		for(int i=0;i<N;i++){
//		for(int j=0;j<V;j++)
//		cout<<setw(4)<<a[i][j];
//		cout<<endl;
//	}	
}

void way2(int a[][V],int w[],int v[],int c ,int n){//自下而上的动态规划 
	int x[N];
	int k=c;
	int jmax=min(w[n]-1,n);
	for(int j=0;j<=jmax;j++){
		a[n][j]=0;}
	for(int j=w[n];j<=c;j++){
		a[n][j]=v[n];
	}
	for(int i=n-1;i>=1;i--){
		int jmax=min(w[i]-1,c);
		for(int j=0;j<=jmax;j++){
			a[i][j]=a[i+1][j];
		}
		for(int j=w[i];j<=c;j++){
			a[i][j]=max(a[i+1][j],a[i+1][j-w[i]]+v[i]);
		}
	}
	//cout<<a[1][c];
	for(int i=1;i<n;i++){
		if(a[i][k]==a[i+1][k])
		x[i]=0;
		else{
			x[i]=1;
			k=k-w[i];
		}
		x[n]=(a[n][c]>0?1:0);	
	}
	for(int i=1;i<N;i++)
	cout<<x[i]<<" ";
	cout<<endl;
//	for(int i=1;i<=n;i++){
//		if(x[i]==1)
//		cout<<"选择了物品标号为"<<i<<"的物品"<<endl;
//	}
	cout<<"该背包所能装载的物品的最高价值为"<<a[1][c]<<endl;
//	for(int i=1;i<N;i++){
//		for(int j=0;j<V;j++)
//		cout<<setw(4)<<a[i][j];
//		cout<<endl;
//	}
}

void way3(int t)//回溯法 
{
 //叶子节点，输出结果
	if(t>n)
 	{
//如果找到了一个更优的解
 		if(CurValue>BestValue)
 		{
 //保存更优的值和解
 			BestValue = CurValue;
 			for(int i=1; i<=n; ++i){
 				BestX[i] = m[i];
 				//cout<<"m["<<i<<"]="<<m[i]<<endl;
			 }
 			//cout<<t<<" "<<n<<"keyi"<<endl;
 		}
	}
 else
 	{
 		//cout<<t<<endl;
//遍历当前节点的子节点：0 不放入背包，1放入背包
 		for(int i=0; i<=1; i++)
 		{
 			m[t]=i;
 			if(i==0) //不放入背包
 			{
 			way3(t+1);
 			}
 			else //放入背包
 			{
 				//约束条件：放的下
 				if((CurWeight+w[t])<=c)
 				{	
 				CurWeight += w[t];
 				CurValue += v[t];
				way3(t+1);
 				CurWeight -= w[t];
				CurValue -= v[t];
 				}
 			}
 		}
 	}
}
void output()
{
    cout<<"最优值是:"<<BestValue<<endl;
    for(int i=1;i<=n;i++)
        cout<<BestX[i]<<" ";
    cout<<endl;
 
}

struct Node {
	Node() {
		value = 0;
		weight = 0;
		level = 0;
		parent = 0;
		bound = 0;
	}
	int value;           //搜索到该节点时的价值
	int weight;          //搜索到该节点时的总重量
	float bound;		 //搜索以该节点为根的子树能达到的价值上界
	int level;           //该节点所处层次，从0开始
	struct  Node *parent;//指向父节点
};
 
struct cmp {
	bool operator () (Node *a, Node *b) {
		return a->bound < b->bound;
	}
};
 
struct Item{
	int ItemID;			//物品编号
	int value;			//物品价值
	int weight;			//物品重量
	float ratio;		//价值/重量
};
 
bool ItemCmp(Item item1, Item item2) {
	return item1.ratio > item2.ratio;
}
	int branchAndBound(Item items[], int c);
	float maxBound(Node *node, Item items[], int c);
	int maxSize=0;
	int searchCount=0;
int  way4(){
	int maxValue;
	int w1[n];
	int v1[n];
//	cout << "请输入" << n << "个物品的质量:" << endl;
	for (int i = 0; i <n; i++)
	{
		w1[i]=w[i+1];
	}
//	cout << "请输入" << n << "个物品的价值:" << endl;
	for (int i = 0; i <n; i++)
	{
		v1[i]=v[i+1];
	}
	Item items[n];
	//初始化结构体数组
	for (int i = 0; i <n; i++)
	{
		items[i].ItemID = i;
		items[i].value = v1[i];
		items[i].weight = w1[i];
		items[i].ratio = 1.0*v1[i] / w1[i];
	}
	//按价值率排序
	sort(items , items + n, ItemCmp);
	cout << "选取方案为:" << endl;
	maxValue = branchAndBound(items, c);
	cout << "最大价值为:" << maxValue;
//	cout << "队列中元素最多为：" << maxSize << endl;
//	cout << "搜索次数为：" << searchCount << "" << endl;
	cout<<endl; 
//	getchar();
//	getchar();
//	getchar();
//	delete []w1;
//	delete []v1;	
}
int main(int argc, char* argv[])
{	
	double z;                
	LARGE_INTEGER nFreq;          //时间
	LARGE_INTEGER nBeginTime;
	LARGE_INTEGER nEndTime;
	QueryPerformanceFrequency(&nFreq);
	srand((unsigned)time(NULL));  //产生随机数种子，避免产生的随机数相同
	for(int i=1;i<N;i++)              //初始化数组
	{
		w[i]=rand()%20;
		v[i]=rand()%20; 
	}	
	cout<<"自上而下的动态规划：" <<endl; 
	QueryPerformanceCounter(&nBeginTime);//开始计时
	way1(a,w,v,c,n);
	QueryPerformanceCounter(&nEndTime);//结束计时
	z=(double)(nEndTime.QuadPart-nBeginTime.QuadPart)/(double)nFreq.QuadPart;//计算程序执行时间单位为s
	cout<<"自上而下的动态规划所花费的时间："<<z*1000<<"毫秒"<<endl;
	
	cout<<"自下而上的动态规划：" <<endl;
	QueryPerformanceCounter(&nBeginTime);//开始计时 
	way2(b,w,v,c,n);
	QueryPerformanceCounter(&nEndTime);//结束计时
	z=(double)(nEndTime.QuadPart-nBeginTime.QuadPart)/(double)nFreq.QuadPart;//计算程序执行时间单位为s
	cout<<"自下而上的动态规划所花费的时间："<<z*1000<<"毫秒"<<endl;
	
	cout<<"回溯法：" <<endl; 
	QueryPerformanceCounter(&nBeginTime);//开始计时
	way3(1);
	output();
	QueryPerformanceCounter(&nEndTime);//结束计时
	z=(double)(nEndTime.QuadPart-nBeginTime.QuadPart)/(double)nFreq.QuadPart;//计算程序执行时间单位为s
	cout<<"回溯法所花费的时间："<<z*1000<<"毫秒"<<endl;
	
	cout<<"分支限界法:"<<endl;
	QueryPerformanceCounter(&nBeginTime);//开始计时 
	way4();
	QueryPerformanceCounter(&nEndTime);//结束计时
	z=(double)(nEndTime.QuadPart-nBeginTime.QuadPart)/(double)nFreq.QuadPart;//计算程序执行时间单位为s
	cout<<"分支限界法所花费的时间："<<z*1000<<"毫秒"<<endl;
} 
int branchAndBound(Item items[], int c) {
	int x[n] = { 0 };
	int maxValue;			                              //保存当前搜索到的最大价值
	Node *maxNode = new Node();                           //保存当前最大价值节点（叶节点）
	priority_queue<Node *, vector<Node *>, cmp> maxQueue; //最大价值优先队列
	Node *firstNode, *curNode;
 
	searchCount = 1;
	firstNode = new Node();
	firstNode->bound = maxBound(firstNode,items,c);
	firstNode->parent = NULL;
	maxQueue.push(firstNode);							      //入队第一个结点
	maxValue = 0;
	maxNode = firstNode;
	while (!maxQueue.empty())
	{
		curNode = maxQueue.top(); maxQueue.pop();
		//扩展左孩子结点
		if (curNode->weight + items[curNode->level].weight <= c) {	//最大重量限界
			Node *leftNode = new Node();
			leftNode->value = curNode->value + items[curNode->level].value;
			leftNode->weight = curNode->weight + items[curNode->level].weight;
			leftNode->level = curNode->level + 1;
			leftNode->parent = curNode;
			leftNode->bound = curNode->bound;
			if (leftNode->level<n) {
				maxQueue.push(leftNode);
				searchCount++;
			}
			if (maxValue < leftNode->value) {
				maxValue = leftNode->value;
				maxNode = leftNode;
			}
		}
		//扩展右孩子结点
		if (maxBound(curNode, items, c)>maxValue) {					//最大价值上界限界
			Node *rightNode = new Node();
			rightNode->value = curNode->value;
			rightNode->weight = curNode->weight;
			rightNode->level = curNode->level + 1;
			rightNode->parent = curNode;
			rightNode->bound = maxBound(rightNode,items,c);
			if (rightNode->level < n) {
				maxQueue.push(rightNode);
				searchCount++;
			}
		}
		//跟踪队列大小
		if (maxQueue.size()>maxSize)
			maxSize = maxQueue.size();
	}
	curNode = maxNode;
	while (curNode) {
		int tempValue = curNode->value;
		curNode = curNode->parent;
		if (curNode && curNode->value != tempValue)
			x[items[curNode->level].ItemID] = 1;
	}
	for (int i = 0; i < n; i++) {
		cout << x[i] << " ";
	}
	cout << endl;
	return maxValue;
}

//限界函数
float maxBound(Node *node, Item items[], int c) {			//求以该节点为根的子树能达到的价值上界
	float maxValue;
	int restCapacity;				//扩展到该节点时的剩余容量
	int i;
 
	maxValue = node->value;
	restCapacity = c - node->weight;
	i = node->level;
	while (i<N && restCapacity>items[i].weight) {
		maxValue += items[i].value;
		restCapacity -= items[i].weight;
		i++;
	}
	if (restCapacity != 0) {
		maxValue += restCapacity*items[i].ratio;
	}
	return maxValue;
}


