//0-1背包问题 
#include<iostream>
#include<stdio.h>
using namespace std;
void Knapsack(int n,int c,int *w,int *v)//定义参数：物品个数n，背包容量c，物重w，物价v 
{
	int f[100][100];                    //用一个二维数组来记录 
	int i=0,j=0;                        //f[i][j] i->n(物品个数)；j->c(背包容量) 
	for(i=1;i<=n;i++)                   //两层for循环(遍历n，再嵌套遍历c) 
	{
		for(j=1;j<=c;j++)
		{
			f[i][j]=f[i-1][j];
			if(j>=w[i])              //背包剩余容量必须大于物品的重量 
			{
				f[i][j]=max(f[i-1][j],f[i-1][j-w[i]]+v[i]);
			}
		}
	}
	cout<<"背包能装的最大价值是：" << f[i-1][j-1] <<endl;
}
int main()
{
	int n;                 //物体个数n
    int c;                 //背包容量c
    int w[100]; 
    int v[100];
    cout << "请输入背包容量c："<< endl;
    cin >>c;
    cout <<"请输入物体个数n："<<endl;
    cin >>n;
    for(int i=1;i<=n;i++) 
	{
		cout<<"请输入物品的重量w["<<i<<"]："<<endl;
        cin >> w[i]; 
	}
	for(int i=1;i<=n;i++) 
	{
        cout<<"请输入物品的价值v["<<i<<"]："<<endl;
        cin >> v[i];
	}
	Knapsack(n,c,w,v);
		
}


