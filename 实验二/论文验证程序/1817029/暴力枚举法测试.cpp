#include<stdio.h>
#include<math.h>
#include<algorithm>
#include<windows.h>   
#define Time_init	LARGE_INTEGER lFrequency; \
					QueryPerformanceFrequency(&lFrequency);  
#define Time_begin	LARGE_INTEGER lBeginCount; \
					QueryPerformanceCounter(&lBeginCount);
#define Time_end	LARGE_INTEGER lEndCount; \
					QueryPerformanceCounter(&lEndCount);
#define Time_out	double time = (double)(lEndCount.QuadPart - lBeginCount.QuadPart) / (double)lFrequency.QuadPart; \
					printf("运行时间%lf毫秒\n",time*1000);
using namespace std;
const int maxn=100;   //物品最大件数 
int w[maxn], c[maxn];
int n;                //物品件数n 
//从第i个物品开始挑选总重小于V的部分
int solve(int i,int V)
{
	int res;
	if (i==n)        //已经没有剩余的物品了
		res=0;
	else if(V<w[i])  //该物品重量超过背包容量上限，不选 
		res=solve(i+1,V);
	else
		res=max(solve(i+1,V),solve(i+1,V-w[i])+c[i]);
		//一个物品选还是不选都试一下
	return res;
}
int main()
{    
    Time_init;     
    int V;                 //背包容量上限V 
	scanf("%d%d",&n,&V);   //输入物品件数n及背包容量V
	for(int i=0;i<n;i++)   //输入各物品的重量w[i] 
	scanf("%d",&w[i]);
	for(int i=0;i<n;i++)   //输入各物品的价值c[i] 
	scanf("%d",&c[i]);
	Time_begin;
	printf("%d\n",solve(0,V)); 
	Time_end;  
    Time_out; 
	return 0;
}
