#include<stdio.h>
#include<math.h>
#include<algorithm>
#include<windows.h>   
#define Time_init	LARGE_INTEGER lFrequency; \
					QueryPerformanceFrequency(&lFrequency);  
#define Time_begin	LARGE_INTEGER lBeginCount; \
					QueryPerformanceCounter(&lBeginCount);
#define Time_end	LARGE_INTEGER lEndCount; \
					QueryPerformanceCounter(&lEndCount);
#define Time_out	double time = (double)(lEndCount.QuadPart - lBeginCount.QuadPart) / (double)lFrequency.QuadPart; \
					printf("运行时间%lf毫秒\n",time*1000);
using namespace std;
const int maxn=100;     //物品最大件数 
const int maxv=1000;    //容量的上限 
int w[maxn],c[maxn],dp[maxv];
int main()
{
	Time_init;
	int n,V;                //输入物品件数n及背包容量上限V 
	scanf("%d%d",&n,&V);
	for(int i=1;i<=n;i++)   //输入各物品的重量w[i] 
	scanf("%d",&w[i]);
	for(int i=1;i<=n;i++)   //输入各物品的价值c[i] 
	scanf("%d",&c[i]);
	Time_begin;
	for(int v=0;v<=V;v++)   //边界 
	dp[v]=0;
	for(int i=1;i<=n;i++)
    for(int v=V;v>=w[i];v--)
    dp[v]=max(dp[v],dp[v-w[i]]+c[i]);  //状态转移方程 
    int max=0;
    for(int v=0;v<=V;v++)   //寻找dp[0...V]中最大的即为答案 
    if(dp[v]>max)
    max=dp[v];
    printf("%d\n",max);
    Time_end; 
    Time_out;
    return 0;
} 
