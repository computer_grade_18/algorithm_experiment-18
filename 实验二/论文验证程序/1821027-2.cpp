﻿
#include<iostream>
#include<string>
#include<algorithm>
using namespace std;
int main()
{
	ios::sync_with_stdio(false);
	cin.tie(0);
	int N, cur, pre;
	string s1, s2;
	int dp[2][1001];
	unsigned len1, len2, i, j;
	cin >> N;
	while (N--)
	{
		cin >> s1 >> s2;
		len1 = s1.size();
		len2 = s2.size();
		cur = 1;
		pre = 0;
		for (i = 0; i <= len2; i++)dp[0][i] = 0;
		dp[1][0] = 0;
		for (i = 1; i <= len1; i++)
		{
			for (j = 1; j <= len2; j++)
			{
				if (s1[i - 1] == s2[j - 1])
					dp[cur][j] = dp[pre][j - 1] + 1;
				else
					dp[cur][j] = max(dp[cur][j - 1], dp[pre][j]);
			}
			swap(cur, pre);
		}
		cout << dp[pre][len2] << endl;
	}
	return 0;
}
