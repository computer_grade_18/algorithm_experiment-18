#include <iostream>
using namespace std;
 
/* 0-1背包 输出最优方案  直接根据状态数组算
 * Time Complexity  O(N*V)
 * Space Complexity O(N*V)
 * 设 V <= 200 N <= 10
 * 状态转移方程：f(i,v) = max{ f(i-1,v), f(i-1,v-c[i])+w[i] }
 */
 
int maxValue[11][201]; /* 记录子问题最优解 */
int weight[11];
int value[11];
int V, N;
 
void main()
{
    int i, j;
    scanf("%d %d",&V, &N);
    for(i = 0; i < N; ++i)
    {
        scanf("%d %d",&weight[i],&value[i]);
    }
    for(i = 0; i < N; ++i)
    {
        for(j = 0; j <= V; ++j)
        {
            if(i > 0)
            {
                maxValue[i][j] = maxValue[i-1][j];
                if(j >= weight[i])
                {
                    int tmp = maxValue[i-1][j-weight[i]] + value[i];
                    maxValue[i][j] = ( tmp > maxValue[i][j]) ? tmp : maxValue[i][j];
                }
            }else
            {
                if(j >= weight[0])
                    maxValue[0][j] = value[0];
            }
        }
    }
 
    printf("%d\n",maxValue[N-1][V]);
 
    i = N-1;
    j = V;
    while(i >= 0)
    {
        if(maxValue[i][j] == maxValue[i-1][j-weight[i]] + value[i])
        {
            printf("%d ",i);
            j = j - weight[i];
        }
        --i;
    }
}
