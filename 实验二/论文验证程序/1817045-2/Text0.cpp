#include <iostream>
#include <math.h>
using namespace std;
/**以下变量级数组均为全局变量**/
struct good
{
	int		weight; /* 货物重量 */
	int		value;  /* 货物价值 */
	unsigned	flag;   /* 货物在装包组合中的标识码，一般是2的i次方，i为货物编号. */
} goods[1000];                   /* 货物件数预置100件的量 */

int	    sum_v;          /* 货物总价值 */
int		sum_w;          /* 货物总重量 */
int		goodNum;        /* 货物总数量 */
int		PackCapacity;   /* 背包总容量 */
unsigned	ans;            /* 装包货物的组合编码，由货物标识码组合 */

void PackingProcess()
{
	int	num = pow( 2, goodNum );
	int	s_value, s_weight = 0;
	sum_v= 0;
	sum_w= 0;
	for ( int i = 1; i < num; i++ )
	{
		s_value	= 0;
		s_weight= 0;
		for ( int j = 0; j < goodNum; j++ )
		{
			if ( i & goods[j].flag )
			{
				s_value	+= goods[j].value;
				s_weight+= goods[j].weight;
			}
		}
		if ( s_value > sum_v && s_weight <= PackCapacity )
		{
			sum_v	= s_value;
			ans	= i;
		}
	}
};
int main()
{
	int x = 1;  

	cout<<"输入背包容量"<<endl; 
	 cin>>PackCapacity;  
	cout<<"输入物品数量"<<endl;             
	cin >> goodNum;                       
	for ( int i = 0; i < goodNum; i++ )
	{
	    cout<<"输入物品"<<i+1<<"的物品重量和价值"<<endl;                       
		cin >> goods[i].weight ;
		cin>>goods[i].value;      
		goods[i].flag = x;
		x = x * 2;                                   
	}
	PackingProcess();
	sum_v = 0, sum_w = 0;
	for ( int i = 0; i < goodNum; i++ )
	{
		if ( goods[i].flag & ans )
		{
			//cout << i + 1 << " ";
			sum_v	= sum_v + goods[i].value;
			sum_w	= sum_w + goods[i].weight;
		}
	}
	cout << endl << sum_v <<endl;
	return(0);
}

