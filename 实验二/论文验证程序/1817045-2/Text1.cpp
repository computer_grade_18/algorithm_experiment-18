#include <iostream>
#include <vector>
#include "time.h"
#define M 1024
using namespace std;
class Solution {
public:
    int beibao(int w[],int v[], int C,int m) {
		if(m==0) return 0;
		vector<vector<int> > dp(m,vector<int>(C+1,0));
		for(int j=0; j<=C; j++)
			dp[0][j]=(j>=w[0]?v[0]:0);
			
		for(int i=1; i<m; i++)
		{
			for(int j=0; j<=C; j++)
			{
				dp[i][j] = dp[i-1][j];
				if(j>=w[i]) 
				{
					dp[i][j] = max(dp[i-1][j],dp[i-1][j-w[i]]+v[i]); 
				} 
			}
		} 
		return dp[m-1][C]; 
    }
};
int main()
{
	double star,finish;

	int C,i,n,s,y;
	freopen("data.txt","r",stdin);
	cout<<"背包容量："<<endl; 
	cin>>C;
	cout<<"物品数量"<<endl;
	cin>>n;
	int w[n];
	int v[n];
	for(i=0;i<n;i++)
	{
	cout<<"输入物品"<<i+1<<"的物品重量和价值"<<endl;
		cin>>w[i]; 
		cin>>v[i];
	} 
	star=(double)clock();
	cout<<"能放入物品产生的最大价值为"<<Solution().beibao(w,v,C,n)<<endl;
	for(int j=0;j<M;j++)finish=(double)clock();
		printf("The time is:%.2fms\n",(finish-star));
	return 0;

}
