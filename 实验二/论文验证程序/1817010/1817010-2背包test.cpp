#include<iostream>
#include<algorithm>
#include<stdlib.h>
#include<stdio.h>
#include<time.h>
#include<windows.h>
using namespace std;
int w[505],v[505],n;
int c;
int m[505][505];
int p[500],P[500];//回溯法中分别记录当前解和最优解的状态数组 
int Value=0;//回溯中的最大值 
bool legal(int t)
{
	int sum=0;
	for(int i=1;i<=t;i++)
	sum+=w[i]*p[i];
	if(sum>c)
	return false;
	else
	return true;
}
void backTrack(int t)
{
	if(t>n)
	{
		int i;
		int value=0;
		for(i=1;i<=n;i++)
		{
			value+=v[i]*p[i];
		}
		if(value>Value)
		{
			Value=value;
			for(int i;i<=n;i++)
			P[i]=p[i];
		}
	}
	else
	{for(int i=1;i>=0;i--)
	{
		p[t]=i;
		if(legal(t))
		backTrack(t+1);
	}
	}
 } 
int Back()
{backTrack(1);
return Value;
}
int force()//蛮力 
{int maxvalue=0;
int m=1<<n;
int S[32],T[32];
for(int i=1;i<m;i++)
{
	int t=1;
	int temp=i;
	while(temp)
	{T[t++]=temp%2;
	temp=temp/2;
	}
	int j;
	int weight=0,value=0;
	for(j=1;j<t;j++)
	{
		if(T[j]==1&&weight+w[j]<=c)
		{
			weight=weight+w[j];
			value=value+v[j];
		}
	}
	if(maxvalue<value)
	{
		maxvalue=value;
		int k=0;
		for(j=0;j<t;j++)
		{
			S[k++]=T[j];
		}
	}
}
return maxvalue;
}
int solve()//dp 
{
n=4;
c=10;
	int jmax=min(w[n]-1,c);
	for(int j=0;j<=jmax;j++)
	{m[n][j]=0;
	}
	for(int j=w[n];j<=c;j++)
	{m[n][j]=v[n];
	}
	for(int i=n-1;i>=1;i--)
	{int jmax=min(w[i]-1,c);
	for(int j=0;j<=jmax;j++)
	{m[i][j]=m[i+1][j];
	}
	for(int j=w[i];j<=c;j++)
	{m[i][j]=max(m[i+1][j],m[i+1][j-w[i]]+v[i]);
	}
	}
	return m[1][c];
	/*cout<<"背包最大价值为："<<m[1][c]<<endl;
	
	for(int i=1;i<=n;i++)
	{for(int j=0;j<=c;j++)
	{cout<<m[i][j]<<" ";
	}
	cout<<endl; 
	} 	*/
}

void fun(int w[],int c,int n,int x[])
{cout<<"物品装入情况（1表示装，0表示未装）：";
	for(int i=1;i<n;i++)
	{
		if(m[i][c]==m[i+1][c])
		{x[i]=0;
		}
		else
		{x[i]=1;
		c=c-w[i];
		}
		cout<<x[i]<<" ";
	}
	x[n]=(m[n][c]>0?1:0);
	cout<<x[n];
}
int Random()
{return (rand()%(1000)+ 1);
}
void text(int (*matter)())
{
LARGE_INTEGER litmp;
LONGLONG start,over;
double dfMinus,dfFreq,dfTim;
QueryPerformanceFrequency(&litmp);
dfFreq=(double)litmp.QuadPart;
QueryPerformanceCounter(&litmp);
start=litmp.QuadPart;
for(int k=0;k<100;k++)
{
	c=0;
	n=Random()%30;
	for(int i=1;i<=n;i++)
	{
		w[i]=Random();
		v[i]=Random();
		c=c+w[i];
	}
	c=c/2;
	matter();
}
	QueryPerformanceCounter(&litmp);
    over=litmp.QuadPart;
    dfMinus=(double)(over -start);
    dfTim=dfMinus/ dfFreq/ 100;
    cout<<"平均时间T(k): ";
    printf("%e\t",dfTim);
    cout<<endl; 
}
int main()
{/*int n,c,r;
cout<<"请输入物品件数：" ;
cin>>n;
cout<<"请输入背包总容量：";
cin>>c;
int x[n];
	int m[max_i][max_j];
	int w[n+1]={0};
	int v[n+1]={0};
cout<<"请输入每件物品重量："; 
for(int i=1;i<=n;i++)
{cin>>w[i];
}
cout<<"请输入每件物品价值："; 
for(int j=1;j<=n;j++)
{
cin>>v[j];
}*/
n=4;
c=10;
int x[n+1];
w[1]=7;w[2]=3;w[3]=4;w[4]=5;
v[1]=42;v[2]=12;v[3]=40;v[4]=25;
cout<<"蛮力法："<<force()<<endl;
cout<<"动态规划:"<<solve()<<endl;
cout<<"回溯法："<<Back()<<endl;
return 0;
}
