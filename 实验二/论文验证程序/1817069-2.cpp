#include <iostream> 
#include <algorithm>
#include <queue>
#include <algorithm>
using namespace std;
#define MAXN 50
//问题表示
int n=3,W=30;
vector<int> w;//={0,16,15,15};		//重量，下标0不用
vector<int> v;//={0,45,25,25};  	//价值，下标0不用
//求解结果表示
int maxv=-9999;				//存放最大价值,初始为最小值
int bestx[MAXN];			//存放最优解,全局变量
int total=1;				//解空间中结点数累计,全局变量
// #  分支限界优先队列法
//  使用n,W,w[],v[],maxv,bestv[]
//  队列中的节点类型
struct NodeType
{//  分支限界节点
	int no;					//  节点编号
	int	i;				//  当前节点在搜索空间的层次
	int w;					//	当前节点的总重量
	int v;					//	当前节点的总价值
	int x[MAXN];			        //	当前节点包含的解向量
	double ub;				//	上界
	bool operator<(const NodeType& node) const
	{
		return ub < node.ub;	//  ub越大越优先出队
	}
};
/*  主干
 *  ->初始化根节点
 *  ->计算根节点上界及进队
 *  ->循环遍历队列，条件为非空：出一个节点,
       计算左孩子节点剪枝条件，满足的左孩子计算上界及进队；
       计算右孩子节点上界，符合上界条件的右孩子进队；	
	   （根据容量剪去不满足要求的左孩子，根据上界条件剪去不满足要求的右孩子）
 *
*/
void bfs();
//  进队----不是叶子节点就直接进队，是叶子节点则判断是否更优解，是的话则更新最优解
void EnQueue(NodeType e,priority_queue<NodeType> &qu);
//  计算边界 就是根据剩余容量的大小，计算剩下全部物品装入的价值和装入部分物品的价值
//  （部分物品按照单位容量内价值高低的顺序装入---这有点贪心的思想了
void bound(NodeType &e);
void bfs_back_main();
void dfs_back(int i,int tw,int tv,int rw,int op[]);
// !# 回溯法

// # 贪心法----非0/1背包问题，而是部分背包问题
//  使用n,W,w[],v[]

struct NodeType_Knap
{  
	double w;
	double v;
	double p;					//p=v/w
	bool operator<(const NodeType_Knap &s) const
	{
		return p>s.p;			//按p递减排序
	}
};
vector<NodeType_Knap> A;
double V = 0;
double x[MAXN];	
/*
 * 求单位重量的价值->按照自定义的格式排序->调用 Knap
*/
void knap_m();
/*
 * 排序后则贪心循环选择，如果剩余的容量还能容纳当前的，则放进去，不能的话跳出循环，选择部分放入
*/
void Knap();
// # 斐波那契数列
int countf = 1;
int Fib(int n);
int dp_fib[MAXN];				//所有元素初始化为0
int Fib1(int n);
// !# 斐波那契数列
// # 动态规划法
//  使用n,W,w[],v[],maxv,bestv[]
//  动态规划数组
int dp[MAXN][MAXN];
void dp_Knap();
void buildx();

int main()
{
	//  输入格式
	/*
    	3      n个物品假设为3
		16 45  第一个物品的重量和价值
		15 25  第二个物品的重量和价值
		15 25  第三个物品的重量和价值
		30	   背包容量W
	*/
	cin >> n;
	int m,l;
	//  下表0不用，填充0
	w.push_back(0);
	v.push_back(0);
	for (int j = 1; j <= n;j++)
	{
		cin >> m >> l;
		w.push_back(m);
		v.push_back(l);
	}
		cin >> W; 				
	dp_Knap();
	buildx();
	cout << "最优解：";
		for (int i = 1;i <= n;i++)
			{		if (V > 0)	
				{// 贪心法   输出的是double类型 			
				cout << x[i] << " ";		}
		else		
		    {//  分支限界和回溯法输出的是int型		
				cout << bestx[i] << " ";	
				}		
			}
	return 0;
}
void bfs()
{	
int j;	
NodeType e,e1,e2;				//定义3个结点	
priority_queue<NodeType> qu;	                //定义一个优先队列（大根堆）	
e.i=0;						//根结点置初值，其层次计为0	
e.w=0; e.v=0;	e.no=total++; 	
for (j=1;j<=n;j++)	
	e.x[j]=0;	
	bound(e);					//求根结点的上界	
	qu.push(e);		
	while (!qu.empty())				//队不空循环
	{  		e=qu.top(); 
		qu.pop();		        //出队结点e		
		if (e.w+w[e.i+1]<=W)		        //剪枝：检查左孩子结点		
		{  		
			e1.no = total++; 			
			e1.i = e.i + 1;			//建立左孩子结点			
			e1.w = e.w + w[e1.i];		
			e1.v = e.v + v[e1.i];			
			for (j=1;j<=n;j++) 
			e1.x[j]=e.x[j]; 	//复制解向量			
			e1.x[e1.i]=1;			
			bound(e1);				//求左孩子结点的上界			
			EnQueue(e1,qu);			//左孩子结点进队操作	
		}
		e2.no = total++;			//建立右孩子结点	
		e2.i = e.i + 1;		
		e2.w = e.w; 		
		e2.v = e.v;		
		for (j=1;j<=n;j++) 			
		e2.x[j]=e.x[j]; 		//复制解向量		
		e2.x[e2.i]=0;		bound(e2);				//求右孩子结点的上界		
		if (e2.ub>maxv)				//若右孩子结点剪枝			
		EnQueue(e2,qu);	
	}	
}
void EnQueue(NodeType e,priority_queue<NodeType> &qu)
{  //结点e进队qu	
if (e.i==n)					//到达叶子结点	
{  		if (e.v>maxv)				//找到更大价值的解	
	{  			maxv=e.v;			
				for (int j=1;j<=n;j++)		
					bestx[j]=e.x[j];		
		}	
	}	
		else qu.push(e);				//非叶子结点进队
}
void bound(NodeType &e)				        //计算分枝结点e的上界
{  	int i=e.i+1;					//考虑结点e的余下物品	
int sumw=e.w;					//求已装入的总重量	
double sumv=e.v;				//求已装入的总价值	
while (i<=n && (sumw+w[i]<=W) )	
{  		sumw+=w[i];					//计算背包已装入载重	
	sumv+=v[i];					//计算背包已装入价值	
	i++;
	}
		if (i<=n)						//余下物品只能部分装入	
		e.ub=sumv+(W-sumw)*v[i]/w[i];
		else							//余下物品全部可以装入	
		e.ub=sumv;
}
void bfs_back_main()
{	int *op  = new int[n];
	for (int j = 0;j < n;j++)
		{//  初始化为全0	
			op[j] = 0;	}	//  所有物品的总容量
				int rw = 0;	
				for (int j = 0;j < n;j++)
					{		
						rw += w[j];
					}
	dfs_back(1,0,0,rw,op);
}
void dfs_back(int i,int tw,int tv,int rw,int op[])
{  //初始调用时rw为所有物品重量和	
int j;	
if (i>n)				        //找到一个叶子结点
	{  		if (tw==W && tv>maxv) 		        //找到一个满足条件的更优解,保存	
		{ 			maxv=tv;			
		for (j=1;j<=n;j++)		//复制最优解			
			bestx[j]=op[j];	
				}
			}
				else
					{				                //尚未找完所有物品	
						if (tw+w[i]<=W)			        //左孩子结点剪枝	
							{  			op[i]=1;			//选取第i个物品		
								dfs_back(i+1,tw+w[i],tv+v[i],rw-w[i],op);	
								}		op[i]=0;				//不选取第i个物品,回溯	
										if (tw+rw>W)			        //右孩子结点剪枝	
											dfs_back(i+1,tw,tv,rw-w[i],op);	
					}
}
void knap_m()
{		for (int i=0;i<=n;i++)	
{		NodeType_Knap k;	
	k.w = w[i];	
		k.v = v[i];	
			A.push_back(k);	
			} 
			for (int i=1;i<=n;i++)		//求v/w	
				A[i].p=A[i].v/A[i].w;	
				sort(++A.begin(),A.end());	
							//A[1..n]排序	
			Knap();
					 }

void Knap()		
{  	
V=0;						//V初始化为0	
double weight=W;				//背包中能装入的余下重量	
	int i=1;
	while (A[i].w < weight)			        //物品i能够全部装入时循环
		{ 		x[i]=1;					//装入物品i	
			weight -= A[i].w;			//减少背包中能装入的余下重量	
				V += A[i].v;				//累计总价值	
					i++;					//继续循环	}
						if (weight > 0)					//当余下重量大于0
							{  		
							    x[i] = weight / A[i].w;		        //将物品i的一部分装入	
								V += x[i] * A[i].v;			//累计总价值
				        	}
	 }
}
int Fib(int n)
{	
printf("(%d)求解Fib(%d)\n",countf++,n);	
if (n==1 || n==2)
	{  		printf("   计算出Fib(%d)=%d\n",n,1);	
		return 1;	}	
		else
			{ 		int x = Fib(n-1);	
				int y = Fib(n-2);	
					printf("   计算出Fib(%d)=Fib(%d)+Fib(%d)=%d\n",		n,n-1,n-2,x+y);	
						return x+y;
			} 
	}

int Fib1(int n)			//算法
{  	dp_fib[1]=dp_fib[2]=1;
	printf("(%d)计算出Fib(1)=1\n",countf++);
		printf("(%d)计算出Fib(2)=1\n",countf++);
			for (int i=3;i<=n;i++)	
			{  		dp_fib[i]=dp_fib[i-1]+dp_fib[i-2];	
				printf("(%d)计算出Fib(%d)=%d\n",countf++,i,dp_fib[i]);
				}
			return dp_fib[n];
	}
void dp_Knap()
{	int i,r;
	for(i = 0;i <= n;i++)		//置边界条件dp[i][0]=0	
		dp[i][0] = 0;
			for (r = 0;r <= W;r++)		//置边界条件dp[0][r]=0	
				dp[0][r] = 0;
					for (i = 1;i <= n;i++)
						{  	
							for (r = 1;r <= W;r++)		
							if (r < w[i])			
								dp[i][r] = dp[i-1][r];		
							else		
						     	dp[i][r] = max(dp[i-1][r],dp[i-1][r-w[i]]+v[i]);
						} 
	}
void buildx()
{	
int i=n,r=W;
	maxv=0;	
	while (i>=0)				//判断每个物品	
	{		
	if (dp[i][r] != dp[i-1][r]) 	
		{  			
			bestx[i] = 1;		//选取物品i		
			maxv += v[i];		//累计总价值	
				r = r - w[i];	
						}		
	else		
		bestx[i]=0;		//不选取物品i
		i--;
	}
 }











