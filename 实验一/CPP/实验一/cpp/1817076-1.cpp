#include<iostream>
#include<vector>
using namespace std; 
//快速排序 分治思想 
void quickSort(int left,int right,int arr[],int n)//left:左哨兵位置,right:右哨兵位置,n:数组长度 
{	
	if(left >= right)
		return;
	int i, j, base, temp;
	i = left, j = right;
	base = arr[left];  //取最左边的数为基准数
	while (i < j)
	{
		while (arr[j] >= base && i < j)
			j--;//右哨兵先动 
		while (arr[i] <= base && i < j)
			i++;
		if(i < j)
		{
			temp = arr[i];
			arr[i] = arr[j];
			arr[j] = temp;
		}
	}
	//基准数归位
	arr[left] = arr[i];
	arr[i] = base;
	for(int i=0;i<n;i++)
		cout<<arr[i]<<" ";//输出一遍排序后结果 
	cout<<endl;	
	quickSort(left, i - 1, arr,n);//递归左边
	quickSort(i + 1, right, arr,n);//递归右边
}

int main()
{
	int n,left,right;
	cin>>n;
	int arr[n];
	
	for(int i=0;i<n;i++)
		cin>>arr[i];
		
	left=0;
	right=n-1;
	quickSort(left,right,arr,n);
	for(int i=0;i<n;i++)
		cout<<arr[i]<<" ";
} 

