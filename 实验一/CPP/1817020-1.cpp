/*#include<iostream>
using namespace std;
//const int MAX_LEN =100;
long maxSubSum(int a[],int left,int right)
//求a[left..right]序列中最大连续子序列和
{
	int i,j;
	long maxLeftSum,maxRightSum;
	long maxLeftBorderSum,leftBorderSum;
	long maxRightBorderSum,rightBorderSum;
	
	if(left == right)   //子序列只有一个元素
	{
		if(a[left] > 0)
			return a[left];
		else 
			return 0;
	 } 
	 
	 int mid = (right - left) / 2;
	 maxLeftSum = maxSubSum(a,left,mid);
	 maxRightSum =  maxSubSum(a,mid+1,right);
	 
	 maxLeftBorderSum = 0;
	 leftBorderSum = 0;
	 
	 for(i = mid;i >= left;i--)  //求出以左边加上a[mid]元素
	 {
	 	maxLeftBorderSum = maxLeftBorderSum +a[i];//构成的序列的最大和 
	 	if(leftBorderSum > maxLeftBorderSum)    
	 		maxLeftBorderSum=leftBorderSum; 
	  } 
	  
	  maxRightBorderSum = 0;
	  rightBorderSum = 0;
	  
	  for(j = mid + 1;j <= right; j++)  //求出a[mid]右边的元素
	  {
	  	rightBorderSum = rightBorderSum + a[j];   //构成的序列的最大和
	  	if(rightBorderSum > maxRightBorderSum)    
	 		maxRightBorderSum = rightBorderSum; 
	   } 
	   
	  
	  return maxLeftBorderSum+maxRightBorderSum ;
 } 
 

 int main()
 {
 	int a[] = {1,-2,3,4};
 	int left = 0,right = 3,b;
 	b = maxSubSum(a,0,3);
 	cout<<b;
 } */ 
 /*#include<iostream>
const int MAX_LEN =100;

using namespace std;

int getMaxSum(int* a,int n){
    int max=0;//假设最大值为0
    int temp=0;
    for(int i=0;i<n;i++){
        if(temp>0){
            temp+=a[i];//最大值临时变量只有大于零，才可能越加越大
        }else{
            temp=a[i];////最大值临时变量只有小于零，直接等于a[i],否则越加越小
        }
        if(max<temp){
            max=temp;//判断赋值
        }
    }
    return max;
}

int main(){
    int a[MAX_LEN];
    int maxSum;
    int n;
	cout<<"请输入序列的元素个数"<<endl;
    cin>>n;
	cout<<"请依次输入序列的元素"<<endl;
    for(int i=0;i<n;i++){
        cin>>a[i];
    }
    maxSum=getMaxSum(a,n);
    cout<<maxSum<<endl;
}  
*/
//#include<iostream>
//using namespace std;
//const int max_len =100;
//int getmaxsum(int* a,int n)
//{
//	int max = 0;
//	int temp = 0;
//	for(int i = 0 ; i < n ; i++)
//	{
//	if(temp > 0)
//	{
//		temp = temp + a[i];
//	 } 
//	 else {
//	 	temp = a[i];
//	 }
//	 if(max < temp)
//	 {
//	 	max = temp;
//	 }
//}
//return max;
//}
//
//int main()
//{
//	int a[max_len];
//	int maxsum;
//	int n;
//	cout<<"请输入序列的元素个数；";
//	cin>>n;
//	cout<<"请输入序列的元素 :";
//	for(int i = 0; i < n; i++)
//	 	cin>>a[i];
//	maxsum = getmaxsum(a,n);
//	cout<<maxsum;
//}



#include<iostream>
#include<stdio.h>
using namespace std;
#define MAXSIZE 100
 
int max3(int a,int b,int c)
{
	if(a>b) return a>c?a:c;
	else return b>c?b:c;
}
 
int MaxSubseqSum(int a[],int left,int right)
{
	int maxLeftSum,maxRightSum,maxMidSum;
	int maxLeftBorderSum,LeftBorderSum;
	int maxRightBorderSum,RightBorderSum;
	int mid;
	int i;
	
	if(left==right)	//递归出口，子序列只有一个元素时
		return a[left];
		
	mid=(left+right)/2;	//求中间位置
	maxLeftSum=MaxSubseqSum(a,left,mid);	//求左边序列的最大子序列和
	maxRightSum=MaxSubseqSum(a,mid+1,right);	//求右边序列的最大子序列和
	
	maxLeftBorderSum=0;
	LeftBorderSum=0;
	for(i=mid;i>=left;i--)	//从中间位置向左找靠边界的最大子序列
	{
		LeftBorderSum+=a[i];
		if(LeftBorderSum>maxLeftBorderSum)
			maxLeftBorderSum=LeftBorderSum;
	}
	
	maxRightBorderSum=0;
	RightBorderSum=0;
	for(i=mid+1;i<=right;i++)	//从中间位置向右找靠边界的最大子序列
	{
		RightBorderSum+=a[i];
		if(RightBorderSum>maxRightBorderSum)
			maxRightBorderSum=RightBorderSum;
	}
	
	maxMidSum=maxLeftBorderSum+maxRightBorderSum;	//得到处在中间位置上的最大子序列和
	return max3(maxLeftSum,maxRightSum,maxMidSum);
}
 
int MaxNum(int a[],int left,int right)
{
	int maxLeft,maxRight;
	int mid;
	if(left==right)
		return a[left];
	mid=(left+right)/2;
	maxLeft=MaxNum(a,left,mid);
	maxRight=MaxNum(a,mid+1,right);
	return maxLeft>maxRight?maxLeft:maxRight;
}
 
int main()
{
	int a[MAXSIZE];
	int count=0;
	int i,n;
	cout<<"序列长度:";
	cin>>n;
	cout<<"输入整数序列:";
	for(i=0;i<n;i++)
	{
		cin>>a[i];
		if(a[i] <= 0)
			count ++ ;
	}
	if(count == n)	//判断是否含有正整数
	{
		cout<<"最大子序列和";
		cout<<MaxNum(a,0,n-1);
	}	
	else
	{
		cout<<"最大子序列和:";
		cout<<MaxSubseqSum(a,0,n-1);
	}
}



