#include <iostream>
using namespace std;
template <class T>
int indexOfMax(T* a, int n);
template <class T>
void quickSort(T * a, int n);
template <class T>
void quickSort(T* a, int leftEnd, int rightEnd);
int main()
{
	
	int* a =new int[5];
	a[0] = 4, a[1] = 3, a[2] = 1, a[3] = 5, a[4] = 2;
	quickSort(a, 5);
	for (int i = 0; i < 5; i++) {
		cout<<a[i]<<"\t";
	}
	delete[] a;
	return 0;
}
/*
	快速排序的驱动程序
*/
template <class T>
void quickSort(T *a, int n) {
	if (n <= 1) { return; }
	int max = indexOfMax(a, n);
	swap(a[max], a[n - 1]);
	quickSort(a, 0, n - 2);
}
/*
快速排序函数
*/
template <class T>
void quickSort(T * a, int leftEnd, int rightEnd) {
	if (leftEnd >= rightEnd) { return; }
    //这里我们把支点选择为序列的第一个元素
	T privot = a[leftEnd];
	int leftCursor= leftEnd;//从左到右移动的索引
	int rightCursor = rightEnd + 1;//从右到左移动的索引、
    //将位于左侧不小于支点的元素和位于右侧不大于支点的元素进行交换
	while (true) {
		do
		{//寻找左侧不小于支点的元素
			leftCursor++;
		} while (a[leftCursor]<privot);
		do
		{//寻找右侧不大于支点的元素
			rightCursor--;
		} while (a[rightCursor]>privot);
		if (leftCursor >= rightCursor) { break; }
		swap(a[leftCursor], a[rightCursor]);
	}
	a[leftEnd] = a[rightCursor];
	a[rightCursor] = privot;
	quickSort(a, leftEnd, rightCursor - 1);//对左侧的数段进行快速排序
	quickSort(a, rightCursor + 1, rightEnd);//对右侧的数段进行快速排序
}
/*
找到最大值
*/
template< class T>
int indexOfMax(T* a, int n) {
	int index = 0;
	for (int i = 0; i < n; i++) {
		if (a[index] < a[i]) {
			index = i;
		}
	}
	return index;
}
