#include<iostream>  
#include<algorithm>  
using namespace std;  
#define maxn 100  
int num[maxn];  

template<class Type>  
void Merge(Type a[],Type b[],int left,int mid,int right)  
{  
     int i=left;                
     int j=mid+1;  
     int k=left;  
     while(i<=mid && j<=right)  
     {  
           if(a[i]<a[j])    
                b[k++]=a[i++];  
           else   
                b[k++]=a[j++];  
     }     
     if(i>mid)   
          for(int z=j;z<=right;z++)  
               b[k++]=a[z];  
     else  
          for(int z=i;z<=mid;z++)  
               b[k++]=a[z];  
}   

 
template<class Type>  
void MergePass(Type x[],Type y[],int s,int n)  
{  
     int i=0;  
     while(i+2*s-1<n)  
     {  
           Merge(x,y,i,i+s-1,i+2*s-1);  
           i+=2*s;       
     }   
     if(i+s<n)  
           Merge(x,y,i,i+s-1,n-1);  
     else                              
           for(int j=i;j<=n-1;j++)  
                y[j]=x[j];  
}      
template<class Type>  
void MergeSort(Type c[],int n)  
{  
     Type *d=new Type [n];   
     int  s=1;  
     while(s<n)  
     {  
           MergePass(c,d,s,n);   
           s+=s; 
           MergePass(d,c,s,n);    
           s+=s;                
     }
     delete[] d;  
}   
int  main()  
{   
     int n;  
     while(cin>>n)  
     {  
          for(int i=0;i<n;i++)  
               cin>>num[i];      
          MergeSort(num,n);     
          for(int i=0;i<n;i++)  
               cout<<num[i]<<endl;                         
     }   
     return 0;         
}   

