#include<iostream> 
using namespace std;
int binnarysearch(int a[],int key,int n)//A[]为待查数组；key为要查找的值；n为数组长度 
{
     int low=0;
     int high=n-1;
     while(low<=high)
     {
          int mid=(low+high)/2 ;      //mid=(low+high)/2
		  if(key==a[mid])
          return key;
          else if(key<a[mid])
          high=mid-1;
          else
          low=mid+1;     
     }
     return -1;              //当high<low 返回-1 
}
int main()
{
	int a[]={5,6,7,10,13,18,19,24,26,30};     //输入数组 
	cout<<"输入:数组a[]={5,6,7,10,13,18,19,24,26,30}"<<endl;
	cout<<"输入:数组长度为10"<<endl; 
	cout<<"输入：待查找的值key=24"<<endl; 
	cout<<"输出："<<binnarysearch(a,24,10)<<endl; 
	return 0;
}
