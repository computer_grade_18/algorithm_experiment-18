#include<bits/stdc++.h>
using namespace std;
vector<int> vec;
const int maxn=1000+8;
int a[maxn];
int n;
int quick_partition(int p,int q){
    int i=p;
    int x=a[p];
    for(int j=p+1;j<=q;j++){
        if(a[j]<=x){
            ++i;
            swap(a[i],a[j]);
        }
    }
    swap(a[i],a[p]);
    return i;
}
int rand(int p,int q){
    int k=rand()%(q-p+1)+p;
    swap(a[k],a[p]);
    return quick_partition(p,q);
}
void qsort(int p,int q){
    if(p<q){
        int k=rand(p,q);
        qsort(p,k-1);
        qsort(k+1,q);
    }
}
int main()
{
    while(cin>>n){
        for(int i=0;i<n;i++)
            cin>>a[i];
        qsort(0,n-1);
        for(int i=0;i<n;i++)
            cout<<a[i]<<" ";
        cout<<endl;
    }
    return 0;
}

