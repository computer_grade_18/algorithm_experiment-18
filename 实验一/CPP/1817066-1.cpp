#include<stdio.h>
#include<string.h>

void quickSort(int a[], int i, int j) { //i 是左边游标 j是右边游标
    if(i<j) {
        int l=i,r=j,base=a[l];  //设置左边游标位置 右边游标位置 以及基准
        while(l<r) {
            while(a[r]>base&&r>l) {     //从右往左扫描
                r--;
            }

            //找到右边一个比基准小的
            if(l<r) {               //判断是否j>i
                a[l++]=a[r];
            }

            while(a[l]<base&&r>l) {     //接下来，从左往右扫描
                l++;
            }
            //找到左边一个比基准大的

            if(l<r) {               //判断是否i<j
                a[r--]=a[l];
            }
        }
        //扫描完成 ，填充基准 此时 l==r
        a[l]=base;
        //依次对左右调用上面的算法
        quickSort(a,i,l-1);
        quickSort(a,l+1,j);
    }
}

int main() {

    int a[100];
    int n;
    printf("请输入数组大小：");
    scanf("%d",&n);
    for(int i=0; i<n; i++) {
        scanf("%d",&a[i]);
    }

    quickSort(a,0,n-1);
    for(int i=0; i<n; i++) {
        printf("%d ",a[i]);
    }
    putchar('\n');
}

