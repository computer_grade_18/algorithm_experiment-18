#include<iostream>
using namespace std;
 
void Merge(int arr[],int ft,int fina,int high)//ft为第1有序区的第1个元素，i指向第1个元素, fina为第1有序区的最后1个元素
{  
    int i=ft,j=fina+1,s=0; //fina+1为第2有序区第1个元素，j指向第1个元素
    int *temp=new(nothrow) int[high-ft+1]; 
    if(!temp)//内存分配失败
	{ 
        cout<<"error";
        return;
    }
    while(i<=fina&&j<=high)
	{
        if(arr[i]<=arr[j]) //较小的先存入temp中
            temp[s++]=arr[i++];
        else
            temp[s++]=arr[j++];
    }
    while(i<=fina)//若比较完之后，第一个有序区仍有剩余，则直接复制到t数组中
        temp[s++]=arr[i++];
    while(j<=high)
        temp[s++]=arr[j++];
    for(i=ft,s=0;i<=high;i++,s++)//将排好序的存回arr中ft到high这区间
		arr[i]=temp[s];
    delete []temp;//删除指针
}

void MergeSort(int arr[],int ft,int high)//分治的递归应用二路归并 
{
    if(ft<high)
	{
        int fina=(ft+high)/2;
        MergeSort(arr,ft,fina);
        MergeSort(arr,ft+1,high);
        Merge(arr,ft,fina,high);
    }
}
 
int main(){
	int y;//输入y个数 
	cin>>y;
	int arr[y]; 
    for(int i=0;i<y;i++)
    	cin>>arr[i];
    MergeSort(arr,0,y-1);
    for(int i=0;i<y;i++)
        cout<<arr[i]<<" ";
    return 0;
}

