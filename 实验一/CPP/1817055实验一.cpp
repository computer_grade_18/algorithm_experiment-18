#include<iostream> 
using namespace std;
int binnarysearch(int A[],int key,int n)//A[]为待查数组；key为要查找的值；n为数组长度 
{
     int low=0;
     int high=n-1;
     while(low<=high)
     {
          int mid=(low+high)/2 ;      //mid=(low+high)/2
		  if(key==A[mid])
          return key;
          else if(key<A[mid])
          high=mid-1;
          else
          low=mid+1;          
     }
     return -1;              //high>low 返回-1 
}
int main()
{
	int A[]={1,4,8,10,13,14,19,24,29,30};     //输入数组 
	cout<<"输入:数组A={1,4,8,10,13,14,19,24,29,30}"<<endl;
	cout<<"输入:数组长度为10"<<endl; 
	cout<<"输入：待查找的值key=29"<<endl; 
	cout<<"输出："<<binnarysearch(A,29,10)<<endl; 
	
	return 0;
}
