#include<bits/stdc++.h>
using namespace std;
 
 void merge(int arr[], int l, int mid, int r){
	int help[r-l+1];
	int i = 0;
	int lIndex = l;
	int rIndex = mid+1;
	while(lIndex <= mid && rIndex <= r){
		help[i++] = arr[lIndex] < arr[rIndex] ? arr[lIndex++]:arr[rIndex++];	
	}
  
	while(lIndex <= mid){
		help[i++] = arr[lIndex++];
	} 
	while(rIndex <= r){
		help[i++] = arr[rIndex++];
	}
    
	for(i = 0; i < r-l+1; i++){
		arr[l+i] = help[i];
	}
}
 

static void mergeSort(int arr[], int l, int r){
	if(l == r){
		return;
	}
	int mid = (l + r) / 2;
mergeSort(arr, l, mid);
mergeSort(arr, mid+1, r);
merge(arr, l, mid, r);
}
 

void mergeSort(int arr[], int n){
     if(arr == NULL || n < 2){
		return;
	}
	mergeSort(arr,0,n-1);
}
 
int main(){
	int n; 
	while(cin >> n){
		int arr[n];
		for(int i = 0; i < n; i++) cin >> arr[i];
	mergeSort(arr, n);
 for(int i = 0; i < n; i++){
			cout << arr[i] << " ";
		} 
		cout << endl;
	}
	return 0;
} 
