#include<iostream>
#include <stdio.h>
#include<time.h>
#include<math.h>
#include<stdlib.h>
using namespace std;
void swap(int a[], int i, int j) 
{
	int temp = a[i];
	a[i] = a[j];
	a[j] = temp;
}
int partition(float r[], int first, int end)
{
	int i = first, j = end;
	while (i < j)
	{
		while (i < j && r[i] <= r[j])j--;
		if (i < j)
		{
			float temp = r[i]; r[i] = r[j]; r[j] = temp;
			i++;
		}
		while (i < j && r[i] <= r[j])i++;
		if (i < j)
		{
			float temp = r[i]; r[i] = r[j]; r[j] = temp;
			j--;
		}
	}
	return i;
}
void QuickSort(float r[], int first, int end)
{
	int pivot;
	if (first < end)
	{
		pivot = partition(r, first, end);
		QuickSort(r, first, pivot - 1);
		QuickSort(r, pivot + 1, end);
	}
}

int main()
{
	int i = 0;
	int n;
	
	float a[1000];
	cout << "输入元素的个数" << endl;
	cin >> n;
	cout << "输入元素 " << endl;
	for (i = 0; i < n; i++)
	{
		cin >> a[i];
	}
	QuickSort(a, 0, n - 1);
	cout << "最终结果：";              
	int times1;
	time_t start1, end1; double Time1;
	start1 = (double)clock();
	times1 = clock();

	for (i = 0; i <n; i++)
	{
		cout << a[i] << " ";
	}
	end1 = (double)clock();
	Time1 = (double)(end1 - start1) / CLK_TCK;
	cout << endl << "快排使用时间：" << Time1 << endl;
	cout << "比较次数：" << times1;
	return 0;
}
