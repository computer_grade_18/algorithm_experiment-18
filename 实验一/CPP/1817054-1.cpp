#include <iostream>
using namespace std;
 
class QuickSort {
	public:
  // 交换两个数组元素
   void exchange(int a[]  , int i, int j) {
    int temp = a[i];
    a[i] = a[j];
    a[j] = temp;
  }
 
  // 选取左中右三个元素，求出中位数， 放入数组最左边的a[low]中
   int selectMiddleOfThree(int a[] , int low, int high) {
    int middle = low + (high -  low)/2;  // 取得位于数组中间的元素middle

    if(a[low]>a[high])    { 
      exchange(a, low, high);  //此时有 a[low] < a[high]
    }
    if(a[middle]>a[high]){
      exchange(a, middle, high); //此时有 a[low], a[middle] < a[high]
    }
    if(a[middle]>a[low]) {
      exchange(a, middle, low); //此时有a[middle]< a[low] < a[high]
    }
    return a[low];  // a[low]的值已经被换成三数中的中位数， 将其返回
  }
 
  	 int partition (int a[] , int low, int high) {
    int i = low, j = high+1;      // i, j为左右扫描指针 
    int pivotkey = selectMiddleOfThree( a, low, high);
    while(true) { 
      while (a[--j]>pivotkey) {   if(j == low) break; }  // 右游标左移
      while(a[++i]<pivotkey) {   if(i == high) break;  }  // 左游标右移
      if(i>=j) break;    // 左右游标相遇时候停止， 所以跳出外部while循环
      else exchange(a,i, j) ;  // 左右游标未相遇时停止, 交换各自所指元素，循环继续 
    }
    exchange(a, low, j); // 基准元素和游标相遇时所指元素交换，为最后一次交换
    return j;  // 一趟排序完成， 返回基准元素位置
  }
 
    void sort (int a [] ,  int low, int high) {
    if(high<= low) { return; } // 当high == low, 此时已是单元素子数组，自然有序， 故终止递归
    int j = partition(a, low, high);  // 调用partition进行切分
    sort(a,  low,  j-1);   // 对上一轮排序(切分)时，基准元素左边的子数组进行递归
    sort(a,  j+1,  high); // 对上一轮排序(切分)时，基准元素右边的子数组进行递归
  }

    void sort (int a[] ,int n){ //sort函数重载， 只向外暴露一个数组参数
    sort(a, 0, n - 1);
  }
};
int main(){
	int test1[] = {70,68,86,44,87,12,2,1,53,100}; int n=10;//随机情况 
	int test2[] = {10,9,8,7,6,5,4,3,2,1};				  //最坏情况 
	QuickSort s;
	s.sort(test1,n);
	
	cout<<"随机情况 :";
	for(int i=0;i<n;i++)
		cout<<test1[i]<<" ";
		
	cout<<endl;
	
	s.sort(test2,n);
	cout<<"最坏情况:"; 
	for(int i=0;i<n;i++)
		cout<<test2[i]<<" "; 
}

