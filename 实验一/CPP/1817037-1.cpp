
	
 
#include <iostream>
using namespace std;
 
///结构体存储开始下标、结束下标、区间和
struct SubArray
{
    int startIndex;
    int endIndex;
    int totalNum;
};
 
SubArray GetMaxArray(int arr[],int low,int high);
int main()
{
    //求最大的连续子段和
    int myArray[] = {13,-3,-25,20,-3,-16,-23,18,20,-7,12,-5,-22,15,-4,7};
    int length = sizeof(myArray)/sizeof(myArray[0]);
 
    SubArray subArray = GetMaxArray(myArray,0, length-1);
    cout<<"首index："<<subArray.startIndex<<endl;
    cout<<"尾index："<<subArray.endIndex<<endl;
    cout<<"最大值"<<subArray.totalNum<<endl;
 
    return 0;
}
 
SubArray GetMaxArray(int arr[],int low,int high)
{
    if (low == high)
    {
        SubArray subArray;
        subArray.startIndex = low;
        subArray.endIndex = high;
        subArray.totalNum = arr[low];
        return subArray;
    }
 
 
    int mid = (low + high) / 2;
    //左区间的最大子段
    SubArray subArrayLeft = GetMaxArray(arr,low, mid);
    //右区间的最大子段
    SubArray subArrayRight = GetMaxArray(arr,mid + 1, high);
 
    //左下标在左区间右下标在右区间的最大字段
    SubArray subArrayAll;
 
    //计算subArrayAll的左半最大部分
    int allMaxLeft = arr[mid] ;
    int leftIndex = mid;
    int tempNum = 0;
    for (int i = mid; i >= low; i--)
    {
        tempNum += arr[i];
        if (tempNum > allMaxLeft)
        {
            allMaxLeft = tempNum;
            leftIndex = i;
        }
    }
 
    //计算subArrayAll的右半最大部分
    int allMaxRight = arr[mid + 1];
    int rightIndex = mid + 1;
    tempNum = 0;
    for (int j = mid + 1; j <= high; j++)
    {
        tempNum += arr[j];
        if (tempNum > allMaxRight)
        {
            allMaxRight = tempNum;
            rightIndex = j;
        }
    }
 
    subArrayAll.totalNum = allMaxLeft + allMaxRight;
    subArrayAll.startIndex = leftIndex;
    subArrayAll.endIndex = rightIndex;
 
    //三者比较，谁的值最大
    if (subArrayLeft.totalNum >= subArrayRight.totalNum && subArrayLeft.totalNum >= subArrayAll.totalNum)
    {
        return subArrayLeft;
    }
    else if (subArrayRight.totalNum >= subArrayLeft.totalNum && subArrayRight.totalNum >= subArrayAll.totalNum)
    {
        return subArrayRight;
    }
    else
    {
        return subArrayAll;
    }
}
