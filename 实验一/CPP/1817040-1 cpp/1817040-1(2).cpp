//随机快排 
#include<iostream>
#include <stdlib.h>
#include <time.h>
#include<windows.h>
using namespace std;
int swap(int a[],int i,int j)  //交换函数 
{
	int temp=a[i];
	a[i]=a[j];
	a[j]=temp;
}
int partition(int a[],int p,int q,int n) //对数组进行分区 快速排序主要程序 
{
	int x=a[p],i=p;
	for(int j=p+1;j<=q;j++)
	{
		if(a[j]<=x)                //左小右大  若要左大右小 改为>=
		{
			i++;                   //i从p开始的,而且i交换后再遇到就要前进 
			swap(a,i,j);           //交换 
		}
	}
	swap(a,p,i);                   //关键点：将x放入正确的位置 
	cout<<"起始位置："<<p<<";终止位置："<<q<<";基准："<<x<<";基准所在位置："<<i<<";数组：";   //输出 基准及其相关数据  便于检查程序运行是否正确 
	for(int i=0;i<n;i++)
        cout<<a[i]<<" ";	
	cout<<endl; 
	return i;
}
void quicksort(int a[],int p,int q,int n)  //递归的调用 
{
	if(p<q)                                //执行条件 
	{   
	    srand((unsigned)time(NULL));      //设计随机数种子 
	    int m = rand()%(q-p+1)+p;         //利用rand()函数生成随机数 范围公示为rand()%(max-min+1)+min 
	    swap(a,m,p);                      //将随机基准与第一个交换 则分区函数就不用进行更改 同时增加了程序的最坏情况下的时间复杂度 
		int r=partition(a,p,q,n);
		quicksort(a,p,r-1,n);
		quicksort(a,r+1,q,n);
	}  
}
int main() 
{   int n,i;
    double d;
    LARGE_INTEGER frequency,start,end;
    QueryPerformanceFrequency(&frequency);
    cout<<"输入数组个数："; 
    cin>>n;
    int a[n];
    cout<<"输入数组：";
    for(i=0;i<n;i++)
    	cin>>a[i];
    QueryPerformanceCounter(&start);  
	quicksort(a,0,n-1,n);
	QueryPerformanceCounter(&end);
	cout<<"输出：";                //输出
	for(i=0;i<n;i++)
	cout<<a[i]<<" ";
    cout<<endl;
    d = (double)(end.QuadPart- start.QuadPart) /(double)frequency.QuadPart* 1000.0;
    cout<<d<<"s";
}
