//优化快速排序--随机基准快速排序 
#include<iostream>
#include <cstdlib>
#include "time.h"
using namespace std;
int swap(int a[],int i,int j)//数组元素的交换 
{
	int temp = a[i];
	a[i]=a[j];
	a[j]=temp;
}
int partition(int a[],int p,int q,int n)//分区算法
{
	int x = a[p];         //设基准 
	int i=p,j;
	for(j=p+1;j<=q;j++)
	{
		if(a[j]<=x)
		{
			i++;
			swap(a,i,j);
		}
	}
	swap(a,p,i);    //最后把基准与a[i]交换 
    cout<<"基准："<<x<< ";r： "<<i<<"数组：";
	for(int i=0;i<n;i++)
	{
		cout<<a[i]<<" ";
	 } 
	 cout<<";p: "<<p<<";q: "<<q<<endl;
	 return i;
} 
void quicksort(int a[],int p,int q,int n)  //递归
{
if(p<q)
{
    srand(unsigned(time(NULL)));   //随机函数种子使下面随机基准不重复 
	int randnum=rand()%(q-p+1)+p;
	swap(a,randnum,p);
	int r=partition(a,p,q,n);
	quicksort(a,p,r-1,n);
	quicksort(a,r+1,q,n);
} 
} 
int main()
{
	int i,n;
	cout<<"数组的元素个数：";
	cin>>n;
	int a[n];
	cout<<"输入数组元素：";
	for(i=0;i<n;i++)
	{
		cin>>a[i];
	}
	quicksort(a,0,n-1,n);
	cout<<"排序结果：";
	for(i=0;i<n;i++)
	{
		cout<<a[i]<<" "; 
	 } 
}

 
