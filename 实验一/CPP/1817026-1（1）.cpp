#include <iostream>

using namespace std;

typedef int Type;

void print(Type a[], int n)
{
    for (int i=0; i<n; ++i) {
        cout << a[i] << " ";
    }
    cout << endl;
}

void GetIndex(int a[], int t[], int n, int &m)
{
    int j = 0;
    t[j++] = 0;
    for(int i = 0; i < n-1; i++) {
        if(a[i+1] < a[i]) {
            t[j++] = i+1;
        }
    }
    m = j;     
}

void Merge(Type c[], Type d[], int l, int m, int r)
{
    int i = l, j = m + 1, k = l;
    while (i <= m && j <= r) {
        if (c[i] <= c[j]) {
            d[k++] = c[i++];
        } else {
            d[k++] = c[j++];
        }
    }
    if (i > m) {
        for (int q = j; q <= r; q++) {
            d[k++] = c[q];
        }
    } else {
        for (int q = i; q <= m; q++) {
            d[k++] = c[q];
        }
    }
}

void MergePass(Type x[], Type y[], Type t[], int s, int n, int cnt)
{
  
    int i = 0;

    while(i <= cnt - 2 * s) {
        int r = ((i + 2 * s) < cnt) ? t[i+2*s] : n;
        Merge(x, y, t[i], t[i+s]-1, r-1);
        i = i + 2 * s;
    }

    if(i + s < cnt) {
        Merge(x, y, t[i], t[i+s]-1, n-1);
    } else if (i < cnt) {
        for(int j = t[i]; j <= n - 1; j++) {
            y[j] = x[j];
        }
    }
}

void MergeSort(Type a[], Type t[], int n, int cnt)
{
    Type *b = new Type[n];
    int s = 1;

    while (s < cnt) {
        MergePass(a, b, t, s, n, cnt);   
        s += s;
        MergePass(b, a, t, s, n, cnt);    
        s += s;
    }

    delete[] b;
}

int main(int argc, char *argv[])
{
    Type a[] = {1, 5, 2, 3, 6, 0, 7, 4, 8};
    int len = sizeof(a) / sizeof(int);
    Type *t = new Type[len];
    int cnt;     

    GetIndex(a, t, len, cnt);
    MergeSort(a, t, len, cnt);
    print(a, len);

    delete[] t;

    return 0;
}
