#include <iostream>
using namespace std;
int BinarySearch(int arr[],int low,int high,int key){
	int mid;
	while (low<=high)
	{
		mid=(low+high)/2;
		if (key==arr[mid])
		   return mid;
		else if(key<arr[mid]) 
		   high=mid-1;
		else 
		   low=mid+1;
	}
	return -1;
}
int main()
{
	int arr[]={1,2,3,4,5,6,7,8,9,10};
	int key,n;
	cin>>key;
	n=BinarySearch(arr,0,9,key)+1;
	cout<<"该数在第"<<n<<"个位置"<<endl; 
}
 

