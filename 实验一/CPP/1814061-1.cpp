//
//  main.cpp
//  实验一
//
//  Created by 喵 on 2020/5/16.
//  Copyright © 2020 喵. All rights reserved.
//

#include <iostream>
using namespace std;
#define N 100
void Perm(int *list,int k,int m)
{
    if(k==m)
    {
        for(int i=0;i<m;i++)
            cout<<list[i]<<"";
        cout<<endl;
        return;
    }
    else
    {
        for(int i=m;i<k;i++)
        {
            swap(list[m],list[i]);
            Perm(list,k,m+1);
            swap(list[m],list[i]);
        }
    }
}
void swap(int a,int b)
{
    int temp;
    temp=a;
    a=b;
    b=temp;
}
int main()
{
    int i,n;
    int a[N];
    cout<<"请输入排列数据总个数：";
    cin>>n;
    cout<<"请输入数据：";
    for(i=0;i<n;i++)
    {
        cin>>a[i];
    }
    cout<<"该数据的全排列："<<endl;
    Perm(a,n,0);
    return 0;
}
