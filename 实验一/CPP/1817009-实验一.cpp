#include <iostream> 
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
using namespace std;
void Merge(int sourceArr[],int tempArr[], int startIndex, int midIndex, int endIndex)
{
    int i = startIndex, j=midIndex+1, k = startIndex;
    while(i!=midIndex+1 && j!=endIndex+1)		//当左子数组和右子数组都没有扫描完的时候，循环继续 
    {
        if(sourceArr[i] > sourceArr[j])			//如果i指向的数字大于j指向的数字，把j指向的数组放入temp数组 
            tempArr[k++] = sourceArr[j++];
        else									//反之，把i指向的数组放入temp数组 
            tempArr[k++] = sourceArr[i++];
    }
    while(i != midIndex+1)						//当左子数组没有扫描完而右子数组扫描完时，把左子数组的剩下数字都放入temp数组 
        tempArr[k++] = sourceArr[i++];
    while(j != endIndex+1)
        tempArr[k++] = sourceArr[j++];			//当右子数组没有扫描完而左子数组扫描完时，把左子数组的剩下数字都放入temp数组
    for(i=startIndex; i<=endIndex; i++)
        sourceArr[i] = tempArr[i];
}
 
void MergeSort(int sourceArr[], int tempArr[], int startIndex, int endIndex)
{
    int midIndex;
    if(startIndex < endIndex)
    {
        midIndex = startIndex + (endIndex-startIndex) / 2;			//计算中间数字位置，为切数组做准备 
        MergeSort(sourceArr, tempArr, startIndex, midIndex);		//对左子数组进行切分
        MergeSort(sourceArr, tempArr, midIndex+1, endIndex);		//对右子数组进行切分 
        Merge(sourceArr, tempArr, startIndex, midIndex, endIndex);	//比较排序，合并数组 
    }
}
 
int main(int argc, char * argv[])
{
    printf("请输入8个待排序数字");
	int a[8];
    for(int x=0;x<8;x++)
    {
    	cin>>a[x];
    }
    int i, b[8];
    MergeSort(a, b, 0, 7);
    for(i=0; i<8; i++)
        printf("%d ", a[i]);
    printf("\n");
    return 0;
}
