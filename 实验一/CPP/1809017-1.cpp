/*
给定N(N≤10^5)个整数，要求用快速排序对数据进行升序排列
输入
 连续输入多组数据，每组输入数据第一行给出正整数N(≤10^5)，随后给出N个整数，数字间以空格分隔。
输出
 输出排序后的结果，数字间以一个空格间隔，行末不得有多余空格。
举例输入数组 
8
29 38 65 87 56 13 27 49
例子输出 
13 27 29 38 49 56 65 87*/ 
#include<iostream> 
using namespace std;
int a[100000];  //给定数组内数字的范围 
int kspaixu(int L,int R)  //设L为数组最左的数字，R为数组最右的数字 
{  
    if(L>=R)  
    {  
        return 0;  
    }  
    int i=L,j=R;  //设定指示器i，j，分别指向L,R 
    int flag=a[L];     //用数组内第一个元素作为基准 
    while(i<j)    //数组内至少存在两个元素的情况 
    {                         
        while(i<j&&flag<=a[j])  //从左向右扫描，找出第一个小于a[i]的值 
        {  
            j--;  
        }          
        a[i]=a[j];     //将找到的值进行交换 
        while(i<j&&flag>=a[i])  //从右向左扫描，找第一个小于a[j]的值 
        {  
            i++;  
        }          
        a[j] = a[i];  // 将找到的值进行交换
    }     
    a[i]=flag;  
    kspaixu(L,i-1); //对左区间排序 
    kspaixu(i+1,R);  //对右区间排序
}  
int main()  
{  
    int n,i,j;  
    while()  
    {  
        for(i=0;i<n;i++)  //输入数组 
        {  
            cin>>a[i];  
        }  
        kspaixu(0,n-1);  //调用快速排序算法 
        for(j=0;j<n;j++)  //输出排序后的数组 
        {  
            if(n-1!=j) cout<<a[j];  
            else cout<<a[j]<<endl;  
        }  
    }  
}  
