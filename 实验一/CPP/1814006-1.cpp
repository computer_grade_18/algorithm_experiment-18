#include <iostream>
#include<stdlib.h>
#include <time.h>
#define random(x) (rand()%x)
using namespace std;
int merge(int a[],int l,int mid,int r)//归并
{
	int n=r-l+1,T=0;//临时数组存合并后的有序序列
    int* tmp=new int[n];
    int i=0;
    int left=l;
    int right=mid+1;
    while(left<=mid && right<=r)
        {
        	tmp[i++] = a[left]<= a[right]?a[left++]:a[right++];
        	T++; 
        }
    while(left<=mid)
        tmp[i++]=a[left++];
    while(right<=r)
        tmp[i++]=a[right++];
    for(int j=0;j<n;++j)
        a[l+j]=tmp[j];
    delete [] tmp;//删掉堆区的内存
    return T; 
} 
int mergesort(int a[],int l,int r)//分治
{
	int T=0;
	if(l==r)return 0;
	int mid=l+(r-l)/2;
	mergesort(a,l,mid);
	mergesort(a,mid+1,r);
	if(a[mid]>a[mid+1]) // 如果原来就有序就不用归并 
	T+=merge(a,l,mid,r);
	return T; 
} 
string yz(int a[],int n) 
{
	for(int i=1;i<n;i++)
	{
		if(a[i]<a[i-1])return "false";
	}
	return "yes";
}
int main(){
    //int a[519152];  这个是边界，多于这个数就不能排序了 
    int n,times=0;
    cout<<"输入产生随机数个数"<<endl;
	cin>>n; 
    int a[n]; 
	cout<<"随机产生的数组 : "; 
    for(int i=0;i<n;i++)
    {
   	   a[i]=random(1000);
   	   cout<<a[i]<<" ";
    }
    time_t start,end;
    double Time;
    start=(double)clock();
    times=mergesort(a,0,n-1);
	cout<<endl<<"归并排序后的数组:"; 
    for(int i=0;i<n;i++)
    cout<<a[i]<<" ";
    cout<<endl<<"验证结果："<<yz(a,n); 
    end=(double)clock();
    Time=(double)(end-start)/CLK_TCK;
    cout<<endl<<"归并排序所用时间:"<<Time<<endl; 
    cout<<"归并排序的比较次数:"<<times; 
}
