#include <bits/stdc++.h>
using namespace std;
int binary_search(int* A, int n, int key)
{
	int left = 0, right = n - 1;
	while (left <= right)
	{
		int mid = left + (int)((key - A[left])) / (A[right] - A[left]) * (right - left);
		if (key == A[mid])return mid;
		else if (key < A[mid])right = mid - 1;
		else left = mid + 1;
	}
	return -1;
}

int main()
{
	int A[8] = { 4,7,9,12,15,34,36,67 }, key;
	cin >> key;
	cout << binary_search(A, sizeof(a)/sizeof(int), key);
	return 0;
}

