#include<iostream>
#include <stdio.h>
#include<time.h>
#include<math.h>
#include<stdlib.h>
#define N  12        //数组长度 
using namespace std;

    void quickSort(int *arr,int left,int right)
    {
    	if (left >= right) return; //序列只有1个或没有元素 
    	int le= left;
    	int ri= right;
    	int key = arr[left];  //以首元素为基准数 
    	while (le<ri)    //从区间两端交替向中间扫描，直到le>=ri 
    	{
    		while (le<ri && arr[ri] >= key)//从右向左扫描，找到第一个小于基准数的前（左）移 
    		{	
    			ri--;	
    		}
    		arr[le] = arr[ri];
    		while (le<ri&& arr[le] <= key)//从左向右扫描，找到第一个大于基准数的后（右）移
    		{
    			le++;
    		}
    		arr[ri] = arr[le];
    	}
    	arr[le] = key; 
     
    	quickSort(arr,left,le- 1);//递归调用快排进行左区间排序 
    	quickSort(arr,le+1,right);//右区间快排 
    	
    }
    void quick_sort(int *arr, int len)
    {	
    	quickSort(arr,0,len-1);	
    }
    	
int main()
{
    int i = 0; 
//   int arr[N] = {23, 45, 67, 89, 9, 16, 55, 43, 22, 98, 87, 76};
//              结果：9 16 22 23 43 45 55 67 76 87 89 98
    int arr[N];
    for(int i=0;i<N;i++)
    {
    	cin>>arr[i];
	}
	int times1;
	time_t start1,end1;double Time1;
	start1=(double)clock();
	quick_sort(arr,N);
	times1=clock();

   	for(i=0;i<N;i++)
   	 {
   	  cout<<arr[i]<<" "; 
  	 }
  	 end1=(double)clock();
	 Time1=(double)(end1-start1)/CLK_TCK;
  	 cout<<endl<<"快排使用时间："<<Time1<<endl;
  	 cout<<"比较次数："<<times1;
   return 0;	
}

