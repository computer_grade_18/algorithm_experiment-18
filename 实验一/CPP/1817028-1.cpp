#include <iostream>     
using namespace std;     
#define length 100
void swap(int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}
     
int spite(int *data, int low, int high)
{
    int i = low, j = high;

    int mid = data[i];

    while(i < j) {

    	while (i < j && mid < data[j]) --j;

    	if (i < j) 
    	{
    		swap(data[j], data[i]);

    		++i;
    	}

    	while (i < j && mid >= data[i]) ++i;
    	if (i < j) 
    	{
   			swap(data[i], data[j]);

    		--j;
    	}
   	}
   	return i;
}
void quickSort(int *data, int low, int high)
{
    if (low < high)
    {
    	int mid = spite(data, low, high);
    	quickSort(data, low, mid - 1);
    	quickSort(data, mid + 1, high);
    }
}
int main()
{
   	int data[length], all;
   	std::cout << "请输入数组元素的个数" << std::endl;
   	std::cin >> all;
   		if (all <= 0)
   		{
   			std::cout << "输入数据的个数不能小于等于0" << std::endl;
   		return -1;
   		}
   		std::cout << "请输入数组的每个元素值" << std::endl;
   		for (int i = 0; i < all; ++i)
   		{
   			std::cin >> data[i];	
   		}
   		quickSort(data, 0, all - 1);

   		for (int i = 0; i < all; ++i)
   		{
   		std::cout << data[i] << "\t";	
   		}
	std::cout << std::endl;
	return 0;	
}

