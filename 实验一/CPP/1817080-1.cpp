#include <iostream>
#include <time.h>
#include <stdlib.h>
using namespace std;
//合并函数
void Merge(int *_Array, int p, int q, int r) // p:第0个;r:第n-1个数,q:第(r + p) / 2个数 
{
    int len1 = q - p + 1;
    int len2 = r - q;
    int *L = new int[len1 + 1];      //用动态数组储存左边的数 
    int *R = new int[len2 + 1];      //用动态数组储存右边的数 
    for (int i = 0; i < len1; i++) 
	{                                
        L[i] = _Array[p + i];        // 把Array数组左边的数放入L数组 
    }
    for (int j = 0; j < len2; j++) 
	{                                
        R[j] = _Array[q + 1 + j];    // 把Array数组右边的数放入R数组
    }
    L[len1]=R[len2]=INT_MAX;         //定义无穷大 
    int i = 0, j = 0;
    for (int k = p; k <= r; k++) 
	{
        if (L[i] < R[j]) 
		{ 
            _Array[k] = L[i];        //小的放左边，大的放右边
            i++;
        }
        else 
		{
            _Array[k] = R[j];
            j++;
        }
    }
}
//  归并排序
void MergeSort(int _Array[], int p, int r) 
{
    if (p < r)                       //p:第0个;r:第n-1个数。数组至少要有两个数据
	{ 
        int q;
        q = (r + p) / 2;             //拆分两组 
        MergeSort(_Array , p , q);   //拆分第0个——第 (r + p) / 2个 ，即拆分左半部分 
        MergeSort(_Array , q+1 , r); //拆分第(r + p) / 2个——第r个 ，即拆分右半部分 
        Merge(_Array , p , q , r);   //调用合并函数，从第0个——第n-1个排好 
    }
} 
int main() 
{
    int n;
    cout << "输入产生的数组个数：";
    cin >> n;
    cout << endl;
    int *Array = new int[n];
    cout << "产生的随机数组为：";
    srand((unsigned)time(0));    
    for (int i = 0; i < n; i++) 
	{
        Array[i] =  (rand()%(100-0+1))+0;
        cout<<Array[i]<<"  ";
    }
    cout<<endl;    
    MergeSort(Array,0,n-1);     
    cout << "排序后的数组为：";
    for (int j = 0; j < n; j++)
	{
        cout<<Array[j]<<"  ";
    }
    return 0 ;
} 
