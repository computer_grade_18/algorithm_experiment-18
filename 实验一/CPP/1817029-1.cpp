#include<iostream>
using namespace std;
//A[]为严格递增序列，left为二分下界，right为二分上界，x 为欲查询的数
//二分区间为左闭右闭得[left,right]，传入的值为[0,n-1] 
int binarySearch(int A[],int left,int right,int x)
{
	int mid;//mid为left和right的中点 
	while(left<=right)//若left>right则无法形成闭区间 
	{
		mid=(left+right)/2;//取中点 
		if(A[mid]==x) return mid;//找到x，返回下标 
		else if(A[mid]>x) right=mid-1; 
//往左子区间[left,mid-1]查找 
		else left=mid+1;//往右子区间[mid+1,right]查找 
	}
	return -1;//查找失败，返回-1 
}
int main()
{
	int n,x;
	cout<<"请输入数组个数"<<endl;
	cin>>n;
	int A[n];
	cout<<"请输入有序数组"<<endl;
	for(int i=0;i<n;i++)
	cin>>A[i];
	cout<<"请输入查询的数"<<endl;
	cin>>x;
	cout<<binarySearch(A,0,n-1,x);
	return 0;
}
