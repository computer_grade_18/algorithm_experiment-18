//分治法-快速排序-随机基准 
#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;
int swap(int a[],int i,int j)  //元素间的交换 
{
	int temp=a[i];
	a[i]=a[j];
	a[j]=temp;
}
int partition(int a[],int p,int q,int n) //分区算法的设计
{
	int x=a[p];
	int i=p,j;
	for(j=p+1;j<=q;j++)
	{
		if(a[j]<=x)
		{
			i++;
			swap(a,i,j);
		}
	}
	swap(a,p,i);   
	cout<<"基准："<<x<<";r："<<i<<";数组：";   //输出每次递归的各变量的数据 
	 for(int i=0;i<n;i++)
    {
    	cout<<a[i]<<" ";
	}	
	cout<<";p："<<p<<";q："<<q<<endl;
	return i;
}
void quicksort(int a[],int p,int q,int n)  //递归的调用 
{
	if(p<q)
	{   
	    srand(unsigned(time(NULL)));      
        int randNum=rand()%(q-p+1)+p;   //随机基准 
        swap(a,randNum,p);  //分区的首地址元素与随机得到底基准进行调换 
		int r=partition(a,p,q,n);
		quicksort(a,p,r-1,n);
		quicksort(a,r+1,q,n);
	}  
}
int main() 
{   int n,i;
    cout<<"输入的数组的个数："; 
    cin>>n;
    int a[n];
    cout<<"输入的数组的元素：";
    for(i=0;i<n;i++)
    {
    	cin>>a[i];
	}
	quicksort(a,0,n-1,n);
	cout<<"最终结果：";                //for循环输出最终值 
	 for(i=0;i<n;i++)
    {
    	cout<<a[i]<<" ";
	}	
}
