function work() {
	function quickSort(arr, low, high) {
		var key = arr[low];
		var start = low;
		var end = high;
		while (end > start) {
			while (end > start && arr[end] >= key) end--;
			if (arr[end] <= key) {
				var temp = arr[end];
				arr[end] = arr[start];
				arr[start] = temp;
			}
			while (end > start && arr[start] <= key) start++;
			if (arr[start] >= key) {
				var temp = arr[start];
				arr[start] = arr[end];
				arr[end] = temp;
			}
		}
		if (start > low) quickSort(arr, low, start - 1);
		if (end < high) quickSort(arr, end + 1, high);
	}

	var list = $("#input").val().split(',').map(Number);
	var start = 0;
	var end = list.length-1;
	$("#head").text("待排序列:" + list);
	quickSort(list,start,end); 
	$("#after").text("排序后:" + list);
}
