//分治法-快速排序 
#include<iostream>
#include<cstdlib>
#include<math.h>
#include<windows.h>
#include<time.h>
#include <stack>
#include"stdio.h"
#define Maxsize 100
using namespace std;
int swap(int a[],int i,int j)  //元素间的交换 
{
	int temp=a[i];
	a[i]=a[j];
	a[j]=temp;
}

int partition(int a[],int p,int q) //分区算法的设计
{   
	int x=a[p];
	int i=p,j;
	for(j=p+1;j<=q;j++)
	{
		if(a[j]<=x)
		{
			i++;
			swap(a,i,j);
		}
	}
	swap(a,p,i);   
	return i;
}
void quicksort(int a[],int p,int q)  //随机基准
{
	if(p<q)
	{   srand(unsigned(time(NULL)));      
        int randNum=rand()%(q-p+1)+p;   //随机基准 
        swap(a,randNum,p);
		int r=partition(a,p,q);
		quicksort(a,p,r-1);
		quicksort(a,r+1,q);
	}  
}
void quicksort1(int a[],int p,int q)  //经典的快速排序 
{
	if(p<q)
	{   
		int r=partition(a,p,q);
		quicksort1(a,p,r-1);
		quicksort1(a,r+1,q);
	}  
}
void Quicksort(int r[],int first,int end,int n){//普通快速排序 
	
	if(first>=end)
	return;
	int i=first,j=end;
	int tmp=r[first];
		 while (i < j) {
            while (r[j] >= tmp && i < j) {
                j--;
            }
            r[i]=r[j];
            while (r[i] <= tmp && i < j) {
                i++;
            }
            r[j]=r[i]; 
        }  
        r[i]=tmp;
//		n++;
//		cout<<"第"<<n<<"次排序结果"; 
//        for(int k=0;k<m;k++)
//        	cout<<r[k]<<" ";
//        	cout<<endl;
        Quicksort(r,first,j-1,n);  
        Quicksort(r,j+1,end,n);
	}
		//划分算法

 	int Partition( int a[], int low, int high )
{
	//假设每次都以第一个元素作为枢轴值，进行一趟划分：
	int pivot = a[low];
	 
	while( low<high )
	{
		while( low<high && a[high]>=pivot )
		--high;
		a[low] = a[high];  //停下来做交换 
		while( low<high && a[low]<=pivot )
		++low;
		a[high] = a[low];  //停下来做交换 
	}
	
	a[low] = pivot;  //pivot的最终落点 
	return low;
}
	void quicksort3(int arr[], int left, int right)
{
	//手动利用栈来存储每次分块快排的起始点
	//栈非空时循环获取中轴入栈
	stack<int> s;
	if( left<right )
	{
		int boundary = Partition(arr,left,right);
		
		if( boundary-1>left ) //确保左分区存在 
		{
			//将左分区端点入栈 
			s.push(left);
			s.push(boundary-1);
		}
		if( boundary+1<right ) //确保右分区存在 
		{
			s.push(boundary+1);
			s.push(right);
		}
		
		while( !s.empty() )
		{
			//得到某分区的左右边界 
			int r = s.top();
			s.pop();  
			int l = s.top();
			s.pop();
			
			boundary = Partition(arr,l,r);
			if( boundary-1>l ) //确保左分区存在 
		    { 
			    //将左分区端点入栈 
		    	s.push(l);
		    	s.push(boundary-1);
	    	}
		    if( boundary+1<r ) //确保右分区存在 
	     	{
	 	    	s.push(boundary+1);
	    		s.push(r);
	    	}
		}
	}
}
//非递归快排-不运用栈 
void quicksort2(int a[],int n)
{
int i,j,low,high,temp,top=-1;
struct node
{
int low,high;
}st[Maxsize]; 
top++;
st[top].low=0;st[top].high=n-1;
while(top>-1)
{
low=st[top].low;high=st[top].high;
top--;
i=low;j=high;
if(low<high)
{
temp=a[low];
while(i!=j)
{
while(i<j&&a[j]>temp)j--;
if(i<j){a[i]=a[j];i++;}
while(i<j&&a[i]<temp)i++;
if(i<j){a[j]=a[i];j--;}
}
a[i]=temp;
top++;st[top].low=low;st[top].high=i-1;
top++;st[top].low=i+1;st[top].high=high;
}
}
}
int main(void) 
{   
    LARGE_INTEGER frequency,start,end;
 QueryPerformanceFrequency(&frequency);
    srand((unsigned)time(NULL));
    double d;
    int a[100000],b[100000]; 
	int n;  //产生随机数种子，避免产生的随机数相同
	cout<<"输入的随机元素的个数：";
	cin>>n; 
    for(int i=0;i<n;i++)              //初始化数组
	{
		b[i]=rand();
	}
	
 for(int i=0;i<n;i++)              //初始化数组
	{
		a[i]=b[i];
	}
 QueryPerformanceCounter(&start);
 quicksort(a,0,n-1);   //随机基准 
 QueryPerformanceCounter(&end);
 d = (double)(end.QuadPart- start.QuadPart) /(double)frequency.QuadPart* 1000.0; 
	cout<<endl;
 cout<<"随机基准-快速排序："<<d<<"毫秒"<<endl;
  cout<<endl;

   
	 for(int i=0;i<n;i++)              //初始化数组
	{
		a[i]=b[i];
	}
	QueryPerformanceCounter(&start);
quicksort1(a,0,n-1); 
 QueryPerformanceCounter(&end);
 d = (double)(end.QuadPart- start.QuadPart) /(double)frequency.QuadPart* 1000.0; 
 cout<<"经典快速排序："<<d<<"毫秒"<<endl;
  cout<<endl;

  for(int i=0;i<n;i++)              //初始化数组
	{
		a[i]=b[i];
	}
QueryPerformanceCounter(&start);
quicksort2(a,n);  
 QueryPerformanceCounter(&end);
 d = (double)(end.QuadPart- start.QuadPart) /(double)frequency.QuadPart* 1000.0; 
 cout<<"非递归-不使用栈："<<d<<"毫秒"<<endl;
  cout<<endl;

  for(int i=0;i<n;i++)              //初始化数组
	{
		a[i]=b[i];
	}
  
QueryPerformanceCounter(&start);
quicksort3(a,0,n-1);  
 QueryPerformanceCounter(&end);
 d = (double)(end.QuadPart- start.QuadPart) /(double)frequency.QuadPart* 1000.0; 
 cout<<"非递归-手动入栈："<<d<<"毫秒"<<endl;
  cout<<endl; 

  return 0;
}
