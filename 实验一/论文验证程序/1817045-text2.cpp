#include <iostream>
#include<stdlib.h>
#include<time.h>
#define M 1024
#define NUMBER 500
using namespace std;

//两数交换
void exchange(int &a,int &b)
{
    int temp;
    temp=a;
    a=b;
    b=temp;
}
//p表示将数组A排序的起始下标，r是结束下标
int random_patition(int *A,int p,int r)
{
    int temp;
    int i=p-1;
    //产生随机数组下标
    int k= p + rand()%(r -p +1);
    //仍然将随机的枢轴交换到最后
    exchange(A[r],A[k]);
    temp=A[r];

    for(int j=p;j<=r-1;j++)
    {
        if(A[j]<=temp)
        {
            i=i+1;
            exchange(A[i],A[j]);
        }
    }
    //最后主元交换
    exchange(A[i+1],A[r]);
    return i+1;
}

//递归调用
void QuickSort(int *A,int p,int q)
{
    if(p<q){
        int r = random_patition(A, p, q);
        QuickSort(A, p, r-1);
        QuickSort(A, r+1, q);
    }

}

int main(void)
{
	int n;
	freopen("data.txt","r",stdin);
    srand((unsigned)time(NULL));
    //clock_t begin_time,end_time;
    double star,finish;
	
    int *p;
    cout<<"输入元素的个数"<<endl;
    cin>>n;
	cout<<"输入元素 "<<endl;
    p=new int[n];
                       //begin_time=clock();
    for(int k=0;k<n;k++)
    {
                               //p[k]=rand()%NUMBER;
        cin>>p[k];
    }
    cout<<endl<<endl;

  star=(double)clock(); 
    QuickSort(p,0,n-1);
                             //end_time=clock();

    for(int i=0;i<n;i++)
    {
        cout<<p[i]<<",";
    }
    cout<<endl;
    for(int i=0;i<M;i++)finish=(double)clock();
	printf("The time is:%.2fms\n",(finish-star));
                               // cout<<endl<<endl<<end_time-begin_time<<" ms"<<endl;

    return 0;
}

