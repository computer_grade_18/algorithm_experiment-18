#include<iostream>
#include <cstring>
 using namespace std;
typedef struct TreeNode
{
    char val;
    struct TreeNode *left, *right;
}TreeNode;    
class MinAndMax {
    public:
        int min;
        int max;
        MinAndMax() {}
        MinAndMax(int x, int y):min(x), max(y) {}
};
class Solution {
public:
    int isBST = 1;
    int isValidBST(TreeNode* root) {
        if(!root) return 1 ;
        dfs(root);
        cout<<isBST;
        return 0;
    }
    MinAndMax dfs(TreeNode *node) {
        if(isBST) {
            MinAndMax leftMAM;
            MinAndMax rightMAM;
            //深度遍历子树并返回子树的最值
            if(node->left) {
                leftMAM = dfs(node->left);//获取左子树最大值最小值
                if(node->val <= leftMAM.max) isBST = 0;//如果结点值不大于左子树的最大值, 则断定不是BST
            }
            if(node->right) {
                rightMAM = dfs(node->right);//获取右子树最大值最小值
                if(node->val >= rightMAM.min) isBST = 0;//如果结点值不小于右子树的最小值, 也断定不是BST
            }
            //默认最小值最大值就是结点本身的值
            int min = node->val;
            int max = node->val;
            if(node->left) min = leftMAM.min;//如果有左子树, 则min是左子树的最小值
            if(node->right) max = rightMAM.max;//如果有右子树, 则max是右子树的最大值
            return MinAndMax(min, max);//每次遍历返回树的最大值最小值
        }
        return MinAndMax(0, 0);
    }
};
     //??????
 
TreeNode *creat(char *pre, char *in, int len)    //????????
{
    int k;
    if (len <= 0)
        return NULL;
    TreeNode *head; 
    head=new TreeNode;
    head -> val = pre[0];
    char *p;
    for (p = in; p != NULL; p++){
        if (*p == *pre)    //?????????????????? 
            break;
    }
    k = p - in;
    head -> left = creat(pre + 1, in, k);    //???????? 
    head -> right = creat(pre + k + 1, p + 1, len - k - 1);    //????
    return head;
}
 void postorder(TreeNode *root)  //后序遍历树输出
{
    if(root!=NULL)
    {
        postorder(root->left);
        postorder(root->right);
        cout<<root->val;           
    }
    else
    return;
}
int main()
{    char pre[30], in[30];      //???????????
    TreeNode *head;
    head=new TreeNode;
    cout<<"先序遍历为："; 
    cin >> pre ;
    cout<<"中序遍历为：" ;
	cin>> in;
    int len = strlen(pre);
    head = creat(pre, in, len);
    cout<<"该二叉树后序遍历为:";
    postorder(head);
    cout << endl;
    
        //	[2,1,3]
	Solution a;
    if(a.isValidBST(head)==1) cout<<"并不是二叉查找树";
    else cout<<"是二叉查找树";
    return 0;
}
