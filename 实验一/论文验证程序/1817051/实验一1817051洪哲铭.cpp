/*
	c++大二下学期实验一：递归与分治策略应用基础
	实验要求：
		1.认真领会分治法、递归技术方法提要,练习利用分治法分析问题、解决问程序,加深对它的理解
		2.阅读经典问题关键代码段,仔细领会算法、代码的异同。 
		3. 选定实验题目，仔细阅读实验要求，设计好输入输出，按照分治法的思想构思算法，选取合适的存储结构实现应用的操作。
		4. 实验要有详细的测试记录，包括各种可能的测试数据。
	选择问题： 最大子段和 
*/
#include<bits/stdc++.h>
using namespace std;
class Max_find{
	private:
		vector<int> data;//字段存储 
		int data_length=0;//字段长度 
	public:
		/*
			随机获取数据规模为length的数据 
		*/
		int set_data(int length, int high){
			int i;
			char mid;
			if(length <= 0){
				return -1;
			}
			srand((int)time(0));
			for(i=0;i<length;i++){
				data.push_back(rand()%(2*high+1)-high);
			}
			data_length=length;
			return 0;
		}
		/*
			展示容器内所有元素 
		*/
		void show_data(){
			int i;
			if(data_length==0){
				cout << "数组为空！" << endl;
			}else{
				cout << "data_length(" << data_length << "): ";
				for(i=0;i<data_length;i++){
					cout << data[i] <<" ";
				}
				cout << endl;
			}
		}
		/*
			清空容器内所有元素 
		*/
		void cut_data(){
			vector<int>().swap(data);
			data_length=data.size();
			cout << "删除完成!" << endl;
		}
		/*
			分治法 ：分为三种情况
			1、最大子段和在左边
			2、最大子段和在右边
			3、最大子段和在中间 
		*/
		int maxsum(int left, int right)
			{
			    int sum=0;
			    int center,leftsum,rightsum,s1,lefts,i,s2,rights,j;
			    if (left==right)        //如果序列长度为1，直接求解
			    {
			        if (data[left]>0)
			            sum=data[left];
			        else
			            sum=0;
			    }
			    else
			    {
			        center=(left+right)/2;		//划分
			        leftsum=maxsum(left, center);		//对应情况1，递归求解
			        rightsum=maxsum(center+1, right);		//对应情况2，递归求解
			        s1=0;
			        lefts=0;
					//以下对应情况3，先求解s1
			        for (i=center; i>=left; i--)
			        {
			            lefts+=data[i];
			            if (lefts>s1)
			                s1=lefts;
			        }
			        s2=0;
			        rights=0;             //再求解s2
			        for (j=center+1; j<=right; j++)
			        {
			            rights+=data[j];
			            if (rights>s2)
			                s2=rights;
			        }
			        sum=s1+s2;              //计算情况3的最大子段和
			        if (sum<leftsum)
			            sum=leftsum;
			        //合并，在sum、leftsum和rightsum中取较大者
			        if (sum<rightsum)
			            sum=rightsum;
			    }
			    return sum;
			}
		int get_length(){
			return data_length;
		}
		/*
			暴力匹配算法(三重循环匹配) 
		*/
		int test1(int length){
		    int thisSum, maxSum = 0, i, j, k;   
		    for (i = 0; i < length; i++){
		        for (j = 0; j < length; j++){
		            thisSum = 0;
		            for (k = i; k < j; k++){
		                thisSum += data[k];
		            }
		            maxSum = thisSum>maxSum ? thisSum : maxSum;
		        }
		    }
		    return maxSum;
		}
		/*
			暴力匹配方法(加if减少一次循环) 
		*/
		int test2(int length){
		    int thisSum, maxSum, i, j;
		    maxSum = 0;
		    for (i = 0; i < length; i++){
		        thisSum = 0;
		        for (j = i; j < length; j++){
		            thisSum += data[j];
		            if (thisSum>maxSum){
		                maxSum = thisSum;
		            }
		        }
		    }
		    return maxSum;
		}
		/*
			动态规划方法 
		*/
		int test4(int length){
		    int thisSum = 0, maxSum = 0;
		    int i;
		    for (i = 0; i < length; i++){
		        thisSum += data[i];
		        if (thisSum>maxSum){
		            maxSum = thisSum;
		        }
		        else if (thisSum < 0){
		            thisSum = 0;
		        }
		    }
		    return maxSum;
		}
};
int main(){
	Max_find f1;
	clock_t start_time, finish_time;
	/*小规模小数值测验*/
	f1.set_data(10,100);
	f1.show_data();
	start_time = clock();
	cout << "分治法答案为：" << f1.maxsum(0, f1.get_length()-1) << endl;
	finish_time = clock();
	cout << "分治法所需时间：" << finish_time - start_time << endl;
	start_time = clock();
	cout << "暴力匹配无if答案为：" << f1.test2(f1.get_length()-1) << endl;
	finish_time = clock();
	cout << "暴力匹配无if所需时间：" << finish_time - start_time << endl;
	start_time = clock();
	cout << "暴力匹配带if答案为：" << f1.test2(f1.get_length()-1) << endl;
	finish_time = clock();
	cout << "暴力匹配带if所需时间：" << finish_time - start_time << endl;
	start_time = clock();
	cout << "动态规划答案为：" << f1.test4(f1.get_length()-1) << endl;
	finish_time = clock();
	cout << "动态规划所需时间：" << finish_time - start_time << endl;
	f1.cut_data();
	/*大规模小数值测验*/
	f1.set_data(10000,100);
	f1.show_data();
	start_time = clock();
	cout << "分治法答案为：" << f1.maxsum(0, f1.get_length()-1) << endl;
	finish_time = clock();
	cout << "分治法所需时间：" << finish_time - start_time << endl;
	start_time = clock();
	cout << "暴力匹配无if答案为：" << f1.test2(f1.get_length()-1) << endl;
	finish_time = clock();
	cout << "暴力匹配无if所需时间：" << finish_time - start_time << endl;
	start_time = clock();
	cout << "暴力匹配带if答案为：" << f1.test2(f1.get_length()-1) << endl;
	finish_time = clock();
	cout << "暴力匹配带if所需时间：" << finish_time - start_time << endl;
	start_time = clock();
	cout << "动态规划答案为：" << f1.test4(f1.get_length()-1) << endl;
	finish_time = clock();
	cout << "动态规划所需时间：" << finish_time - start_time << endl;
	f1.cut_data();
	/*小规模大数值测验*/
	f1.set_data(10,20000);
	f1.show_data();
	start_time = clock();
	cout << "分治法答案为：" << f1.maxsum(0, f1.get_length()-1) << endl;
	finish_time = clock();
	cout << "分治法所需时间：" << finish_time - start_time << endl;
	start_time = clock();
	cout << "暴力匹配无if答案为：" << f1.test2(f1.get_length()-1) << endl;
	finish_time = clock();
	cout << "暴力匹配无if所需时间：" << finish_time - start_time << endl;
	start_time = clock();
	cout << "暴力匹配带if答案为：" << f1.test2(f1.get_length()-1) << endl;
	finish_time = clock();
	cout << "暴力匹配带if所需时间：" << finish_time - start_time << endl;
	start_time = clock();
	cout << "动态规划答案为：" << f1.test4(f1.get_length()-1) << endl;
	finish_time = clock();
	cout << "动态规划所需时间：" << finish_time - start_time << endl;
	f1.cut_data();
	/*大规模大数值测验*/
	f1.set_data(10000,20000);
	f1.show_data();
	start_time = clock();
	cout << "分治法答案为：" << f1.maxsum(0, f1.get_length()-1) << endl;
	finish_time = clock();
	cout << "分治法所需时间：" << finish_time - start_time << endl;
	start_time = clock();
	cout << "暴力匹配无if答案为：" << f1.test2(f1.get_length()-1) << endl;
	finish_time = clock();
	cout << "暴力匹配无if所需时间：" << finish_time - start_time << endl;
	start_time = clock();
	cout << "暴力匹配带if答案为：" << f1.test2(f1.get_length()-1) << endl;
	finish_time = clock();
	cout << "暴力匹配带if所需时间：" << finish_time - start_time << endl;
	start_time = clock();
	cout << "动态规划答案为：" << f1.test4(f1.get_length()-1) << endl;
	finish_time = clock();
	cout << "动态规划所需时间：" << finish_time - start_time << endl;
	f1.cut_data();
	return 0;
} 
