#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<windows.h>
using namespace std;

int Random(int left, int right)              //随机函数生成[left, righth]之间的一个整数
{
    srand((unsigned)time(NULL));
    int r = (rand() % (right-left+1))+ left;
    return r;
}
int Partition(int a[], int left, int right)
{
    int x = a[right];        //将输入数组的第rand个元素作为基准元，用它来对数组进行划分
    int i = left - 1;        //i为最后一个小于基准元的数的下标
    for (int j = left; j < right; j++)//遍历下标由left到right-1的数
    {
        if (a[j] < x)//如果数小于基准元的话就将i向前挪动一个位置，并且交换j和i所分别指向的数
        {
            int temp;
            i++;
            temp = a[i];
            a[i] = a[j];
            a[j] = temp;
        }
    }
//经历上面的循环之后下标为从left到i（包括i）的数就均为小于x的数了，现在将主元和i+1位置上面的数进行交换
    a[right] = a[i + 1];
    a[i + 1] = x;
    return i + 1;
}
//经历上面的之后下标为从left到i（包括i）的数就均为小于x的数了，而i+2到right的数则均不小于x
int RandommizedPartition(int a[], int p, int r)
{        
    int i = Random(p, r);        
    int temp = a[p];        
    a[p]=a[i];        
    a[i]=temp;       
    return Partition(a,p,r);
}
void RandommizedQuickSort(int a[], int left, int right)
{        
    if (left < right)        
    {                
        int q = RandommizedPartition(a, left, right);                
        RandommizedQuickSort(a, left, q - 1);                
        RandommizedQuickSort(a, q + 1, right);        
    }
}
int main()
{      
    double d;
    LARGE_INTEGER frequency,start,end;
    QueryPerformanceFrequency(&frequency);
	srand(time(NULL));//设置随机数种子。     
    int i, j, n;        
    cout<<"输入元素个数n:"<<endl;        
    cin>>n;       
	int a[n];           
    for(i=0;i<n;i++)               
        a[i]=rand()%100+1;  
	for(i=0;i<n;i++)                
        cout<<a[i]<<" ";  
	cout<<endl;  
	QueryPerformanceCounter(&start);      
    RandommizedQuickSort(a,0,n-1);  
	QueryPerformanceCounter(&end);     
    for(i=0;i<n;i++)                
        cout<<a[i]<<" ";   
	cout<<endl;
	d = (double)(end.QuadPart- start.QuadPart) /(double)frequency.QuadPart* 1000.0;
    cout<<d<<"s";           
    return 0;
}

