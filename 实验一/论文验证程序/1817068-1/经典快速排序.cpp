#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<windows.h>
using namespace std;
 
void swap(int i,int j,int arr[])
{
	int t;
	t=arr[i];
	arr[i]=arr[j];
	arr[j]=t;
}
 
void quicksort(int p,int q,int arr[])
{
	if(p>=q)return;
	int i=p,j=q+1,key=arr[p];
	while(1)
	{
		while(arr[++i]<key && i<q);
		while(arr[--j]>key);
		if(j<=i)
			break;
		swap(i,j,arr);
	}
	arr[p]=arr[j];
	arr[j]=key;
	quicksort(p,j-1,arr);
	quicksort(j+1,q,arr);
}
 
int main()
{
	double d;
    LARGE_INTEGER frequency,start,end;
    QueryPerformanceFrequency(&frequency);
	srand(time(NULL));//设置随机数种子。
	int n;
	cin>>n;
	int arr[n];
	for(int i=0; i<n; i++)
	{
		arr[i]=rand()%100+1;
	}
	for(int i=0; i<n; i++)
	{
		cout << arr[i] << " ";
	}	
	cout<<endl;
	QueryPerformanceCounter(&start); 
	quicksort(0,n,arr);
	QueryPerformanceCounter(&end);
	for(int i=0; i<n; i++)
	{
		cout << arr[i] << " ";
	}		
	cout<<endl;
	d = (double)(end.QuadPart- start.QuadPart) /(double)frequency.QuadPart* 1000.0;
    cout<<d<<"s";
	return 0;
}


