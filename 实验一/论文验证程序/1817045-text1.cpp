#include <iostream>
#include "time.h"
#define M 1024
using namespace std;
 int partition(float r[],int first,int end)
{
	int i=first,j=end;
	while(i<j)
	{
		while(i<j&&r[i]<=r[j])j--;
		if(i<j)
		{
			float temp=r[i];r[i]=r[j];r[j]=temp;
			i++;
		}
		while(i<j&&r[i]<=r[j])i++;
		if(i<j)
		{
			float temp=r[i];r[i]=r[j];r[j]=temp;
			j--;	
		}
	}
	return i;
}
void QuickSort(float r[],int first,int end)
{
	int pivot;
	if(first<end)
	{
		pivot=partition(r,first,end);
		QuickSort(r,first,pivot-1);
		QuickSort(r,pivot+1,end);
	}
}
int main()
{
	freopen("data.txt","r",stdin);
	int n,i;
	double star,finish;
		
	float r[10000];
	cout<<"输入元素的个数"<<endl;
	cin>>n;
	cout<<"输入元素 "<<endl;
	for(i=0;i<n;i++)
	{
		cin>>r[i];
	}	
	star=(double)clock();
	QuickSort(r,0,n-1);

	for(i=0;i<n;i++)
	{
		cout<<r[i]<<",";
	}
	cout<<endl;
	for(i=0;i<M;i++)finish=(double)clock();
	printf("The time is:%.2fms\n",(finish-star));
}
