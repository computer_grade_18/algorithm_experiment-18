#include <iostream>
#include <algorithm>
using namespace std;
template<typename  T>
void merge(T arr[], int l, int mid, int r) {
    T *aux = new T[r-l+1];
	for (int i = l; i <= r; i++)
		aux[i - l] = arr[i];
	int i = l, j = mid + 1;
	for (int k = l; k <= r; k++) {

		if (i > mid) {  // 如果左半部分元素已经全部处理完毕
			arr[k] = aux[j - l]; j++;
		}
		else if (j > r) {  // 如果右半部分元素已经全部处理完毕
			arr[k] = aux[i - l]; i++;
		}
		else if (aux[i - l] < aux[j - l]) {  // 左半部分所指元素 < 右半部分所指元素
			arr[k] = aux[i - l]; i++;
		}
		else {  // 左半部分所指元素 >= 右半部分所指元素
			arr[k] = aux[j - l]; j++;
		}
	}
}
template <typename T>
void mergeSort(T arr[], int n) {

	    for( int sz = 1; sz < n ; sz += sz )
	        for( int i = 0 ; i < n - sz ; i += sz+sz )
	            merge(arr, i, i+sz-1, min(i+sz+sz-1,n-1) );
}
int main()
{
    int n;
    cout<<"请输入数组长度"<<endl; 
    cin>>n;
	int arr[n];
	cout<<"请输入"<<n<<"个数组元素"<<endl; 
    for(int i=0;i<n;i++)
	cin>>arr[i];
	mergeSort(arr, n);
	cout<<"排序后的数组："<<endl;
	for (int i = 0; i < n; i++)
	{
		cout << arr[i] <<" ";
	}
	return 0;
}
