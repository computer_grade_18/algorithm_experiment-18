#include<iostream>
using namespace std; 
void merge(int a[],int l,int m,int r,int temp[])
{
	int i=l,j=m+1;
	int n=r;
	int k=0;
	while(i<=m&&j<=n) // 把较小的那个数据放到结果数组里， 同时移动指针
	{
		if(a[i]<a[j])
			temp[k++]=a[i++];
		else
			temp[k++]=a[j++];
	}
	while(i<=m)// // 如果a[i]还有元素，把剩下的数据直接放到结果数组
		temp[k++]=a[i++];
	while(j<=n)//如果a[j]还有元素，把剩下的数据直接放到结果数组
		temp[k++]=a[j++];
	for(i=0;i<k;i++) // 把结果数组 copy 到 a[l+i]里
		a[l+i]=temp[i];
}
void mergesort(int a[],int l,int r,int temp[])
{
	if(l<r)
	{
		int m=(l+r)/2;//中间元素下标 
		mergesort(a,l,m,temp);//左边进行排序 
		mergesort(a,m+1,r,temp);//右边进行排序 
		merge(a,l,m,r,temp);	
	}
}
int main()
{
	int a[10000];
	int temp[10000];
	int n;
	cout<<"输入数组长度："; 
	cin>>n;
	cout<<"输入数组元素："; 
	for(int i=0;i<n;i++)//输入数组 
		cin>>a[i];
	mergesort(a,0,n-1,temp);
	cout<<"排序后的数组："; 
	for(int i=0;i<n;i++)//输出数组 
		cout<<a[i]<<" ";
} 
