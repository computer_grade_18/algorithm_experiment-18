/*随机选择枢轴的位置，区间在low和high之间*/ 
int SelectPivotRandom(int arr[],int low,int high) 
{ 
  //产生枢轴的位置 
   srand((unsigned)time(NULL)); 
  int pivotPos = rand()%(high - low) + low;  
  //把枢轴位置的元素和low位置元素互换，此时可以和普通的快排一样调用划分函数 
  swap(arr[pivotPos],arr[low]); 
   return arr[low]; 
}