//二分查找（插值） 
#include <iostream>
using namespace std; 
int binarySearch(int A[],int left,int right,int x)
{
	int mid;
	while(left<=right)
	{
		if(A[right]==x) return right;
		mid=left+(x-A[left])/(A[right]-x)*(right-left); //根据上下界与x大小取中点 
		if(A[mid]==x) return mid;
		else if(A[mid]>x) right=mid-1;
		else left=mid+1;
	}
	return -1;
} 
int main()
{
	int n,x;
	cout<<"请输入数组个数"<<endl;
	cin>>n;
	int A[n];
	cout<<"请输入有序数组"<<endl;
	for(int i=0;i<n;i++)
	cin>>A[i];
	cout<<"请输入查询的数"<<endl;
	cin>>x;
	cout<<binarySearch(A,0,n-1,x);
	return 0;
}
