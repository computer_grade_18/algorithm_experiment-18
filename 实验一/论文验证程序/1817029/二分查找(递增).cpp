#include<iostream>
using namespace std;
//A[]为递增序列，x为欲查询数，函数返回第一个等于x的元素位置 
int binarySearch(int A[],int left,int right,int x)
{
	int mid;
	while(left<right)
	{
		mid=left+(right-left)/2;//取中点，防止left+right超过int溢出 
		if(A[mid]>=x) right=mid;//往左子区间[left,mid]查找 
		else left=mid+1;//往右子区间[mid+1,right]查找 
	}
    if(A[left]==x) return left;//找到x，返回下标 
    else return -1;//查找失败,返回-1 
} 
int main()
{
	int n,x;
	cout<<"请输入数组个数"<<endl;
	cin>>n;
	int A[n];
	cout<<"请输入有序数组"<<endl;
	for(int i=0;i<n;i++)
	cin>>A[i];
	cout<<"请输入查询的数"<<endl;
	cin>>x;
	cout<<binarySearch(A,0,n,x);
	return 0;
}
