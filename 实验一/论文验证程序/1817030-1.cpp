#include<iostream>
#include <stdio.h>
#include<time.h>
#include<math.h>
#include<stdlib.h>
#define N 10 
using namespace std;
string isright(int *arr,int n)
{
	for(int i=0;i<n;i++)
	{
		if(arr[i]>arr[i+1]){
		return "false";break;}
		else return "true";
	}
}
//朴素版 
    void quickSort(int *arr,int left,int right)  
    {
    	if (left >= right) return; 
    	int le= left;
    	int ri= right;
    	int key = arr[left]; 
    	while (le<ri)   
    	{
    		while (le<ri && arr[ri] >= key)
    		{	
    			ri--;	
    		}
    		arr[le] = arr[ri];
    		while (le<ri&& arr[le] <= key)
    		{
    			le++;
    		}
    		arr[ri] = arr[le];
    	}
    	arr[le] = key; 
     
    	quickSort(arr,left,le- 1);
    	quickSort(arr,le+1,right);
    	
    }
    void quick_sort(int *arr, int len)
    {	
    	quickSort(arr,0,len-1);	
    }

//三点中值法
int partition(int *a,int p,int r){
    //选取一个基准点 
    int midIndex= p+((r-p)>>1);//中间下标
    int midValue=-1;//中间值下标
    if(a[p]<=a[midIndex]&&a[p]>=a[r]) midValue=p;
    else if(a[r]<=a[midIndex]&&a[r]>=a[p]) midValue=r;
    else midValue=midIndex;
    swap(a[midValue],a[p]);
    
    int x=a[p];//基准点
    int l=p+1,q=r;
    while(l<=q){
        while(a[l]<=x&&l<=q) ++l;
        while(a[q]>x&&l<=q) --q;
        if(l<=q) swap(a[l],a[q]);
     }
     swap(a[p],a[q]);
     return q;
} 
void partition1(int *a,int n)
{
	partition(a,0,n-1);
}

//绝对中值法
void insertsort(int a[],int p,int q){//插入排序 
    int temp;
    for(int i=p;i<=q;i++){
        if(a[i]<a[i-1]){
            temp=a[i];
            for(int j=i;j>=0;j--){
                if(j>0&&a[j-1]>temp) a[j]=a[j-1];
                else {
                    a[j]=temp;
                    break;
                }
            }
        }
    }
}
int getmid(int a[],int p,int r){
    int length=r-p+1;
    int grouplength=(length%5==0)?(length/5):(length/5+1);//每五个元素一组，得到划分的个数 
    int *medians=new int[grouplength];//定义存储中间值的数组
    int midIndex=0;
    for(int i=0;i<grouplength;i++){
        if(i==grouplength-1){//最后一组时 ,需要单独处理，因为这组中元素个数小于5个 
            insertsort(a,p+i*5,r);//调用插入排序 
            medians[midIndex++]=a[(p+i*5+r)/2];//存入数组 
        }else {
            insertsort(a,p+i*5,p+i*5+4);
            medians[midIndex++]=a[(p+i*5+2)];
        }
    } 
    insertsort(medians,0,grouplength-1);
    return medians[grouplength/2];
}
void insertso(int *a,int n)
{
	getmid(a,0,n-1);
 } 
 
 //+直接插入排序 
 void quicksort(int a[],int l,int r){//快排递归形式 
    if(l<r){
        if((r-l+1)<=8) insertsort(a,l,r);//调用插入排序
        else{
            int q=partition(a,l,r);//找到中间数,不一定是中位数 
            quicksort(a,l,q-1);
            quicksort(a,q+1,r);            
        } 
    }
}
void quick(int *a,int n)
{
	quicksort(a,0,n-1);
}
int main()
{int i=0;
    int arr[N];
    for(i=0;i<N;i++)
    {
    	arr[i]=rand()%300 ;
	}cout<<"随机初始数组：";
	for(int j=0;j<N;j++)
	{
		cout<<arr[j]<<" ";
	}cout<<endl<<endl;
	int times1,times2,times3,times4;
	time_t start1,end1;double Time1;
	start1=(double)clock();
	quick_sort(arr,N);
	times1=(double)clock();

   	for(i=0;i<N;i++)
   	 {
   	  cout<<arr[i]<<" "; 
  	 }
  	 end1=(double)clock();
	 cout<<endl<<"验证结果："<<isright(arr,N);
	 Time1=(double)(end1-start1)/CLK_TCK;
  	 cout<<endl<<"普通快排使用时间："<<Time1<<endl;
  	 cout<<"比较次数："<<times1<<endl;
  	//f法2 
  	cout<<endl;
  	 time_t start2,end2;double Time2;
  	  start2=(double)clock();
  	  partition1(arr,N);
  	  times2=(double)clock();
  	     	for(i=0;i<N;i++)
   	 {
   	  cout<<arr[i]<<" "; 
  	 }
  	 end2=(double)clock();
  	 cout<<endl<<"验证结果："<<isright(arr,N);
	 Time2=(double)(end2-start2)/CLK_TCK;
  	 cout<<endl<<"三点中值法快排使用时间："<<Time2<<endl;
  	 cout<<"比较次数："<<times2<<endl;	 
  	 
	   //f法3
	cout<<endl;
	  time_t start3,end3;double Time3;
  	  start3=(double)clock();
  	  insertso(arr,N);
  	  times3=(double)clock();
  	     	for(i=0;i<N;i++)
   	 {
   	  cout<<arr[i]<<" "; 
  	 }
  	 end3=(double)clock();
  	 cout<<endl<<"验证结果："<<isright(arr,N);
	 Time3=(double)(end3-start3)/CLK_TCK;
  	 cout<<endl<<"绝对中值法快排使用时间："<<Time3<<endl;
  	 cout<<"比较次数："<<times3<<endl; 
  	 
  	 
  	 //f法4
	  cout<<endl; 
	  time_t start4,end4;double Time4;
  	  start4=(double)clock();
  	  quick(arr,N);
  	  times4=(double)clock();
  	     	for(i=0;i<N;i++)
   	 {
   	  cout<<arr[i]<<" "; 
  	 }
  	 end4=(double)clock();
  	 cout<<endl<<"验证结果："<<isright(arr,N);
	 Time4=(double)(end4-start4)/CLK_TCK;
  	 cout<<endl<<"快排+直插使用时间："<<Time4<<endl;
  	 cout<<"比较次数："<<times4<<endl; 
   return 0;	
}
