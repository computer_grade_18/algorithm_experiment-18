#include <iostream>
using namespace std;
void P(int a[],int n)
{
    for(int i=0; i<n; i++)
        cout<<a[i]<<" ";
    cout<<endl;
}//将排列的数打印出来 

int qp(int s[], int l, int r){
                        
    int i = l, j = r, x = s[l]; //取x为中间值，将最左边的值赋给x 
    while (i < j)
    {
      
        while(i < j && s[j] >= x)//与x比较  
            j--;
        if(i < j)
            s[i++] = s[j]; //将比x大的值放在x的右边 
        
      
        while(i < j && s[i] <= x)
            i++;
        if(i < j)
            //将比x小的值放在x的左边 
            //最左元素一定被覆盖过，若没有，则表明右侧所有元素都>x，那么算法将终止
            s[j--] = s[i];
    }
    s[i] = x;  //第i个值为x，其左侧都小于x，右侧都大于x
    return i;
}

void qs(int s[], int l, int r)
{
    //数组左边值<右边值才能继续
    if (l>=r){
        return;
    }
    
    int i = qp(s, l, r);
    
    
    qs(s, l, i - 1);//递归处理 
    qs(s, i + 1, r);
}

int main()
{
    int a[]= {32,-6,77.3,99,99,63,71,48,25};

    P(a,10);
    qs(a,0,9);
    P(a,10);
    return 0;
}
