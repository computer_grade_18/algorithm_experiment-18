//折半查找非递归 
//1817010冯勇杰 
#include<iostream>
using namespace std; 
int binarysearch2( int array[], int low, int high,int k) {
int mid=(low + high)/2;
	while (low<= high) {
		if (k< mid) 
		{
		  high = mid - 1;
		}
		else if (k>mid) 
		{
			low = mid + 1;
		}
		else 
		{
			return mid;
		}
	}
	return -1;
}
int main()
{
int array[17]={1,4,5,7,8,9,10,12,15,18,19,22,24,27,30,32,35};
cout<<"原来地数列为:";
for(int i=0;i<17;i++)
{cout<<array[i]<<" ";
}
cout<<endl;
int a,n;
cout<<"请输入要查找的数："; 
	cin>>n;
	a=binarysearch2(array,0,16,n);
	if(a!=-1)
	cout<<"这个数在第"<<a<<"个位置";
	else
	cout<<"未找到该数。";
	return 0;
}
