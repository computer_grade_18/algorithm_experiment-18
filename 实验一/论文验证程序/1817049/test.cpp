#include<iostream>
#include <cstring>
using namespace std;
   typedef struct TreeNode
{
    char val;
    struct TreeNode *left, *right;
}TreeNode;
class Solution {
public:
    TreeNode* last = NULL, *node1, *node2;
    bool flag=false;
    void DFS(TreeNode* root){
        if(!root) return;
        DFS(root->left);
        if(last && root->val<=last->val){
            if(!flag) node1=last, node2=root, flag=true;
            else node2 = root;
        }
        last = root;
        DFS(root->right);
    }

    void recoverTree(TreeNode* root) {
        DFS(root);
        int t=node1->val;
        node1->val = node2->val;
        node2->val = t;
    }
};

 
TreeNode *creat(char *pre, char *in, int len)    //?????????
{
    int k;
    if (len <= 0)
        return NULL;
    TreeNode *head; 
    head=new TreeNode;
    head -> val = pre[0];
    char *p;
    for (p = in; p != NULL; p++){
        if (*p == *pre)    //??????????????????? 
            break;
    }
    k = p - in;
    head -> left = creat(pre + 1, in, k);    //????????? 
    head -> right = creat(pre + k + 1, p + 1, len - k - 1);    //?????
    return head;
}
 void postorder(TreeNode *root)  //后序遍历树输出
{
    if(root!=NULL)
    {
        postorder(root->left);
        postorder(root->right);
        cout<<root->val;           
    }
    else
    return;
}
int main()
{
	 //TreeNode a[5]=[1,3,null,null,2]; 
	  char pre[30], in[30]; Solution a;     //????????????
    TreeNode *head;
    head=new TreeNode;
    cout<<"二叉树的先序遍历：" ;
    cin >> pre;
    cout<<"二叉树的中序遍历：";
	cin >> in;
	cout<<"二叉树的后序遍历：";
    int len = strlen(pre);
    head = creat(pre, in, len);
    postorder(head);
    cout << endl;
	a.recoverTree(head);
	cout<<"恢复后的搜索二叉树的后序遍历为：" ; 
	postorder(head);
	return 0;
}
