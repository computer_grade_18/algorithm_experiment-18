#include<iostream>
#include<vector>
#include<math.h>
#include<time.h>
#include<sys/timeb.h>
#include<Windows.h>
#include <algorithm>
#include<cstdlib>
#include<ctime>
//#include<RandomNumber.h> 
using namespace std;
class RandomNumber                  //随机数
{
	public:
		RandomNumber(){
			srand(time(0));
		}
		unsigned random(int,int);
	private:
		
};
class Solution
{
public:
    int maxSubArray(vector<int> &nums)
    {
        int result = INT_MIN;
        int numsSize = int(nums.size());

        vector<int> dp(numsSize);
        dp[0] = nums[0];
        result = dp[0];
        for (int i = 1; i < numsSize; i++)
        {
            dp[i] = max(dp[i - 1] + nums[i], nums[i]);
            result = max(result, dp[i]);
        }

        return result;
    } 
 /*      int maxSubArray(vector<int> &nums)
    {
        int result = INT_MIN;
        int numsSize = int(nums.size());
        int dp(nums[0]);   //dp[i] 用来记录以nums[i]结尾的最大子段和
        result = dp;
        for (int i = 1; i < numsSize; i++)
        {
            dp = max(dp + nums[i], nums[i]);        //比较大小
            result = max(result, dp);
        }

        return result;
    }
*/ 
};
int main()
{
	DWORD start1,end1;
	start1 = GetTickCount();
	srand((unsigned)time(0));
	 
/*	int b[]={1,2,3,1,2,3};    //情况一:输入的元素全部为正整数 
	vector<int> a(b,b + 6); */
/*	int b[]={-1,-2,-3,-1,-2,-3};    //情况二:输入的元素全部为负整数
	vector<int> a(b,b + 6); */
/*	int b[]={-1,2,3,1,-2,-3};    //情况三:输入的元素包含正整数与负整数
	vector<int> a(b,b + 6); */
/*	int b[]={};    //情况四:输入的数据为空
	vector<int> a(b,b + 0); */
/*	vector<int> a;				//	情况五:输入的数据量极大
	
	for(int i = 0;i < 1000000;i++)
	{
		a.push_back(i);
	}*/ 
	vector<int> a;				// 情况六:随机数
	RandomNumber number;
	for(int i = 0;i < 100000000 ;i++)
	{	
		int b;
		b = number.random(-10,10);
	//	cout<<b<<" ";
		a.push_back(b);
	}
	cout<<endl;
	
	Solution c;
	int d;
	d = c.maxSubArray(a);
	cout<<d<<endl;
	
	end1 = GetTickCount();
	cout<<"time(ms) "<<end1 - start1<<endl;
	
	return 0;	
} 
