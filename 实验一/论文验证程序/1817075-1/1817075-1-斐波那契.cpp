#include <iostream>
#include <vector>
using namespace std;
const int MAX_SIZE = 20;
int a[] = { 1, 10, 255, 30, 49, 89, 100, 120, 230, 310, 880};
void Fibonacci(int F[])
{
    F[0] = 0;
    F[1] = 1;
    for (size_t i = 2; i < MAX_SIZE; i++)
        F[i] = F[i - 1] + F[i - 2];
     
}
 
int FibonacciSearch(int value)
{
    int F[MAX_SIZE];
    Fibonacci(F);
    int n = sizeof(a) / sizeof(int);
 
    int k = 0;
    while (n > F[k] - 1)
        k++;
    vector<int> temp;
    temp.assign(a, a + n);
    for (size_t i = n; i < F[k] - 1; i++)
        temp.push_back(a[n - 1]);
 
    int l = 0, r = n - 1;
    while (l <= r)
    {
        int mid = l + F[k - 1] - 1;
        if (temp[mid] < value){
            l = mid + 1;
            k = k - 2;
        }
        else if (temp[mid] > value){
            r = mid - 1;
            k = k - 1;
        }
        else{
            if (mid < n)
                return mid;
            else
                return n - 1;
        }
    }
    return -1;
}
 
int main()
{  
    int F;
    cin>>F;
    int index = FibonacciSearch(F);
    cout <<"该数在第"<< index <<"个位置"<< endl;
 
}
