#include <iostream>
using namespace std;
int BinarySearch(int arr[],int low,int high,int key){
	int i=low,j=high,mid;
	while(i < j) {  
        mid = i+(j-i>>1);  
        if(arr[mid] >= key) {//关键点：并不是等于就直接退出循环  
            j = mid;  
        } else {  
            i = mid+1;  
        }  
    }  
    if(arr[i] == key) {  
        return i;   
    } else {  
        return -1;    
    }         
}  
int main()
{
	int arr[]={1,2,9,9,9,9,9,9,9,10};
	int key,n;
	cin>>key;
	n=BinarySearch(arr,0,9,key)+1;
	cout<<"该数在第"<<n<<"个位置"<<endl; 
}
 
