#include<iostream>
using namespace std;

int AdjustArray(int s[], int l, int r) 
{
       int i = l, j = r;
       int x = s[l]; 
       while (i < j)
       {
            
              while(i < j && s[j] >= x)
                     j--; 
              if(i < j)
              {
                     s[i] = s[j]; 
                     i++;
              }
 
        
              while(i < j && s[i] < x)
                     i++; 
              if(i < j)
              {
                     s[j] = s[i]; 
                     j--;
              }
       }
      
       s[i] = x;
 
       return i;
}
 

void quick_sort(int s[], int l, int r)
{
    if (l < r)
    {
             
        int i = l, j = r, x = s[l];
        while (i < j)
        {
            while(i < j && s[j] >= x)
                            j--; 
            if(i < j)
                            s[i++] = s[j];
                    
            while(i < j && s[i] < x) 
                            i++; 
            if(i < j)
                            s[j--] = s[i];
        }
        s[i] = x;
        quick_sort(s, l, i - 1);
        quick_sort(s, i + 1, r);
    }
}
int main(){
    int array[]={72,6,57,88,60,42,83,73,48,85};
    quick_sort(array,0,9);
    for(int i = 0 ; i <= 9 ; i++)
        cout<<array[i]<<" ";
    cout<<endl;
    return 0;
 }
