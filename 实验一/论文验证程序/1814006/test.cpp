#include <iostream>
#include<stdlib.h>
#include <time.h>
#define random(x) (rand()%x)
using namespace std;
//基本的归并算法
int jiben2(int b[],int low,int mid,int high)//将两个有序段二路并归为一个有序段 
{
	int tmp[high-low+1],i=low,j=mid+1,k=0,m=0; 
	while(i<=mid&&j<=high) 
	if(b[i]<b[j]) 
	{
		tmp[k]=b[i];
		i++;k++;m++;
	}
	else
	{
		tmp[k]=b[j];
		k++;j++;m++;
	}
	while(i<=mid)
	{
		tmp[k]=b[i];
		i++;k++;
	}
	while(j<=high)
	{
		tmp[k]=b[j];
		k++;j++;
	}
	for(k=0,i=low;i<=high;k++,i++)
	{
		b[i]=tmp[k];
	}
	delete []tmp;
	return m;
}
int jiben1(int b[],int len,int length)
{
	int m=0,i;
	for(i=0;i+2*len-1<length;i=i+2*len)//归并len长的两个相邻子表 
	m+=jiben2(b,i,i+len-1,i+2*len-1);
	if(i+len-1<length)//余下两个子表，后者的长度小于len 
	m+=jiben2(b,i,i+len-1,length-1);//归并这两个子表 
	return m;
} 
int jiben(int b[],int length)//对b[0...n-1]按递增进行二路并归算法 
{
	int m=0,len;
	for(len=1;len<length;len=2*len)//进行[log2n]趟排序 
	m+=jiben1(b,len,length);
	return m;
} 


//二路归并算法
int merge2(int a[],int l,int mid,int r)//归并
{
	int n=r-l+1,T=0;//临时数组存合并后的有序序列
    int* tmp=new int[n];
    int i=0;
    int left=l;
    int right=mid+1;
    while(left<=mid && right<=r)
        {
        	tmp[i++] = a[left]<= a[right]?a[left++]:a[right++];
        	T++; 
        }
    while(left<=mid)
        tmp[i++]=a[left++];
    while(right<=r)
        tmp[i++]=a[right++];
    for(int j=0;j<n;++j)
        a[l+j]=tmp[j];
    delete [] tmp;//删掉堆区的内存
    return T; 
} 
int T2=0;
int mergesort2(int a[],int l,int r)//分治
{
	if(l==r)return 0;
	int mid=l+(r-l)/2;
	mergesort2(a,l,mid);
	mergesort2(a,mid+1,r);
	T2+=merge2(a,l,mid,r);
	return T2; 
}  


//改进后的算法 
int insertsort(int a[],int length)//直接插入排序 
{
	int i=1,j,tmp,t=0;
	while(i<length)
	{
		if(a[i]<a[i-1])
		{
			t++;
			tmp=a[i];j=i-1;
			do
			{
				a[j+1]=a[j];
				j--;
				tmp++;
			}while(j>=0&&a[j]>tmp);
			a[j+1]=tmp;
		}
		i++;
	}
	return t;
}
int merge(int a[],int l,int mid,int r)//归并
{
	int n=r-l+1,T=0;//临时数组存合并后的有序序列
	if(n<10)
	{
		T+=insertsort(a,n);
	}
    int* tmp=new int[n];
    int i=0;
    int left=l;
    int right=mid+1;
    while(left<=mid && right<=r) 
        {
        	tmp[i++] = a[left]<= a[right]?a[left++]:a[right++];
        	T++; 
        }
    while(left<=mid)
        tmp[i++]=a[left++];
    while(right<=r)
        tmp[i++]=a[right++];
    for(int j=0;j<n;++j)
        a[l+j]=tmp[j];
    delete [] tmp;//删掉堆区的内存
    return T; 
} 
int T1=0;
int mergesort(int a[],int l,int r)//分治
{
	if(l==r)return 0;
	int mid=l+(r-l)/2;
	mergesort(a,l,mid);
	mergesort(a,mid+1,r);
	if(a[mid]>a[mid+1]) // 如果原来就有序就不用归并 
	T1+=merge(a,l,mid,r);
	return T1; 
} 
int main(){
    int n,times,times1,times2;
    cout<<"输入产生随机数个数"<<endl;
	cin>>n; 
    int a[n],b[n],c[n]; 
	cout<<"随机产生的数组 : "; 
    for(int i=0;i<n;i++)
    {
   	   a[i]=random(1000);
   	   b[i]=a[i];
   	   c[i]=a[i];
   	   cout<<a[i]<<" ";
    }
    time_t start1,end1;
    double Time1;
    start1=(double)clock();
    times1=mergesort(a,0,n-1);
	cout<<endl<<endl<<"改进后的并归排序:"; 
    for(int i=0;i<n;i++)
    cout<<a[i]<<" ";
    end1=(double)clock();
    Time1=(double)(end1-start1)/CLK_TCK;
    cout<<endl<<"归并排序所用时间:"<<Time1<<endl; 
    cout<<"归并排序的比较次数:"<<times1; 
    
    time_t start2,end2;
    double Time2;
    start2=(double)clock();
    times2=mergesort2(c,0,n-1);
	cout<<endl<<endl<<"二路并归排序:"; 
    for(int i=0;i<n;i++)
    cout<<c[i]<<" ";
    end2=(double)clock();
    Time2=(double)(end2-start2)/CLK_TCK;
    cout<<endl<<"归并排序所用时间:"<<Time2<<endl; 
    cout<<"归并排序的比较次数:"<<times2; 
    
    time_t start,end;
    double Time;
    start=(double)clock();
    times=jiben(b,n);
	cout<<endl<<endl<<"基本的并归排序:"; 
    for(int i=0;i<n;i++)
    cout<<b[i]<<" ";
    end=(double)clock();
    Time=(double)(end-start)/CLK_TCK;
    cout<<endl<<"归并排序所用时间:"<<Time<<endl; 
    cout<<"归并排序的比较次数:"<<times; 
}

