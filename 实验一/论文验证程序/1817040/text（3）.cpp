//非递归快排（非栈） 
#include<iostream>
#include <stdlib.h>
#include<time.h>
#include<windows.h>
#define Maxsize 100
using namespace std;
void quicksort2(int a[],int n)
{
    int i,j,low,high,temp,top=-1;
    struct node
  {
    int low,high;
  }st[Maxsize]; 
    top++;
    st[top].low=0;
	st[top].high=n-1;
    while(top>-1)
  {
    low=st[top].low;
	high=st[top].high;
    top--;
    i=low;
	j=high;
    if(low<high)       
  {
    temp=a[low];
    while(i!=j)
  {
    while(i<j&&a[j]>temp)
	    j--;
    if(i<j)
	{
	a[i]=a[j];
	    i++;
	}
    while(i<j&&a[i]<temp)
	    i++;
    if(i<j)
	{
	    a[j]=a[i];
	    j--;
	}
  } //处理一次  即将比基准小的全部放左边 比基准大的放右边
    a[i]=temp;
    if(low>i-1)    //重新定位low和high 
    {
	top++;
	st[top].low=low;
	st[top].high=i-1;
    }
    if(high>=i+1)
    {
    top++;
	st[top].low=i+1;
	st[top].high=high;
	}
    
  }
  }
  }
int main(void)
{
	LARGE_INTEGER frequency,start,end;
    QueryPerformanceFrequency(&frequency);
    double d;
    int n;
    cout<<"输入数组个数：";
	cin>>n; 
    int a[n];
    cout<<"输入数组：";
    for(int i=0;i<n;i++)
    {
	    cin>>a[i];
	} 	
    QueryPerformanceCounter(&start);
    quicksort2(a,n);  
    QueryPerformanceCounter(&end);
    cout<<"输出：";                //输出
	for(int i=0;i<n;i++)
	cout<<a[i]<<" ";
    cout<<endl;
    d = (double)(end.QuadPart- start.QuadPart) /(double)frequency.QuadPart* 1000.0; 
    cout<<d<<"s"<<endl;
    cout<<endl;
    return 0; 
}
