//分治法-快速排序 
#include<iostream>
#include <stack>
#include"stdio.h"
#define Maxsize 100
#include<cstdlib>
#include<math.h>
#include<windows.h>
#include<time.h>
using namespace std;
int swap(int a[],int i,int j)  //元素间的交换 
{
	int temp=a[i];
	a[i]=a[j];
	a[j]=temp;
}

int partition1(int a[],int p,int q) //分区算法的设计
{   
	int x=a[p];
	int i=p,j;
	for(j=p+1;j<=q;j++)
	{
		if(a[j]<=x)
		{
			i++;
			swap(a,i,j);
		}
	}
	swap(a,p,i);   
	return i;
}
//经典快速排序 
void quicksort1(int a[],int p,int q)  
{
	if(p<q)
	{   
		int r=partition1(a,p,q);
		quicksort1(a,p,r-1);
		quicksort1(a,r+1,q);
	}  
}
//随机基准
void quicksort2(int a[],int p,int q) 
{
	if(p<q)
	{   srand(unsigned(time(NULL)));      //随机函数种子，使随机基准不重复 
        int randNum=rand()%(q-p+1)+p;   //随机基准 
        swap(a,randNum,p);
		int r=partition1(a,p,q);
		quicksort2(a,p,r-1);
		quicksort2(a,r+1,q);
	}  
}
//栈实现的非递归快速排序 
int Partition2( int a[], int low, int high )  
{
	int point = a[low];
	 
	while( low<high )
	{
		while( low<high && a[high]>=point )
		--high;
		a[low] = a[high];  
		while( low<high && a[low]<=point )
		++low;
		a[high] = a[low];  
	}
	
	a[low] = point;  //point最终位置输出 
	return low;
}
	void quicksort3(int a[], int left, int right)//手动入栈通过栈来存储每次分块快排的起始点即基准 
{
	stack<int> st;
	if( left<right )
	{
		int border = Partition2(a,left,right);
		
		if( border-1>left ) //使左分区存在 
		{
			st.push(left);     //将左分区端点入栈 
			st.push(border-1);
		}
		if( border+1<right ) //使右分区存在 
		{
			st.push(border+1); //将右分区端点入栈 
			st.push(right);
		}
		
		while( !st.empty() )
		{ 
			int r = st.top();
			st.pop();  
			int l = st.top();
			st.pop();
			
			border = Partition2(a,l,r);
			if( border-1>l )        //将左分区端点入栈
		    {  
		    	st.push(l);
		    	st.push(border-1);
	    	}
		    if( border+1<r )  
	     	{                            //将右分区端点入栈
	 	    	st.push(border+1);
	    		st.push(r);
	    	}
		}
	}
}
int main(void) 
{   
    LARGE_INTEGER frequency,start,end;
 QueryPerformanceFrequency(&frequency);
    srand((unsigned)time(NULL)); //产生随机数种子，避免产生的随机数相同
    double d;
    int a[200000],b[200000]; 
	int n;  
	cout<<"抽取随机元素的个数：";
	cin>>n; 
    for(int i=0;i<n;i++)              //将数组初始化 
	{
		b[i]=rand();
	}
 for(int i=0;i<n;i++)              //数组初始化 
	{
		a[i]=b[i];
	} 
	for(int i=0;i<n;i++) 
	cout<<a[i]<<" "; 
	cout<<endl;
QueryPerformanceCounter(&start);
quicksort1(a,0,n-1); 
 QueryPerformanceCounter(&end);
 d = (double)(end.QuadPart- start.QuadPart) /(double)frequency.QuadPart* 1000.0; 
 cout<<"经典快速排序："<<d<<"毫秒"<<endl;	
 for(int i=0;i<n;i++)              //将数组初始化 
	{
		a[i]=b[i];
	} 
 QueryPerformanceCounter(&start);
 quicksort2(a,0,n-1);   //随机基准 
 QueryPerformanceCounter(&end);
 d = (double)(end.QuadPart- start.QuadPart) /(double)frequency.QuadPart* 1000.0; 
	cout<<endl;
 cout<<"随机基准的快速排序："<<d<<"毫秒"<<endl;
  cout<<endl;  
for(int i=0;i<n;i++)              //初始化数组
	{
		a[i]=b[i];
	} 
QueryPerformanceCounter(&start);
quicksort3(a,0,n-1);  
 QueryPerformanceCounter(&end);
 d = (double)(end.QuadPart- start.QuadPart) /(double)frequency.QuadPart* 1000.0; 
 cout<<"用栈实现的非递归快速排序："<<d<<"毫秒"<<endl;
  cout<<endl; 
  return 0;
}
