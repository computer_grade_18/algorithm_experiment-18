#include <iostream>
     
using namespace std;
     
#define length 100
     
//让小于某个值的所以数排在它前面，所有大于它的数据排后面
void swap(int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}
     
int spite(int *data, int low, int high)
{
    int i = low, j = high;
    	//参考元素用第一个数组的元素
    int mid = data[i];
    	//大循环的条件
    while(i < j) {
    		//右边的指针左移动,既然要移动，肯定需要用到循环
    	while (i < j && mid < data[j]) --j;
    		//这个时候，发现data[j] < mid，所以需要交换数据
    	if (i < j) 
    	{
    		swap(data[j], data[i]);
    		//交换完成之后需要i++,因为我当前的下标从左往右移，当前的就不算了
    		++i;
    	}
    	//左边的指针往右边移动，既然需要用到移动，就要循环
    	while (i < j && mid >= data[i]) ++i;
    	if (i < j) 
    	{
   			swap(data[i], data[j]);
    		//下次从右边往左边移动的下标需要在当前坐标向左移动一下
    		--j;
    	}
   	}
   	return i;
   }
   void quickSort(int *data, int low, int high)
    {
    	if (low < high)
    	{
    		int mid = spite(data, low, high);
    		quickSort(data, low, mid - 1);
    		quickSort(data, mid + 1, high);
    	}
    }
int main()
{
   	int data[length], all;
   	std::cout << "请输入数组元素的个数" << std::endl;
   	std::cin >> all;
   	if (all <= 0)
   	{
   		std::cout << "输入数据的个数不能小于等于0" << std::endl;
   	return -1;
   	}
   	std::cout << "请输入数组的每个元素值" << std::endl;
   	for (int i = 0; i < all; ++i)
   	{
   		std::cin >> data[i];	
   	}
   	quickSort(data, 0, all - 1);
   	//打印数据
   	for (int i = 0; i < all; ++i)
   	{
   	std::cout << data[i] << "\t";	
   	}
   	std::cout << std::endl;
   	return 0;	
	}

