/*

// 动态规划 
#include<bits/stdc++.h>
using namespace std;
int main()
{
	int k;
	cout<<"序列长度:";
	cin>>k;
	int *a = new int [k];
	cout<<"输入整数序列:";
	for(int i = 0;i<k;i++)	
		cin>>a[i];
		int maxsum = 0;
		int sum;
		for(int i = 0;i<k;i++)
		{
			sum = sum +a[i]	;//向右累加
			if(sum < 0)
				sum = 0;	// 如果当前子序列和为负，不可能使后面的部分和增大，舍去 
			maxsum = max(sum,maxsum);	//更新最大值 
		}
		cout<<maxsum<<endl;
		delete []a;
}*/


//O(n)算法 
/* 
#include<iostream>
const int MAX_LEN =100;

using namespace std;

int getMaxSum(int* a,int n){
    int max=0;//假设最大值为0
    int temp=0;
    for(int i=0;i<n;i++){
        if(temp>0){
            temp+=a[i];//最大值临时变量只有大于零，才可能越加越大
        }else{
            temp=a[i];////最大值临时变量只有小于零，直接等于a[i],否则越加越小
        }
        if(max<temp){
            max=temp;//判断赋值
        }
    }
    return max;
}

int main(){
    int a[MAX_LEN];
    int maxSum;
    int n;
	cout<<"请输入序列的元素个数"<<endl;
    cin>>n;
	cout<<"请依次输入序列的元素"<<endl;
    for(int i=0;i<n;i++){
        cin>>a[i];
    }
    maxSum=getMaxSum(a,n);
    cout<<maxSum<<endl;
}  
/*这个算法的效率非常的高，又被称为在线处理算法，算法只需要扫描一遍序列，
就能找到最大的子序列和，它的技巧就是一旦A[i]被读入并被处理，它就不再需要被记忆。
不仅如此，在任意时刻，算法都能够对它已经读入的数据给出正确的答案。具有这种特性的算法叫做联机算法。
仅需要常量的空间并以线性时间运算的联机算法集合是完美的算法 /* 

*/

//O（n^3) 
/*
#include<iostream>
#include<stdio.h>
using namespace std;

int MaxSubsequenceSum(int a[],int n);

int main(){
    //int a[6] = {-2, 11, -4, 13, -5, -2};
    int a[8] = {4, -3, 5, -2, -1, 2, 6, -2};
    printf("%d\n",MaxSubsequenceSum(a,8));
}

int MaxSubsequenceSum(int a[],int n){
    int ThisSum, MaxSum;
    MaxSum = 0;
    for(int i = 0; i < n; i++){
        for(int j = i; j < n; j++){
            ThisSum = 0;
            for(int k = i; k <= j; k++){
                ThisSum += a[k];
            }
            if(ThisSum > MaxSum){
                MaxSum = ThisSum;
            }
        }
    }
    return MaxSum;
} 
*/

//O(n^2)
#include<iostream>
#include<stdio.h>
using namespace std;

int MaxSubsequenceSum(int a[],int n);

int main(){
    //int a[6] = {-2, 11, -4, 13, -5, -2};
    int a[8] = {4, -3, 5, -2, -1, 2, 6, -2};
    printf("%d\n",MaxSubsequenceSum(a,8));
}

int MaxSubsequenceSum(int a[],int n){
    int ThisSum, MaxSum;
    MaxSum = 0;
    for(int i = 0; i < n; i++){
        ThisSum = 0;
        for(int j = i; j < n; j++){
            ThisSum += a[j];
            if(ThisSum > MaxSum){
                MaxSum = ThisSum;
            }
        }
    }
    return MaxSum;
}
