#include <iostream>
#include<time.h>
#include<math.h>
#include<windows.h>
#include <stack>
using namespace std; 
    int n;//次数 	
	int m=7;
void swap(int arr[] , int i, int j) {
        int c = arr[i];
        arr[i] = arr[j];
        arr[j] = c;
    }
void Quicksort(int r[],int first,int end){//普通快速排序 
	
	if(first>=end)
	return;
	int i=first,j=end;
	int tmp=r[first];
		 while (i < j) {
            while (r[j] >= tmp && i < j) {
                j--;
            }
            r[i]=r[j];
            while (r[i] <= tmp && i < j) {
                i++;
            }
            r[j]=r[i]; 
        }  
        r[i]=tmp;
//		n++;
//		cout<<"第"<<n<<"次排序结果"; 
//        for(int k=0;k<m;k++)
//        	cout<<r[k]<<" ";
//        	cout<<endl;
        Quicksort(r,first,j-1);  
        Quicksort(r,j+1,end);
	}
void Quicksort1(int r[],int first,int end){//增加了swap()的快速排序 
	
	if(first>=end)
	return;
	int i=first,j=end;
	int tmp=r[first];
		 while (i < j) {
            while (r[j] >= tmp && i < j) {
                j--;
            }
            while (r[i] <= tmp && i < j) {
                i++;
            }
            swap(r,i,j);  
        }
        swap(r,first,j);  
//		n++;
//		cout<<"第"<<n<<"次排序结果"; 
//        for(int k=0;k<m;k++)
//        	cout<<r[k]<<" ";
//        	cout<<endl;
        Quicksort1(r,first,j-1);  
        Quicksort1(r,j+1,end);
	}
void Quicksort2(int r[],int first,int end){//随机基准+swap()函数的快速排序 
	srand(time(0));
    int pivot = rand()%(end - first) + first;
    swap(r[pivot],r[first]); //把随机基准位置的元素和low位置元素互换
	if(first>=end)
	return;
	int i=first,j=end;
	int tmp=r[first];
		 while (i < j) {
            while (r[j] >= tmp && i < j) {
                j--;
            }
            while (r[i] <= tmp && i < j) {
                i++;
            }
            swap(r,i,j);  
        }
        swap(r,first,j);  
//		n++;
//		cout<<"第"<<n<<"次排序结果"; 
//        for(int k=0;k<m;k++)
//        	cout<<r[k]<<" ";
//        	cout<<endl;
        Quicksort(r,first,j-1);  
        Quicksort(r,j+1,end);
	}
	int Partition( int a[], int low, int high )
{
	//假设每次都以第一个元素作为枢轴值，进行一趟划分：
	int pivot = a[low];
	 
	while( low<high )
	{
		while( low<high && a[high]>=pivot )
		--high;
		a[low] = a[high];  //停下来做交换 
		while( low<high && a[low]<=pivot )
		++low;
		a[high] = a[low];  //停下来做交换 
	}
	
	a[low] = pivot;  //pivot的最终落点 
	return low;
}
	void Quicksort3(int arr[], int left, int right)
{
	//手动利用栈来存储每次分块快排的起始点
	//栈非空时循环获取中轴入栈
	stack<int> s;
	if( left<right )
	{
		int boundary = Partition(arr,left,right);
		
		if( boundary-1>left ) //确保左分区存在 
		{
			//将左分区端点入栈 
			s.push(left);
			s.push(boundary-1);
		}
		if( boundary+1<right ) //确保右分区存在 
		{
			s.push(boundary+1);
			s.push(right);
		}
		
		while( !s.empty() )
		{
			//得到某分区的左右边界 
			int r = s.top();
			s.pop();  
			int l = s.top();
			s.pop();
			
			boundary = Partition(arr,l,r);
			if( boundary-1>l ) //确保左分区存在 
		    { 
			    //将左分区端点入栈 
		    	s.push(l);
		    	s.push(boundary-1);
	    	}
		    if( boundary+1<r ) //确保右分区存在 
	     	{
	 	    	s.push(boundary+1);
	    		s.push(r);
	    	}
		}
	}
}
int main(){
	double z;                
	LARGE_INTEGER nFreq;          //时间
	LARGE_INTEGER nBeginTime;
	LARGE_INTEGER nEndTime;
	QueryPerformanceFrequency(&nFreq);	
	int a[20000];
	int q[20000];
	int p[20000];
	int k[20000];
	cout<<"请输入要输出的数字个数："; 
	cin>>m;
	srand((unsigned)time(NULL));  //产生随机数种子，避免产生的随机数相同
	for(int i=0;i<m;i++)              //初始化数组
	{
		a[i]=rand();
	}
	for(int i=0;i<m;i++)
	q[i]=a[i];
	for(int i=0;i<m;i++)
	p[i]=a[i];
	for(int i=0;i<m;i++)
	k[i]=a[i];
    //普通快速排序 
	QueryPerformanceCounter(&nBeginTime);//开始计时
	Quicksort(a,0,m-1);
	QueryPerformanceCounter(&nEndTime);//结束计时
	z=(double)(nEndTime.QuadPart-nBeginTime.QuadPart)/(double)nFreq.QuadPart;//计算程序执行时间单位为s
//	cout<<"最终排序结果：";
//	for(int i=0;i<m;i++)
//	cout<<a[i]<<" ";
//	cout<<endl; 
	cout<<"普通快速排序所花费的时间："<<z*1000<<"毫秒"<<endl;
	n=0; 
	//swap 改进 
	QueryPerformanceCounter(&nBeginTime);//开始计时
	Quicksort1(q,0,m-1);
	QueryPerformanceCounter(&nEndTime);//结束计时
	z=(double)(nEndTime.QuadPart-nBeginTime.QuadPart)/(double)nFreq.QuadPart;//计算程序执行时间单位为s
//	cout<<"最终排序结果：";
//	for(int i=0;i<m;i++)
//	cout<<q[i]<<" ";
//	cout<<endl; 
	cout<<"swap改进快速排序所花费的时间："<<z*1000<<"毫秒"<<endl;
	n=0; 
	//随机基准+swap+普通快排 
	QueryPerformanceCounter(&nBeginTime);//开始计时
	Quicksort2(p,0,m-1);
	QueryPerformanceCounter(&nEndTime);//结束计时
	z=(double)(nEndTime.QuadPart-nBeginTime.QuadPart)/(double)nFreq.QuadPart;//计算程序执行时间单位为s
//	cout<<"最终排序结果：";
//	for(int i=0;i<m;i++)
//	cout<<p[i]<<" ";
//	cout<<endl; 
	cout<<"随机基准+swap+普通快速排序所花费的时间："<<z*1000<<"毫秒"<<endl;
	n=0;
	//非递归快速排序 
	QueryPerformanceCounter(&nBeginTime);//开始计时
	Quicksort3(k,0,m-1);
	QueryPerformanceCounter(&nEndTime);//结束计时
	z=(double)(nEndTime.QuadPart-nBeginTime.QuadPart)/(double)nFreq.QuadPart;//计算程序执行时间单位为s
//	cout<<"最终排序结果：";
//	for(int i=0;i<m;i++)
//	cout<<k[i]<<" ";
//	cout<<endl; 
	cout<<"非递归快速排序所花费的时间："<<z*1000<<"毫秒"<<endl;
	n=0;
} 
