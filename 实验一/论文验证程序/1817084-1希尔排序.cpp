/*算法：希尔排序*/
 
#include <iostream>
using namespace std;
 
/*********************************************************
Function:swap
Description:交换a,b两个变量的值
**********************************************************/
void swap(int & a,int & b){
    int tmp = a;
    a = b;
    b = tmp;
}
 
/*********************************************************
Function:shsort
Description:希尔排序
Input:数组A[l,h]
Output:排序完成的数组A
*********************************************************/
void shsort(int A[],int l,int h){
    if(l<h){
        int d;//增量
        int tmp;
        int j;
        int size = h-l+1;
        for(d=size/2;d>=1;d/=2){
            //组内使用插入排序
            //d+l是第1组的第2个元素
            for(int i=d+l;i<=h;i++){
                tmp = A[i];
                j=i-d;//该组中，当前元素的前一个元素
                //寻找要插入的位置
                while(j>=l&&A[j]>tmp){
                    swap(A[j],A[j+d]);
                    j = j - d;
                }
                A[j+d] = tmp;
            }
        }
    }
}
 
int main(){
    int A[] = {3,1,2,8,-4,0,11};
    shsort(A,0,6);
    for(int i=0;i<7;i++)
        cout<<A[i]<<" ";
}
