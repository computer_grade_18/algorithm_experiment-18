#include<iostream>
using namespace std;
int partition(int *arr,int left,int right)
{
    int temp=arr[left];
    while(left<right)//直达left和right重合的时候，才找到合适的位置
    {
        //先从后往前找比基准小的
        while(left<right  &&  arr[right]>=temp)//当right的值大于temp的值的时候才执行
                //等号一定得写，因为可能会出现，保存的temp元素和数据中的元素一样的，不写会出现死循环的现象
        {
            right--;
        }
        arr[left]=arr[right];//当right的值小于temp的值的时候执行
        //从前往后找，找比基准大的
        while(left<right  && arr[left] <=temp)//当left的值小于temp的值的时候执行
        {
            left++;
        }
        arr[right]=arr[left];//当left的值大于temp的时候执行
    }
    arr[left]=temp;//此时的left和right在同一个位置，此时为合适的位置，把temp的值给left
    return left;//此时返回的值是temp合适的位置，即小于它的在它的左边，大于它的在它的右边
}
 void quick(int *arr,int left,int right)
{
    if(left<right)
    {
        int pivot=partition(arr,left,right);
        quick(arr,left,pivot-1);
        quick(arr,pivot+1,right);
    }
}
void quick_sort(int *arr,int len)
{
    quick(arr,0,len-1);
}
int main()
{
    int arr[]={9,5,7,10,45,12};
    int len=sizeof(arr)/sizeof(arr[0]);
    quick_sort(arr,len);
    for(int k=0;k<len;++k)
    {
        cout<<arr[k]<<"  ";
    }
    cout<<endl;
}