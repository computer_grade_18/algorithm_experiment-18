#include<cstdio>
#include<algorithm>
using namespace std;
const int MAXV=1000; //最大顶点数 
const int INF=1000000000; //设INF为一个很大的数 
int n,m,G[MAXV][MAXV]; //n为顶点数，m为边数，G为图 
int lowcost[MAXV]; //离集合S（存放已被访问的顶点）的最短距离 
int prim() //默认0号为初始点，函数返回最小生成树和最小生成树的边权之和 
{
	printf("最小生成树为：\n");
	int mst[MAXV]; //mst[]表示对应lowcost[]的起点 
	int ans=0;     //边权之和初始化为0 
	for(int i=1;i<n;i++)
	{
		lowcost[i]=G[0][i]; //lowcost[]存放顶点0可达点的路径长度 
		mst[i]=0;           //初始化以顶点0为起始点 
	}
	for(int i=1;i<n;i++)
	{
		int u=-1,MIN=INF;  //u为离集合s最近的一个顶点，MIN为顶点离集合s的最短路径 
		for(int j=1;j<n;j++)
		{
			if(lowcost[j]!=0&&lowcost[j]<MIN)
			{
				u=j;                 //找出u的id； 
				MIN=lowcost[j];      //找出权值最短的路径长度 
			}
		}
		if(u==-1) {printf("输入图非无向连通图\n");break;}  //找不到小于INF的lowcost[u]，则剩下的顶点与集合s不连通，提示错误 
		printf("V%d--V%d=%d\n",mst[u],u,MIN);
		ans+=lowcost[u];        //求和 
		lowcost[u]=0;           //标记u为已访问 
		for(int v=1;v<n;v++)
		{
			if(lowcost[v]!=0&&G[u][v]!=INF&&G[u][v]<lowcost[v]) //对这一点直达的顶点进行路径更新 
			{
			  lowcost[v]=G[u][v];
			  mst[v]=u;
		    } 
		}
	}
	printf("最小生成树边权之和=%d\n",ans);
} 
int main()
{
	int u,v,w;
	scanf("%d%d",&n,&m);  //输入顶点个数，边数 
	for(int i=0;i<n;i++)  //初始化图G 
	{
		for(int j=0;j<n;j++)
		G[i][j]=INF;
	}
	for(int i=0;i<m;i++)
	{
		scanf("%d%d%d",&u,&v,&w); //输入u，v以及边权 
		G[u][v]=G[v][u]=w;     //无向图 
	}
	prim();  //执行Prim算法函数 
	return 0;
}
