#include <iostream>
#include<stdlib.h>
#include<algorithm>
#include<cstring>
#include <math.h>
#include <time.h>
#define random(x) (rand()%x)
using namespace std;
int num=1,n,c[100][100],ret[100][100];
struct action{
	int index;
	int start;
	int end;
};
bool cmp(const action &a,const action &b)
{
	if(a.end<=b.end)return true;
	return false;
}
int greedy(int n,action a[],int b[],int &num)//贪心法 
{
	int j=1,k=1,t=0;
	b[num]=a[1].index;
	while(j<n){
		j++;
		t++;
		if(a[j].start>=a[k].end)//比较前一个被选择的活动的结束时间是否比该活动的起始时间小或等于。 
		{
			num++;
			b[num]=a[j].index;
			k=j; 
		}
	}
	return t;
}

int yz(action a[],int n,int c[][100],int ret[][100])//动态规划方法 
{
	int i,j,k;
    int temp;
    for(j=2;j<=n;j++)
        for(i=1;i<j;i++)
        {
            for(k=i+1;k<j;k++)
            {
                if(a[k].start>=a[i].end&&a[k].end<=a[j].start)
                {
                    temp=c[i][k]+c[k][j]+1;
                    if(c[i][j]<temp)
                    {
                        c[i][j]=temp;
                        ret[i][j]=k;
                    }
                }
            }
        }
}
int main(){
	cout<<"输入有几个活动：";
	cin>>n;
	if(n==0){
		cout<<endl<<"场地没有活动安排";
		return 0;
	} 
    int times,x,b[1000];
    action a[1000]; 
    a[0].end=0;
    memset(b,0,sizeof(b));
	cout<<endl<<"输入每个活动的起始时间和结束时间："<<endl;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i].start>>a[i].end;
		a[i].index=i;
	}
	
    time_t start,end;
    double Time;
    start=(double)clock();
    sort(a,a+n+1,cmp);
	times=greedy(n,a,b,num);
	cout<<endl<<"该场地能进行的活动数:"<<num;
	cout<<endl<<"活动："; 
	for(int i=1;i<=num;i++)
	cout<<b[i]<<" ";
    end=(double)clock();
    Time=(double)(end-start)/CLK_TCK;
    
    cout<<endl<<"验证结果：";
    memset(c,0,sizeof(0));
    memset(ret,0,sizeof(ret));
    yz(a,n,c,ret);
    if((c[1][n]+2)==num)cout<<"yes";
    else cout<<"false";
	
    cout<<endl<<"贪心算法所用时间:"<<Time<<endl; 
    cout<<"贪心算法除排序的比较次数:"<<times; 
}
