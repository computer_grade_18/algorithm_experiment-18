#include <iostream>
using namespace std;
void Sort(int n,float w[],float v[])              //冒泡排序 ，将各物品按照单位重量的价值从大到小排序 
{ 
    int i,j;
    float temp1,temp2;
    for(i=0;i<=n;i++)
    for(j=0;j<=n-i;j++)
    {
        temp1=v[j]/w[j];
        temp2=v[j+1]/w[j+1];
        if(temp1<temp2)
        {
            swap(w[j],w[j+1]);
            swap(v[j],v[j+1]);
        }
    }
}
int KnapSack(float w[],float v[],int n,float c)   
{
	int i;
	float x[10]={0};
	float maxvalue=0;
	for (i=0;w[i]<c;i++)
	{
		x[i]=1;									//将物品i装入背包 
		maxvalue+=v[i];							 
		c-=w[i];                                //背包的剩余容量 
	}
	x[i]=c/w[i];                                //背包剩余容量与剩余物品中单位价值最大者重量的比 
	maxvalue+=x[i]*v[i];
	cout<<"物品的比例:"; 
	for(i=0;i<n;i++)
		cout<<x[i]<<" ";
}
int main()
{
	float w[10]={20,30,10};
    float v[10]={60,120,50};
    int n=3; 
    float c=50;
    Sort(n,w,v);
    cout<<"物品的重量:";
	for(int i=0;i<n;i++) cout<<w[i]<<" ";
	cout<<endl;
	cout<<"物品的价值:";
	for(int i=0;i<n;i++) cout<<v[i]<<" ";
	cout<<endl;
	KnapSack(w,v,n,c);	   
}

