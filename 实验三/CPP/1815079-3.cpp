#include<iostream>
#include<string.h>
#include<algorithm>
using namespace std;
int n=5;
double W=40;
struct Element{   
	double w;
	double v;
	double p;
};
int cmp(const Element&a,const Element &b) //比较函数 
{
	return a.p>b.p;
}
Element A[]={{0,0},{10,5},{20,40},{10,20},{15,30},{44,66}};
double value;
double x[600];
void knapsack()   //求解背包问题并返回结果 
{
	value=0;               //设初始价值为0 
   	double weight=W;        
	memset(x,0,sizeof(x));      //初始化x向量 
	int i=1;                    
	while(A[i].w<=weight)          //当物品全能装入时 
	{ 
		x[i]=1;                // 为装入物品代表这个物品全部装入 
		weight-=A[i].w;        //减去背包的容量 
		value+=A[i].v;          //并对他的价值进行累加 
		i++;
	}
	 if(weight>0)      //余下的背包容量大于0 
	{

		x[i]=weight/A[i].w; //将物品的一部分装入 
		value+=x[i]*A[i].v; //将物品的一部分价值累加 
	} 
}
int main()
{   for(int i=1;i<=n;i++)            //计算每个物品的单位价值 
{
	A[i].p=A[i].v/A[i].w;
}
	sort(A+1,A+n+1,cmp);         //对每个物品的单位价值进行排序 
cout<<"背包的总容量："<<W<<endl;
cout<<"物品的个数："<<n<<endl;
for(int j=1;j<=n;j++)
{
	cout<<"第"<<j<<"个物品的重量为 "<<A[j].w<<" 价值为 "<<A[j].v<<endl;
}
for(int i=1;i<=n;i++)
{
	cout<<"第"<<i<<"物品的单位价值为 "<<A[i].p<<endl;
}
cout<<endl;
knapsack(); 
cout<<"背包的最高价值："<<value<<endl;         
cout<<"最高价值下物品的选择情况：";
for(int j=1;j<=n;j++)
{
	cout<<x[j]<<" ";      //输出物品的选择情况 
}
return 0;
 } 

