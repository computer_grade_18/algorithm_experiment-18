#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

struct good{
	int v;
	int w;
	int per_v;
};

bool desc(good i, good j) { return i.per_v > j.per_v; }

int KnapSpack(good g[],int n,int c)
{
	double x[n]={0};int i;
	int maxValue = 0;
	for (i=0;g[i].w<c;i++)
	{
		x[i] = 1;
		maxValue += g[i].v;
		c -= g[i].w;
	}
	x[i] = (double)c/g[i].w;
	maxValue += x[i] * g[i].v;
	return maxValue;
}
int main()
{	
	vector<int> w,v,per_v;
	int n,c;//n means goods scale,c means capacity
	
	c =50; n= 3;
	w = {20,30,10};
	v = {60,120,50};
	
	for(int i;i<w.size();i++)
		per_v.push_back(v[i] / w[i]);
		
	good bag[n+1];
	for(int i=0;i<n;i++)
		{
			bag[i].v = v[i];
			bag[i].w = w[i];
			bag[i].per_v = per_v[i];
		}
		
	sort(bag,bag+n,desc);
/*				
		for(int i=0;i<n;i++)
		{
			cout<< g[i].v <<" "<<g[i].w << " " <<g[i].per_v <<endl;
		}*/
	cout << KnapSpack(bag,n,c);

}

