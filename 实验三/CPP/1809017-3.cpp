// 活动安排问题 贪心算法
/*设有n个活动的集合E={1,2,…,n}，其中每个活动都要求使用同一资源，如演讲会场等，而在同一时间内只有一个活动能使用这一资源。
每个活动i都有一个要求使用该资源的起始时间si和一个结束时间fi,且si <fi。如果选择了活动i，则它在半开时间区间[si, fi)内占用资源。
若区间[si, fi)与区间[sj, fj)不相交,则称活动i与活动j是相容的。也就是说，当si≥fj或sj≥fi时，活动i与活动j相容。
活动安排问题就是要在所给的活动集合中选出最大的相容活动子集合。*/ 
#include <iostream> 
using namespace std; 
template<class Type>
void GreedySelector(int n, Type s[], Type f[], bool A[]);
 
const int N = 11;
 
int main()
{
	//下标从1开始,存储活动开始时间
	int s[] = {0,1,3,0,5,3,5,6,8,8,2,12};
 
	//下标从1开始,存储活动结束时间
	int f[] = {0,4,5,6,7,8,9,10,11,12,13,14};
 
	bool A[N+1];
 
	cout<<"各活动的开始时间,结束时间分别为："<<endl;
	for(int i=1;i<=N;i++)
	{
		cout<<"["<<i<<"]:"<<"("<<s[i]<<","<<f[i]<<")"<<endl;
	}
	GreedySelector(N,s,f,A);
	cout<<"最大相容活动子集为："<<endl;
	for(int i=1;i<=N;i++)
	{
		if(A[i]){
			cout<<"["<<i<<"]:"<<"("<<s[i]<<","<<f[i]<<")"<<endl;
		}
	}
 
	return 0;
}
 
template<class Type>
void GreedySelector(int n, Type s[], Type f[], bool A[])
{
	A[1]=true;
	int j=1;//记录最近一次加入A中的活动
 
	for (int i=2;i<=n;i++)//依次检查活动i是否与当前已选择的活动相容
	{
		if (s[i]>=f[j])
		{ 
			A[i]=true;
			j=i;
		}
		else
		{
			A[i]=false;
		}
	}
}
