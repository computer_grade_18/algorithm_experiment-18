#include <iostream>
#include <algorithm>
#include <string.h>
using namespace std;
#define MAXN 1000
int n=5;
double W=100;  //背包重量
struct Element
{
	double w;
	double v;
	double p;  //p=v/w   单位重量价值  
} ;
//比较函数(将物品价值由大到小排序)
int cmp(const Element &a,const Element &b)
{
	return a.p>b.p;    //按p递减排序 
 } 
Element A[]={{0},{10,20},{20,30},{30,66},{40,40},{50,60}}; //下标为0的元素不用
double value;  //最大价值 
double x[MAXN];  //存储选择方案
int knapsack()   //求解背包问题并返回总价值 
{
	value=0;
	double weight;
	weight=W;                 //背包中中能够装入的剩余容量
	memset(x,0,sizeof(x));    //初始化x向量
	int i;
	while(A[i].w<weight)   //物品i能够全部装入时的循环 
	{
		x[i]=1;
		weight-=A[i].w;   //背包中剩余的重量 
		value+=A[i].v;    //累计总价值 
		i++; 
	} 
	if(weight>0)   //剩余重量大于0 
	{
		x[i]=weight/A[i].w;  //将物品i的一部分装入
		value+=x[i]*A[i].v;     
	} 
}
int main()
{
	for(int i=1;i<=n;i++)
	{
		A[i].p=A[i].v/A[i].w;   //求出p 
	}
	cout<<"排序前:(v,w,p)"<<endl;
	for(int i=1;i<=5;i++)
	{cout<<A[i].v<<" ";
	cout<<A[i].w<<" ";
	cout<<A[i].p<<" ";
	}
	cout<<endl; 
	sort(A+1,A+n+1,cmp);
	cout<<"排序后:(v,w,p)"<<endl;
	for(int i=1;i<=5;i++)
	{
	cout<<A[i].v<<" ";
	cout<<A[i].w<<" ";
	cout<<A[i].p<<" ";
	}
	knapsack();
	cout<<endl;
	cout<<"输出结果:"<<endl;
	cout<<"背包装入情况:"<<endl;
	for(int j=1;j<=n;j++)
	{     
	cout<<x[j]<<",";   
	}
	cout<<endl;
	cout<<"最大总价值为:"<<value<<endl;
	return 0; 
} 
