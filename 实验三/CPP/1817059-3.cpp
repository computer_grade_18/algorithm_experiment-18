
#include<iostream>
#include<algorithm>
using namespace std;
class Good
{
	private:
		int num;			//物品编号
		double weight;	//物品重量
		double value;	//物品价值 
	public:
		Good(int n,double w, double v);	//构造函数
		Good();							//默认构造函数
		void init(int n,double w,double v);	//初始化
		int returnNum();
		double returnWeight();		//返回重量
		double returnValue();			//返回价值
		friend int cmp(Good good1,Good good2);			//按照单位价值量降序
		friend void KnapSack(Good *good,int num,int capacity);//贪心法求背包
 
};
//构造函数
Good::Good(int n,double w, double v)
{
	num=n;
	weight=w;
	value=v;
}
 
//默认构造函数
Good::Good()
{
 
}
 
//初始化
void Good::init(int n,double w,double v)
{
	num=n;
	weight=w;
	value=v;
}
//返回物品编号
int Good::returnNum()
{
	return num;
}
//返回重量
double Good::returnWeight()
{
	return weight;
}
 
//返回价值
double Good::returnValue()
{
	return value;
}
 
//按照单位价值量降序排序
int cmp(Good good1,Good good2)
{
	return good1.returnValue()/good1.returnWeight() > good2.returnWeight()/good2.returnValue();
}
//贪心法求解过程
void KnapSack(Good *good,int num,int capacity)
{
	double *process=new double[num];//存放问题的解
	int i;
	for(i=0;i<num;i++)
		process[i]=0;
	int maxValue=0;//背包最大价值
	for(i=0;good[i].returnWeight()<capacity;i++)
	{
		process[i]=1;							//物品i装入背包
		maxValue+=good[i].returnValue();
		capacity-=good[i].returnWeight();		//背包剩余容量
		cout<<"物品"<<good[i].returnNum()<<"完全装入背包"<<endl;
		cout<<"背包容量:"<<capacity<<",当前价值:"<<maxValue<<endl<<endl;
 
	}
	process[i]=((double)capacity)/good[i].returnWeight();	//部分装入
	maxValue+=process[i]*good[i].returnValue();
	cout<<"物品"<<good[i].returnNum()<<"部分装入背包"<<endl;
	cout<<"\n背包价值为："<<maxValue;
 
}
//主函数
int main()
{
	cout<<"欢迎来到贪心法求解背包问题，请输入背包容量"<<endl;
	int capacity;
	int good_num;
	int i;
	while(cin>>capacity)
	{
		cout<<"请输入物品个数"<<endl;
		cin>>good_num;
		Good *good=new Good[good_num];//背包数组
 
 
		//初始化物品的重量和价值
		cout<<"请输入"<<good_num<<"个物品的重量和价值"<<endl;
		for(i=0;i<good_num;i++)
		{
			double w,v;
			cin>>w;
			cin>>v;
			good[i].init(i+1,w,v);
		}
		//输出用户输入的信息
	    cout<<"\n您输入的"<<good_num<<"个物品的信息"<<endl;
		cout<<"物品"<<"\t"<<"重量"<<"\t"<<"价值"<<"\t"<<endl;
		for(i=0;i<good_num;i++)
		{
			cout<<good[i].returnNum()<<"\t"<<good[i].returnWeight()<<"\t"<<good[i].returnValue()<<"\t"<<endl;;
		}
 
		//单位价值降序排序
		sort(good, good+good_num, cmp);  
        cout<<"排序后"<<endl;
		cout<<"物品"<<"\t"<<"重量"<<"\t"<<"价值"<<"\t"<<endl;
		for(i=0;i<good_num;i++)
		{
			cout<<good[i].returnNum()<<"\t"<<good[i].returnWeight()<<"\t"<<good[i].returnValue()<<"\t"<<endl;;
		}
 
 
		//计算
		KnapSack(good,good_num,capacity);
 
		cout<<"\n----------------------------------------------"<<endl;
		cout<<"欢迎来到贪心法求解背包问题，请输入背包容量"<<endl;
	}
	return 0;
}
