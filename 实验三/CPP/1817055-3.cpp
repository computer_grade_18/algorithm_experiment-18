#include <iostream>
using namespace std;
void Sort(int n,float *w,float *v)  //n为物品数;w为物品重量;v为物品价值 
{
    int i,j;
    float temp1,temp2;
    for(i=1;i<=n;i++)
    for(j=1;j<=n-i;j++)                
    {
        temp1=v[j]/w[j];
        temp2=v[j+1]/w[j+1];
        if(temp1<temp2)
        {
            swap(w[j],w[j+1]);
            swap(v[j],v[j+1]);     //通过冒泡排序将物品按照单位重量的价值量大小降序排列
        }
    }
}
int main()
{
    float w[100];			//每个物品的重量
    float v[100];			//每个物品的价值
    float x[100];			//表示最后放入背包的比例
    int n;					//物品数
    float C;				//背包的容量 
    cout<<"请分别输入物品个数和背包容量："<<endl; 
	cin>>n>>C;				//输入物品个数和背包容量     
    for(int i=1;i<=n;i++) 
	{
		cout<<"请输入物品的重量w["<<i<<"]："<<endl;
        cin >> w[i]; 
	}
	for(int i=1;i<=n;i++) 
	{
        cout<<"请输入物品的价值v["<<i<<"]："<<endl;
        cin >> v[i];
	}	                   //输入每件物品重量和价值 
    Sort(n,w,v);
    int i;
    for(i=1;i<=n;i++)
        x[i]=0;			   //初始值，未装入背包，x[i]=0
    float c=C;             //更新背包的容纳
    for(i=1;i<=n;i++)
    {
        if(c<w[i])  
		break;				//不能完全装下
        x[i]=1;
        c=c-w[i];
    }
    if(i<=n)
        x[i]=c/w[i];              
    for(int i=1;i<=n;i++)
        cout<<"重量为"<<w[i]<<"价值量为"<<v[i]<<"的物品"<<"放入的比例为"<<x[i]<<endl;
    return 0;
}

