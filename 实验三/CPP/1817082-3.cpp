#include <iostream>
#include <string.h>
#include<algorithm>
using namespace std;
int n=4;
double W=100;
struct Element{
	double w;
	double v;
	double p;
};
int cmp(const Element&a,const Element &b)
{
	return a.p>b.p;
}
Element A[]={{0},{10,20},{30,66},{40,40},{50,60}};
double value;
double x[500];
void knapsack()   //求解背包问题并返回结果 
{
	value=0;               //初始最大价值为0 
   	double weight=W;        
	memset(x,0,sizeof(x));      //初始化x向量 
	int i=1;                    
	while(A[i].w<weight)          //物品全能装入时 
	{ 
		x[i]=1;                // 装入物品 
		weight-=A[i].w;        //减少背包的容量 
		value+=A[i].v;          //价值进行累加 
		i++;
	}
	if(weight>0)      //余下的背包容量大于0 
	{
		x[i]=weight/A[i].w; //将物品的一部分装入 
		value+=x[i]*A[i].v;  //将物品的一部分价值累加 
	}
}
int main()
{   for(int i=1;i<=n;i++)            //计算每个物品的单位价值 
{
	A[i].p=A[i].v/A[i].w;
}
	sort(A+1,A+n+1,cmp);         //按每个物品的单位价值进行排序 
cout<<"背包的总容量："<<W<<endl;
for(int j=1;j<=n;j++)
{
	cout<<"第"<<j<<"个物品的重量："<<A[j].w<<" 价值："<<A[j].v<<endl;
}
cout<<"每个物品的价值：";
for(int i=1;i<=n;i++)
{
	cout<<A[i].p<<" ";
}
cout<<endl;
knapsack();          
	
cout<<"物品的选择情况：";
for(int j=1;j<=n;j++)
{
	cout<<x[j]<<" ";      //输出物品的选择情况 
}
cout<<endl;
cout<<"最高价值："<<value<<endl;
 } 
 
