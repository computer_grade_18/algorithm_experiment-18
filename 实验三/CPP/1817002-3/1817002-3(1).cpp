#include<iostream>
#include<string>
#include<iomanip>
using namespace std;
class TSP
{
	private:
		int city_number;
		int **distance;
		int start;
		int *flag;
		int TSPLength;
	public:
		TSP(int city_num);
		void correct();
		void printCity(); 
		void TSP1();
};
TSP::TSP(int city_num)
{
	int i=0,j=0;
	int start_city;
	city_number=city_num;
	cout<<"请输入本次运算的城市起点，范围为："<<0<<"-"<<city_number-1<<endl;
	cin>>start_city;
	start=start_city;
	distance=new int*[city_number];
	cout<<"请输入"<<city_number<<"个城市之间的距离"<<endl;
	for(i=0;i<city_number;i++)
	{
		distance[i]=new int[city_number];
		for(j=0;j<city_number;j++)
			cin>>distance[i][j];
	}
	flag=new int[city_number];
	for(i=0;i<city_number;i++)
	{
		flag[i]=0;
	}
	TSPLength=0;
}
void TSP::correct()
{
	int i;
	for(i=0;i<city_number;i++)
	{
		distance[i][i]=0;
	}
}
void TSP::printCity()
{
	int i,j;
	cout<<"您输入的城市距离如下："<<endl;
	for(i=0;i<city_number;i++)
	{
		for(j=0;j<city_number;j++)
			cout<<setw(3)<<distance[i][j];
		cout<<endl;
	}
}
void TSP::TSP1()
{
	int edgeCount=0;
	int min,j;
	int start_city=start;
	int next_city;
	flag[start]=1;
	cout<<"路径如下"<<endl;
	while(edgeCount<city_number-1)
	{
		min=100;
		for(j=0;j<city_number;j++)
		{
			if((flag[j]==0) && (distance[start_city][j] != 0) && (distance[start_city][j] < min))
			{
				next_city=j;
				min=distance[start_city][j];
			}
		}
		TSPLength+=distance[start_city][next_city];
		flag[next_city] = 1;
		edgeCount++;
		cout<<start_city<<"-->"<<next_city<<endl;
		start_city=next_city;
	}
 
	cout<<next_city<<"-->"<<start<<endl;
	TSPLength+=distance[start_city][start];
	cout<<"路径长度为："<<TSPLength;
}
int main()
{
	cout<<"请输入城市个数：";
	int city_number;
	while(cin>>city_number)
	{
		TSP tsp(city_number);
		tsp.correct();	
		tsp.printCity();		
		tsp.TSP1();			  
		cout<<"-------------------------------------------------------"<<endl;
		cout<<"\n请输入城市个数：";
	}
	return 0;
} 
