#include<iostream>
#include<string>
#include<iomanip>
#include<algorithm>
using namespace std;
class TSP
{
	public:
        int start_city;		            
		int end_city;			
		int distance;			
		TSP(int sta,int end,int dis);	
		TSP();				
		void init(int sta,int end,int dis);	
		void printfInfo();		
};
int cmp(TSP t1,TSP t2);						
int TSP2(TSP *t,int city_num,int cities,TSP *process);  
int find(int x,int *pre);					
void join(int x,int y,int *pre);			
TSP::TSP(int sta,int end,int dis)
{
	start_city=sta;
    end_city=end;
	distance=dis;
}
TSP::TSP()
{
}
void TSP::init(int sta,int end,int dis)
{
    start_city=sta;
    end_city=end;
	distance=dis;
}
void TSP::printfInfo()
{
	cout<<start_city<<"\t"<<end_city<<"\t"<<distance<<"\t\n";
}
int cmp(TSP t1,TSP t2)
{
	return t1.distance<t2.distance;
}
int TSP2(TSP *t,int city_num,int cities,TSP *process)
{
	int TSPLength=0;
	int i=0,j=0,k=0;
	int *flag=new int[city_num];
	int *pre=new int[city_num];		
	for(i=0;i<city_num;i++)
	{
		flag[i]=0;				
		pre[i]=i;				
	}
	do{
		for(i=0;i<cities;i++)
		{
			if(find(t[i].start_city,pre)!=find(t[i].end_city,pre))
			{
				if(flag[t[i].start_city]<=2 && flag[t[i].end_city]<=2)
				{
					flag[t[i].start_city]++;	
				    flag[t[i].end_city]++;
					TSPLength+=t[i].distance;	
					process[k].start_city=t[i].start_city;
					process[k].end_city=t[i].end_city;
					process[k].distance=t[i].distance;
					j++;
					k++;
					join(t[i].start_city,t[i].end_city,pre);
				}
			}
		}
	}while(j<city_num-1);
	int m,n;
	for(m=0;m<city_num;m++)
		if(flag[m]==1)
		{
			process[k].start_city=m;
			flag[m]++;
			break;
		}
	for(n=0;n<city_num;n++)
		if(flag[n]==1)
		{
			process[k].end_city=n;
			flag[n]++;
			break;
		}
	for(i=0;i<cities;i++)
		if(t[i].start_city==m && t[i].end_city==n)
			process[k].distance=t[i].distance;
	TSPLength+=process[k].distance;
	return TSPLength;
}
int find(int x,int *pre)
{
    int r=x;
    while(pre[r]!=r)
    r=pre[r];	
    int i=x,j;
    while(i!=r)		
    {
        j=pre[i];	
        pre[i]=r;	
        i=j;
    }
    return r;
}
void join(int x,int y,int *pre)
{
    int a=find(x,pre);	
    int b=find(y,pre);	
    if(a!=b)			
    {
        pre[a]=b;		
    }
}
int main()
{
	cout<<"请输入城市个数：";
	int city_number,cities=0; 
	while(cin>>city_number)
	{
		int i,j,k=0;
		for(i=1;i<city_number;i++)
		   cities+=i;
		TSP *tsp=new TSP[cities];		
		TSP *process=new TSP[city_number];
		cout<<"请输入城市的代价矩阵："<<endl;
		int **distance;
		distance=new int*[city_number];
		for(i=0;i<city_number;i++)
		{
			distance[i]=new int[city_number];
			for(j=0;j<city_number;j++)
			{
		     	cin>>distance[i][j];
			}
		}
		for(i=0;i<city_number;i++)
		{
		    for(j=i+1;j<city_number;j++)
			{
			   tsp[k].init(i,j,distance[i][j]);
			   k++;
			 }
		}
		cout<<"起点\t"<<"终点\t"<<"距离\t"<<endl;
		for(i=0;i<cities;i++)
		{
		    tsp[i].printfInfo();	
		}
        cout<<"-------------------------------------"<<endl;
		sort(tsp,tsp+cities,cmp);
		cout<<"\n排序后"<<endl;
		cout<<"起点\t"<<"终点\t"<<"距离\t"<<endl;
		for(i=0;i<cities;i++)
		{
		    tsp[i].printfInfo();	
		}
		cout<<"-------------------------------------"<<endl;
		cout<<"最短链接求解过程："<<endl;
		int length=TSP2(tsp,city_number,cities,process);
		cout<<"起点\t"<<"终点\t"<<"距离\t"<<endl;
		for(i=0;i<city_number;i++)
		{
		    process[i].printfInfo();	
		}
		cout<<"路径长度:"<<length<<endl;
        delete[] tsp;
	}
	return 0;
} 
