#include<iostream>
#include<limits.h>
using namespace std;
const int n = 5;
int temp = INT_MAX;

//假设所有城市及城市之间的道路构成一个完全图 
int main()
{
	int i,j,k;
	int s[n];		//数组用于记录商人走过城市的顺序 
	int tsp[n][n] = {{temp,2,4,7,8},  // 初始化tsp数组，tsp[i][j] 表示 城市i到城市j的距离 
					 {2,temp,3,10,12},
					 {4,3,temp,5,6},
					 {7,10,5,temp,3},
					 {8,12,6,3,temp}					
					};
	
	int sum = 0;//记录距离
	int l;
	int flag = 0;
	i = 1;
	s[0] = 0;//初始化数组s，s[0] = 0表示出发城市为0号城市，则商人最终要回到0号城市 
	do{
		k = 1;temp = INT_MAX;
		do{
			l = 0;flag = 0;
			do{ //判断编号为k的城市 是否被商人途径过，如果途径过，则寻找下一个城市。 
				if(s[l] == k){
					flag = 1;
					break;
				}
				else l++;
			}while(l<i);
			if(flag == 0 && tsp[k][s[i-1]] < temp){
				j = k;//保留距离i-1最短的城市 
				temp = tsp[k][s[i-1]];//保留最短的距离 
			}
			k++;
		}while(k<n);
		s[i] = j;//将访问的城市记录到数组中 
		cout<<temp<<endl; 
		sum += temp;//距离累加 
		i++; 
	}while(i<n);
	sum+=tsp[0][j];
	cout<<tsp[0][j]<<endl;
	cout<<"最短路径为：";
	for(i=0; i<n; i++){
		cout<<s[i]<<" ";
	}
	cout<<endl;
	cout<<"最短距离为："<<sum<<endl;
	return 0;
}
