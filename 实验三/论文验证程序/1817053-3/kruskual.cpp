/*
    kruskual 最小生成树 贪心算法
    时间复杂度
*/

#include<iostream>
#include<ctime>
#include<algorithm>
using namespace std;

int VNUM,ENUM,a,b,c;
struct Edge{
    int from; // 来自结点
    int to; // 到第几个结点
    int val; // 权重
    Edge(){}
    Edge(int a,int b,int c):from(a),to(b),val(c){    }
};

const int maxn = 25000000;
int parent[maxn] = {0}; // 0表示未初始因此将index = 0空出
Edge edges[maxn];
// 查找
int findRoot(int x){
    if(parent[x] == 0){
        return x;
    }
    return parent[x] = findRoot(parent[x]); // 递归构造树使得查询复杂度O(1)
}

// 合并
bool mergeRoot(int a, int b){
    a = findRoot(a);
    b = findRoot(b);
    if(a == b) return false;
    parent[a] = b;
    return true;
}

// 读入数据
void read(){
    freopen("../test.in","r",stdin);
    scanf("%d %d",&VNUM,&ENUM);

    // 读入数据
    for(int i = 0;i < ENUM;i++){
        scanf("%d %d %d",&a,&b,&c);
        edges[i] = Edge(a + 1,b + 1,c);
    }
}


int main(){
    read();
    
    clock_t start_time,end_time;
    start_time = clock();
    // 按边快速排序
    sort(edges,edges + ENUM,[](const Edge & e1, const Edge & e2){
        return e1.val < e2.val;
    });

    // 遍历边
    int cnt = 0;
    int ans = 0;
    for(int i = 0; i < ENUM && cnt <= VNUM;i++){
        if(mergeRoot(edges[i].from,edges[i].to)){
            cnt ++;
            ans += edges[i].val;
        }
    }
    end_time = clock();

    printf("Min Weights:%d Use time:%.5f",ans,(double)(end_time - start_time) / CLOCKS_PER_SEC);
    return 0;
}