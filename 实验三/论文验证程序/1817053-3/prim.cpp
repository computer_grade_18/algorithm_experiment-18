/*
    普利姆最小生成树 贪心算法（普通版）
    每次找最近的结点加入树，找N-1次
    算法复杂度 O（V ^ 2）
*/

#include<iostream>
#include<cstring>
#include<ctime>
using namespace std;

const int maxn = 10000;

int W[maxn][maxn]; // 存放边权
int V_NUM;
int E_NUM;

int closeSet[maxn]; // 结点与S中最近的点的下标，只考虑未入树的点
int lowCost[maxn]; // 结点到S的的最短距离，只考虑未入树的店
int used[maxn]; // 标记是结点否已经入树
int ans;
int prim(){
    ans = 0;
    // 初始化未入树结点到树的距离，此时树中只有一个结点
    for(int i = 0;i < V_NUM; i++){
        lowCost[i] = W[0][i];
        closeSet[i] = 0;
        used[i] = 0;
    }
    used[0] = 1;
    
    // 找出n-1个结点入树，每次找距离S最近的
    for(int i = 1;i < V_NUM;i++){
        int k = 0; // 未选择的点下标
        for(int j = 0; j < V_NUM;j++){
            if(!used[j] && lowCost[j] < lowCost[k]) k = j;
        }
        used[k] = 1; // 将该点加入树中 
        ans += lowCost[k];

        // 更新lowCost 和 closeSet
        for(int j = 0;j < V_NUM ;j++){
            if(!used[j] && W[j][k] < lowCost[j]){
                lowCost[j] = W[j][k];
                closeSet[j] = k;
            }
        }
    }

    return ans;
}



void read(){
    freopen("../test.in","r",stdin);
    
    // 初始化数组为无穷大
    for(int i = 0;i < maxn;i++)for(int j = i;j < maxn;j++) W[i][j] = W[j][i] = 0x7fffffff;
    scanf("%d %d",&V_NUM,&E_NUM);
    int a,b,c;

    // 输入权重
    for(int i = 0;i < E_NUM ;i++){
        scanf("%d %d %d",&a,&b,&c);
        W[a][b] = W[b][a] = c;
    }
}

int main(){
    read();


    clock_t start_time,end_time;
    start_time = clock();
    int ans = prim();
    end_time  = clock();
    printf("Min Weights:%d use time:%.5f s",ans,(double)(end_time - start_time) / CLOCKS_PER_SEC);
    return 0;
}