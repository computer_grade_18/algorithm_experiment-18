/*
    prim 最小生成树 + 堆优化
    算法时间复杂度 ElogV
*/


#include<iostream>
#include<queue>
#include<vector>
#include<ctime>
using namespace std;



// 点
struct Node
{
    int index;
    int w;
    Node(int a,int b):index(a),w(b){}
    bool operator < (const Node & n) const {
        return w > n.w;
    }
};

struct Edge{
    int from; // 来自结点
    int to; // 到第几个结点
    int val; // 权重
    Edge(){}
    Edge(int a,int b,int c):from(a),to(b),val(c){    }

};

const int maxn = 2500000;
Edge edges[maxn];
int used[maxn] = {0};
priority_queue<Node> q;
int VNUM,ENUM,a,b,c;

void read(){
    freopen("../test.in","r",stdin);
    scanf("%d %d",&VNUM,&ENUM);
    int idx = 0;
    for(int i = 0;i < ENUM;i++,idx+=2){
        scanf("%d %d %d",&a,&b,&c);
        // 存储两次
        edges[idx] = Edge(a,b,c);
        edges[idx + 1] = Edge(b,a,c);
    }
}
int main(){
    read();


    clock_t start_time,end_time;
    start_time = clock();
    q.push(Node(0,0)); // 第一个点
    int cnt = 0;
    int ans = 0;
    while(!q.empty() && cnt <= VNUM){
        int now = q.top().index;
        int w = q.top().w;
        q.pop();

        if (used[now]) continue; // 已经处理过
        used[now] = 1;
        cnt++;
        ans += w;
        // 遍历与当前结点相连接的边
        for(int i = 0; i < 2 * ENUM;i++){
            if(now == edges[i].from && !used[edges[i].to]){
                q.push(Node(edges[i].to,edges[i].val));
            }
        }
        
    }

    end_time = clock();
    printf("Min Weights:%d Use time:%.5f",ans,(double)(end_time - start_time) / CLOCKS_PER_SEC);
    return 0;
}