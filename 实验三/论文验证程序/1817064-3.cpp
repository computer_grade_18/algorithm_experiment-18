#include<iostream>
#include<iomanip>
using namespace std;
int maxIndex(int n,float max,float a[5])
{
	int index=0;
	for(int i = 1; i < n; i++)
	{
        if(a[i] > max)
		{
            max = a[i];
            index = i;
        }
    }
	return index;
}
 
int main()
{
    int n=5;   
    float c=100; 
    float res = 0;  
    
    
	float v[5]={30,20,15,25,25};   
	float w[5]={40,20,30,10,45};   
    float a[5];  
	float &max = a[0];  
    int index = 0;  
	float visit[5]={0}; 
 
	
	for(int i=0;i<5;i++)
		a[i]=v[i]/w[i];
    index =maxIndex(n,max,a);
    
    while(c > 0)
	{
        if(w[index] <= c)
		{
			visit[index]=1;
            res += v[index];
			c -= w[index];  
            a[index] = 0;   
			index =maxIndex(n,max,a);
        }
		else
		{  
            res += c * a[index];  
			visit[index]=c/w[index];
            c = 0;
        }
    }
	cout<<setiosflags(ios::fixed)<<setprecision(2);
	cout<<"物品取得量："<<endl;
	for(int i=0;i<n;i++)
	{
		cout<<"第"<<i+1<<"个物品：";
		cout<<visit[i]<<"   "<<visit[i]*v[i]<<endl;
	}
    cout<<endl<<"最大价值是："<<res<<endl;
    return 0;
}

