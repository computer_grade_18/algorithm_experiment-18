#include<iostream>
#include<cstdio>
#include<algorithm>
#include<queue>
#include<time.h>
#include<math.h>
#include<windows.h>
#define INF 9999
#define N 22
using namespace std;
int mp[N][N];
int dp[N][1<<N];
int inq[N];
int n;
int mp1[N+1][N+1];

//void in()
//{
//    cin>>n;
//    for(int i=0;i<n;i++)
//    {
//        for(int j=0;j<n;j++)
//        {
//			cin>>mp[i][j];
//			if(i==j)
//            {
//                mp[i][j]=INF;
//                continue;
//            }
//            
//        }
//    }
//}

//void out()
//{
//	for(int i=0;i<n;i++){
//		for(int j=0;j<n;j++){
//			cout<<mp[i][j];
//		}
//		cout<<endl;
//	}
//}
//动态规划 
int solve1()
{
    int s=(1<<(n-1));
    dp[0][0]=0;
    for(int i=1;i<n;i++)
    {
        dp[i][0]=mp[i][0];
    }
    dp[0][(s-1)]=INF;
    for(int j=1;j<(s-1);j++)//总共有n-1个点，但不能全部取
    {
        for(int i=1;i<n;i++) //把1~(n-1)这n-1个点，映射为集合对应的二进制数中的							  0~(n-2)位
        {
 
            if((j&(1<<(i-1)))==0)//i不在集合中
            {
                int m=INF;
                for(int k=1;k<n;k++)
                {
                    if((j&(1<<(k-1)))>0)//k在集合中
                    {
                        int tmp=dp[k][(j-(1<<(k-1)))]+mp[i][k];
                        if(m>tmp)
                        m=tmp;
                    }
                }
                dp[i][j]=m;
            }
        }
    }
    dp[0][s-1]=INF;
    for(int i=1;i<n;i++)
    {
        dp[0][s-1]=min(dp[0][s-1],mp[0][i]+dp[i][(s-1)-(1<<(i-1))]);
    }
    return dp[0][s-1];
}
//贪心算法 
int solve2(int u,int k,int l)//深度遍历 
{	
	
	if(n==1) return INF;
    if(k==n) return l+mp[u][0];//回到出发点，所以要加上最后那个点到出发点的距离 
    int minlen=INF,p;
    for(int i=0; i<n; i++)
    {
        if(inq[i]==0&&minlen>mp[u][i])/*取与所有点的连边中最小的边*/
        {
            minlen=mp[u][i];
            p=i;
        }
        
    }
//	cout<<minlen<<"!"<<endl; 
    inq[p]=1;
    return solve2(p,k+1,l+minlen);
}
//分支限界法 
struct node
{
    int visp[N];//标记哪些点走了
    int st;//起点
    int st_p;//起点的邻接点
    int ed;//终点
    int ed_p;//终点的邻接点
    int k;//走过的点数
    int sumv;//经过路径的距离
    int lb;//目标函数的值
    bool operator <(const node &p )const
    {
        return lb>p.lb;
    }
};
priority_queue<node> q;
int low,up;
int inq1[N+1];
//确定上界
int dfs(int u,int k,int l)
{
    if(k==n) return l+mp1[u][1];
    int minlen=INF,p;
    for(int i=1; i<=n;i++)
    {
        if(inq1[i]==0&&minlen>mp1[u][i])/*取与所有点的连边中最小的边*/
        {
            minlen=mp1[u][i];
            p=i;
        }
    }
    inq1[p]=1;
    return dfs(p,k+1,l+minlen);
}
int get_lb(node p)
{
    int ret=p.sumv*2;//路径上的点的距离
    int min1=INF,min2=INF;//起点和终点连出来的边
    for(int i=1; i<=n; i++)
    {
        if(p.visp[i]==0&&min1>mp1[i][p.st])
        {
            min1=mp1[i][p.st];
        }
    }
    ret+=min1;
    for(int i=1; i<=n; i++)
    {
        if(p.visp[i]==0&&min2>mp1[p.ed][i])
        {
            min2=mp1[p.ed][i];
        }
    }
    ret+=min2;
    for(int i=1; i<=n; i++)
    {
        if(p.visp[i]==0)
        {
            min1=min2=INF;
            for(int j=1; j<=n; j++)
            {
                if(min1>mp1[i][j])
                min1=mp1[i][j];
            }
            for(int j=1; j<=n; j++)
            {
                if(min2>mp1[j][i])
                min2=mp1[j][i];
            }
            ret+=(min1+min2);
        }
    }
    return ret%2==0?(ret/2):(ret/2+1);
}
void get_up()
{
    inq1[1]=1;
    up=dfs(1,1,0);
 //   cout<<"up="<<up<<endl;
}
void get_low()
{
    low=0;
    for(int i=1; i<=n; i++)
    {
        /*通过排序求两个最小值*/
        int min1=INF,min2=INF;
        int tmpA[N];
        for(int j=1; j<=n; j++)
        {
            tmpA[j]=mp1[i][j];
        }
        sort(tmpA+1,tmpA+1+n);//对临时的数组进行排序
        low+=tmpA[1];
    }
}
int solve3()
{
    /*贪心法确定上界*/
    get_up();
    
    /*取每行最小的边之和作为下界*/
    get_low();
    
    /*设置初始点,默认从1开始 */
    node star;
    star.st=1;
    star.ed=1;
    star.k=1;
    for(int i=1; i<=n; i++) star.visp[i]=0;
    star.visp[1]=1;
    star.sumv=0;
    star.lb=low;
    
    /*ret为问题的解*/
    int ret=INF;
    
    q.push(star);
    while(!q.empty())
    {
        node tmp=q.top();
        q.pop();
        if(tmp.k==n-1)
        {
            /*找最后一个没有走的点*/
            int p;
            for(int i=1; i<=n; i++)
            {
                if(tmp.visp[i]==0)
                {
                    p=i;
                    break;
                }
            }
            int ans=tmp.sumv+mp1[p][tmp.st]+mp1[tmp.ed][p];
            node judge = q.top();
            
            /*如果当前的路径和比所有的目标函数值都小则跳出*/
            if(ans <= judge.lb)
            {
                ret=min(ans,ret);
                break;
            }
            /*否则继续求其他可能的路径和，并更新上界*/
            else
            {
                up = min(up,ans);
                ret=min(ret,ans);
                continue;
            }
        }
        /*当前点可以向下扩展的点入优先级队列*/
        node next;
        for(int i=1; i<=n; i++)
        {
            if(tmp.visp[i]==0)
            {
                next.st=tmp.st;
 
                /*更新路径和*/
                next.sumv=tmp.sumv+mp1[tmp.ed][i];
 
                /*更新最后一个点*/
                next.ed=i;
 
                /*更新顶点数*/
                next.k=tmp.k+1;
 
                /*更新经过的顶点*/
                for(int j=1; j<=n; j++) next.visp[j]=tmp.visp[j];
                next.visp[i]=1;
 
                /*求目标函数*/
                next.lb=get_lb(next);
                
                /*如果大于上界就不加入队列*/
                if(next.lb>up) continue;
                q.push(next);
            }
        }
    }
    return ret;
}
//主函数 
int main()
{	
	double z;                
	LARGE_INTEGER nFreq;          //时间
	LARGE_INTEGER nBeginTime;
	LARGE_INTEGER nEndTime;
	QueryPerformanceFrequency(&nFreq);
	srand((unsigned)time(NULL));  //产生随机数种子，避免产生的随机数相同
	cout<<"请输入一个数，用来定义二维数组长度："; 
	cin>>n;
	for(int i=0;i<n;i++)
    {
        for(int j=0;j<n;j++)
        {
			mp[i][j]=rand()%100;
			if(i==j)
            {
                mp[i][j]=INF;
                continue;
            }
            
        }
    }
	int k,l;
	for(k=0;k<n;k++){
		for(l=0;l<n;l++){
			mp1[k+1][l+1]=mp[k][l];
		} 	
	} 
//	out();
	cout<<"动态规划:"<<endl;
	QueryPerformanceCounter(&nBeginTime);//开始计时
    cout<<solve1()<<endl;
    QueryPerformanceCounter(&nEndTime);//结束计时
	z=(double)(nEndTime.QuadPart-nBeginTime.QuadPart)/(double)nFreq.QuadPart;//计算程序执行时间单位为s
	cout<<"动态规划所花费的时间："<<z*1000<<"毫秒"<<endl;
	
    cout<<"贪心算法:"<<endl;
    for(int i=0;i<n;i++)
	inq[i]=0;//初始化inq数组 
    inq[0]=1;
    QueryPerformanceCounter(&nBeginTime);//开始计时
    cout<<solve2(0,1,0)<<endl;
    QueryPerformanceCounter(&nEndTime);//结束计时
	z=(double)(nEndTime.QuadPart-nBeginTime.QuadPart)/(double)nFreq.QuadPart;//计算程序执行时间单位为s
	cout<<"贪心算法所花费的时间："<<z*1000<<"毫秒"<<endl;
	
    cout<<"分支限界法"<<endl;
    QueryPerformanceCounter(&nBeginTime);//开始计时
    cout<<solve3()<<endl;
    QueryPerformanceCounter(&nEndTime);//结束计时
	z=(double)(nEndTime.QuadPart-nBeginTime.QuadPart)/(double)nFreq.QuadPart;//计算程序执行时间单位为s
	cout<<"分支限界所花费的时间："<<z*1000<<"毫秒"<<endl;
    return 0;
}
