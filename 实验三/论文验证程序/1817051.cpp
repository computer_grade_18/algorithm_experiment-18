#include<bits/stdc++.h>
#include<time.h>
#include<math.h>
#include<windows.h>
#define INF 9999
using namespace std;
struct node
{
    int visp[22];//标记哪些点走了
    int st;//起点
    int st_p;//起点的邻接点
    int ed;//终点
    int ed_p;//终点的邻接点
    int k;//走过的点数
    int sumv;//经过路径的距离
    int lb;//目标函数的值
    bool operator <(const node &p )const
    {
        return lb>p.lb;
    }
};
int mp[22][22];//最少3个点，最多15个点
int n=10;
priority_queue<node> q;
int low,up;
int inq[22];
int inq_t[22];
void in()
{
    for(int i=1;i<=n;i++)
    {
        for(int j=1;j<=n;j++)
        {
            if(i==j)
            {
                mp[i][j]=INF;
                continue;
            }
            mp[i][j]=rand()%20;
        }
    }
}
int dp[22][1<<22];
//动态规划法 
int solve()
{
	int mp_d[22][22];
	for(int i=0;i<n;i++)
    {
        for(int j=0;j<n;j++)
        {
            mp_d[i][j]=mp[i+1][j+1];
        }
    }
    int s=(1<<(n-1));
    dp[0][0]=0;
    for(int i=1;i<n;i++)
    {
        dp[i][0]=mp_d[i][0];
    }
    dp[0][(s-1)]=INF;
    for(int j=1;j<(s-1);j++)//总共有n-1个点，但不能全部取
    {
 
 
        for(int i=1;i<n;i++) //把1~(n-1)这n-1个点，映射为集合对应的二进制数中的							  0~(n-2)位
        {
 
            if((j&(1<<(i-1)))==0)//i不在集合中
            {
                int m=INF;
                for(int k=1;k<n;k++)
                {
                    if((j&(1<<(k-1)))>0)//k在集合中
                    {
                        int tmp=dp[k][(j-(1<<(k-1)))]+mp_d[i][k];
                        if(m>tmp)
                        m=tmp;
                    }
                }
                dp[i][j]=m;
            }
        }
    }
    dp[0][s-1]=INF;
    for(int i=1;i<n;i++)
    {
        dp[0][s-1]=min(dp[0][s-1],mp_d[0][i]+dp[i][(s-1)-(1<<(i-1))]);
    }
    return dp[0][s-1];
}
//贪心法
int dfs(int u,int k,int l)
{
    if(k==n) return l+mp[u][1];
    int minlen=INF , p;
    for(int i=1; i<=n; i++)
    {
        if(inq_t[i]==0&&minlen>mp[u][i])/*取与所有点的连边中最小的边*/
        {
            minlen=mp[u][i];
            p=i;
        }
    }
    inq_t[p]=1;
    return dfs(p,k+1,l+minlen);
} 
//确定上界
int dfs_f(int u,int k,int l)
{
    if(k==n) return l+mp[u][1];
    int minlen=INF , p;
    for(int i=1; i<=n; i++)
    {
        if(inq[i]==0&&minlen>mp[u][i])/*取与所有点的连边中最小的边*/
        {
            minlen=mp[u][i];
            p=i;
        }
    }
    inq[p]=1;
    return dfs(p,k+1,l+minlen);
}
int get_lb(node p)
{
    int ret=p.sumv*2;//路径上的点的距离
    int min1=INF,min2=INF;//起点和终点连出来的边
    for(int i=1; i<=n; i++)
    {
        if(p.visp[i]==0&&min1>mp[i][p.st])
        {
            min1=mp[i][p.st];
        }
    }
    ret+=min1;
    for(int i=1; i<=n; i++)
    {
        if(p.visp[i]==0&&min2>mp[p.ed][i])
        {
            min2=mp[p.ed][i];
        }
    }
    ret+=min2;
    for(int i=1; i<=n; i++)
    {
        if(p.visp[i]==0)
        {
            min1=min2=INF;
            for(int j=1; j<=n; j++)
            {
                if(min1>mp[i][j])
                min1=mp[i][j];
            }
            for(int j=1; j<=n; j++)
            {
                if(min2>mp[j][i])
                min2=mp[j][i];
            }
            ret+=min1+min2;
        }
    }
    return ret%2==0?(ret/2):(ret/2+1);
}
void get_up()
{
    inq[1]=1;
    up=dfs_f(1,1,0);
}
void get_low()
{
    low=0;
    for(int i=1; i<=n; i++)
    {
        /*通过排序求两个最小值*/
        int min1=INF,min2=INF;
        int tmpA[22];
        for(int j=1; j<=n; j++)
        {
            tmpA[j]=mp[i][j];
        }
        sort(tmpA+1,tmpA+1+n);//对临时的数组进行排序
        low+=tmpA[1];
    }
}
int solve_f()
{
    /*贪心法确定上界*/
    get_up();
    
    /*取每行最小的边之和作为下界*/
    get_low();
    
    /*设置初始点,默认从1开始 */
    node star;
    star.st=1;
    star.ed=1;
    star.k=1;
    for(int i=1; i<=n; i++) star.visp[i]=0;
    star.visp[1]=1;
    star.sumv=0;
    star.lb=low;
    
    /*ret为问题的解*/
    int ret=INF;
    
    q.push(star);
    while(!q.empty())
    {
        node tmp=q.top();
        q.pop();
        if(tmp.k==n-1)
        {
            /*找最后一个没有走的点*/
            int p;
            for(int i=1; i<=n; i++)
            {
                if(tmp.visp[i]==0)
                {
                    p=i;
                    break;
                }
            }
            int ans=tmp.sumv+mp[p][tmp.st]+mp[tmp.ed][p];
            node judge = q.top();
            
            /*如果当前的路径和比所有的目标函数值都小则跳出*/
            if(ans <= judge.lb)
            {
                ret=min(ans,ret);
                break;
            }
            /*否则继续求其他可能的路径和，并更新上界*/
            else
            {
                up = min(up,ans);
                ret=min(ret,ans);
                continue;
            }
        }
        /*当前点可以向下扩展的点入优先级队列*/
        node next;
        for(int i=1; i<=n; i++)
        {
            if(tmp.visp[i]==0)
            {
                next.st=tmp.st;
 
                /*更新路径和*/
                next.sumv=tmp.sumv+mp[tmp.ed][i];
 
                /*更新最后一个点*/
                next.ed=i;
 
                /*更新顶点数*/
                next.k=tmp.k+1;
 
                /*更新经过的顶点*/
                for(int j=1; j<=n; j++) next.visp[j]=tmp.visp[j];
                next.visp[i]=1;
 
                /*求目标函数*/
                next.lb=get_lb(next);
                
                /*如果大于上界就不加入队列*/
                if(next.lb>up) continue;
                q.push(next);
            }
        }
    }
    return ret;
}
int main()
{
	double z;             
	LARGE_INTEGER nFreq;          //时间
	LARGE_INTEGER nBeginTime;
	LARGE_INTEGER nEndTime;
	QueryPerformanceFrequency(&nFreq);
	in();
	//动态规划算法 
	printf("城市连接图：\n");
	for(int i=0;i<=n;i++){
		for(int j=0;j<=n;j++){
			printf("%d ",mp[i][j]);
		}
		printf("\n");
	}
	QueryPerformanceCounter(&nBeginTime);//开始计时
    printf("动态规划答案%d\n",solve());
    QueryPerformanceCounter(&nEndTime);//结束计时
	z=(double)(nEndTime.QuadPart-nBeginTime.QuadPart)/(double)nFreq.QuadPart;//计算程序执行时间单位为s
	cout<<"动态规划所花费的时间："<<z*1000<<"毫秒"<<endl;
	QueryPerformanceCounter(&nBeginTime);//开始计时
    printf("贪心算法答案%d\n",dfs(1,1,0));
    QueryPerformanceCounter(&nEndTime);//结束计时
	z=(double)(nEndTime.QuadPart-nBeginTime.QuadPart)/(double)nFreq.QuadPart;//计算程序执行时间单位为s
	cout<<"贪心算法所花费的时间："<<z*1000<<"毫秒"<<endl;
	QueryPerformanceCounter(&nBeginTime);//开始计时
    printf("分支界限法答案%d\n",solve_f());
    QueryPerformanceCounter(&nEndTime);//结束计时
	z=(double)(nEndTime.QuadPart-nBeginTime.QuadPart)/(double)nFreq.QuadPart;//计算程序执行时间单位为s
	cout<<"分支界限法所花费的时间："<<z*1000<<"毫秒"<<endl;
    return 0;
}
