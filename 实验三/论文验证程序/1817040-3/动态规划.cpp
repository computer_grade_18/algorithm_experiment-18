//0/1背包问题-动态规划（自上而下，最优解在右下角） 
#include<time.h>
#include <stdlib.h>
#include <iostream>
#include<windows.h>
using namespace std;
int m[9999][9999];
void findmax(int v[],int w[],int c,int n)   //填二维数组表格    
{
	int i,j;
	for(i = 0;i <= n;i++)		//边界置0
		m[i][0] = 0;
	for (j = 0;j <= c;j++)		//边界置0
		m[0][j] = 0;
	for (i = 1;i <= n;i++)
	{  
		for (j = 1;j <= c;j++)
			if (j < w[i])      //若重量大于背包容量，不放入 
				m[i][j] = m[i-1][j];//重量就等于上一行的重量 
			else
				m[i][j] = max(m[i-1][j],m[i-1][j-w[i]]+v[i]); 
	}           //若重量小于背包容量 将m[i-1][j]进行比较m[i-1][j-w[i]]+v[i]选择价值较大的 
	cout<<m[n][c]<<endl;  //输出最优的价值 
}
 void findwhat(int w[],int c,int n,int x[])     //最优解回溯 
 {
 	int i=n,r=c;                 //r作为临时变量 
	while (i>0)				    //判断每个物品
	{
		if (m[i][r] != m[i-1][r]) 
		{ 
			x[i] = 1;		 //选取物品i，累计总价值		
			r = r - w[i]; 	//剩下背包容量 
		}
		else
			x[i]=0;		 //不选取物品i
		    i--;        //回到上一行 
	}
 }
int main()
 {
 int n,c;
 srand((unsigned)time(NULL));//设计随机数种子
 double d;
 LARGE_INTEGER frequency,start,end;
 QueryPerformanceFrequency(&frequency);
  cout<<"物品个数n：";
  cin>>n; 
  cout<<"背包容量c：";
  cin>>c; 
  int v[n+1]={0},w[n+1]={0};
 cout<<"物品价值v：";
 for(int i=1;i<=n;i++)                
 {
 	v[i]=(rand() % 19)+1; //利用rand()函数生成随机数
 }
  cout<<"物品重量w："; 
 for(int i=1;i<=n;i++)
 {
 	w[i]=(rand() % 19)+1;//利用rand()函数生成随机数
 }
 int x[n]={0};
 QueryPerformanceCounter(&start); 
 cout<<"最优价值为：";
findmax(v,w,c,n);            
cout<<"物品的选择："<<endl;;
findwhat(w,c,n,x);          
QueryPerformanceCounter(&end);
d = (double)(end.QuadPart- start.QuadPart) /(double)frequency.QuadPart* 1000.0;
cout<<d<<"s";
 }
