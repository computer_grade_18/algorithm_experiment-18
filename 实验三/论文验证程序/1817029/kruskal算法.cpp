#include<cstdio>
#include<algorithm>
using namespace std;
const int MAXV=100;
const int MAXE=10000;
struct edge{                  //边集定义部分 
	int u,v;                  //边的两个端点编号 
	int cost;                 //边权 
}E[MAXE];                     //最多有MAXV条边 
bool cmp(edge a,edge b){   
	return a.cost<b.cost;
}
int father[MAXV];             //并查集数组 
int findFather(int x){        // 并查集查询函数 
	int a=x;
	while(x!=father[x]){
		x=father[x];
	} 
	while(a!=father[a]){
		int z=a;
		a=father[a];
		father[z]=x;
	}
	return x;
} 
//kruskal部分，返回最小生成树及其边权之和，n为顶点个数，m为图的边数 
int kruskal(int n,int m)       
{
	printf("最小生成树为：\n");
	int ans=0,numEdge=0;    //ans为所求边权之和，numEdge为当前生成树的边数 
	for(int i=0;i<n;i++)    //顶点范围为[0,n-1] 
	{
		father[i]=i;        //并查集初始化 
	}
	sort(E,E+m,cmp);        //所有边按边权从小到大排序 
	for(int i=0;i<m;i++)    //枚举所有边 
	{
		int faU=findFather(E[i].u);    //查询测试边的两个端点所在集合的根结点 
		int faV=findFather(E[i].v);
		if(faU!=faV)                   //如果不在一个集合中 
		{
			father[faU]=faV;           //把测试边加入最小生成树中 
			ans+=E[i].cost;            //边权之和增加测试边的边权 
			numEdge++;                 //当前生成树的边数加1 
			printf("V%d--V%d=%d\n",E[i].u,E[i].v,E[i].cost);  //输出最小生成树的边及其边权 
			if(numEdge==n-1) break;    //边数等于顶点数减1时结束算法 
		}
	}
	if(numEdge!=n-1) printf("输入图非无向连通图\n");  //生成树无法连通，提示错误 
	else printf("最小生成树边权之和=%d\n",ans);       //输出最小生成树的边权之和 
}
int main()
{
	int n,m;
	scanf("%d%d",&n,&m);     //顶点数、边数 
	for(int i=0;i<m;i++)     //输入无向连通图 
	{
		scanf("%d%d%d",&E[i].u,&E[i].v,&E[i].cost);
	}
    kruskal(n,m);            //执行kruskal算法函数 
	return 0;
}
