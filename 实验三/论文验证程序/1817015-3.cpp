#include <iostream>

#include <algorithm>

using namespace std;

struct Book{

    int u,v,w;

}book[125010];

int V[520],ans = 0;

int p[520],r[520];

int G[520][520]={0};

bool cmp(Book a,Book b){

    if(a.w!=b.w)

        return a.w<b.w;

    return false;

}

void Init(int n)

{

    for(int i=0;i<=n;i++)

    {

        r[i] = 0;

        p[i] = i;

    }

}

int Find(int x){

    return x==p[x]?x:p[x]=Find(p[x]);

}

void Union(int x,int y)

{

    x = Find(x);y=Find(y);

    if(x==y)return ;

    if(r[x]>r[y])

        p[y] = x;

    else if(r[y]>r[x])

        p[x] = y;

    else{

        p[x] = y;

        r[y]++;

    }

}

int main()

{

    //freopen("in","r",stdin);

    int n,m;scanf("%d%d",&n,&m);Init(n);

    for(int i=0;i<m;i++)

        scanf("%d%d%d",&book[i].u,&book[i].v,&book[i].w);

    sort(book,book+m,cmp);

    int cnt = 0,all = 0,flag = 0;

    for(int i=0;i<m;i++){

        int pu,pv;

        int u = book[i].u,v=book[i].v;

        u=Find(u),v=Find(v);

        if(u!=v){

            for(int j=i+1;j<m;j++){

                if(book[j].w==book[i].w){

                    pu=Find(book[j].u);pv=Find(book[j].v);

                    if((pu==u&&pv==v)||(pv==u&&pu==v))

                        flag=1;

                }else{

                    break;

                }

            }

            Union(u,v);

            cnt++;

            all+=book[i].w;

        }

    }

    if(cnt==n-1)

    {

        printf("%d\n",all);

        if(flag)

            printf("No\n");

        else

            printf("Yes\n");

    }else{

        printf("No MST\n");

        cnt = 0;

        for(int i=1;i<=n;i++)

            if(p[i]==i)cnt++;

        printf("%d\n",cnt);

    }

    return 0;

}