//多个解的情况 
#include<iostream>
#include<cstdlib>
#include<math.h>
#include<windows.h>
#include<time.h>
#include <stack>
#include"stdio.h"
#define Maxsize 100
#include <queue> 
#include<algorithm>
#define V 12 
#define N2 10 
using namespace std;
int n=N2-1;//物品数量
int c=V-1;//背包容量
int w[N2]={0,2,2,6,5,4,0,0,0,0};		//重量，下标0不用
int v[N2]={0,6,3,5,4,6,0,0,0,0};
int X=0,Y=0; 
int x[N2-1]={0};
int bag[N2*10];
int count1=0;//记录该装载方案的总价值
int best=0;//记录最佳装载方案的总价值
int wei=V-1;
int y[5000];//记录最佳装载的方案
int maxv=-9999;				//存放最大价值,初始为最小值
int bestx[N2];			//存放最优解,全局变量
int total=1;
int z=0;

struct Element{             
	double w1;
	double v1;
	double p;
};
int cmp(const Element&a,const Element &b)
{
	return a.p>b.p;
}
double value;
Element A[N2]={0}; 
void knapsack1()                   //贪心算法 
{ 
	value=0;
	double weight=wei;
	memset(x,0,sizeof(x));
	int i=1;
	while(A[i].w1<=weight)
	{
		x[i]=1;
		weight-=A[i].w1;
		value+=A[i].v1;
		i++;
	}

}
void procedure(int k,int wei,int n)
 {  
     for(int i=0;i<=1;i++){//0为装入1位不装入
           if(wei-w[k]*i>=0){//如果还有剩余空间,实现了剪枝
             bag[k]=i;//记录该物品是否被装入
             count1=count1+v[k]*i;//总价值
             if(k==n && count1>best){//此时装载方案是目前已知的装载方案最优
                 X=0;
                 best=count1;//更新这个最优总价值
                 for(int j=1;j<=n;j++){//记录此时背包装入状态
                     y[j]=bag[j];
                 }
             }
             if(k==n && count1 ==best){//此时装载方案是目前已知的装载方案最优
                 
                 for(int i=1,j=1;i<=n*(X+1);i++,j++)
                 {  Y=0;
                 	if(bag[j]==y[i])
                 	z++;
                 
                 	if(z==n)
                 	{   
                 		Y=1;
                 		break;
					}
					if(i%n==0)
					{
						j=1;
						z=0;
					}	 
				 }
				 if(Y==0)
			 {    X=X+1;
                 for(int j=1;j<=n;j++){//记录此时背包装入状态
                     y[X*n+j]=bag[j];
                 }
             }
         }
            if(k<n){//k==n递归
                 procedure(k+1,wei-w[k]*i,n);
             }
             count1-=v[k]*i;//k<n回溯上一步
           }
     }
 }
int main(void) 
{   
    LARGE_INTEGER frequency,start,end;
 QueryPerformanceFrequency(&frequency);
    double d;
for(int i=1;i<n;i++)                   
 {
 	A[i].v1=v[i];
 	A[i].w1=w[i];
 }
  QueryPerformanceCounter(&start);
 for(int i=1;i<=n;i++)
{
	A[i].p=A[i].v1/A[i].w1;
}
sort(A+1,A+n+1,cmp);
knapsack1();
 QueryPerformanceCounter(&end);
 for(int j=1;j<=n;j++)
{
	cout<<"第"<<j<<"个物品的重量："<<A[j].w1<<" 价值："<<A[j].v1<<endl;
}
cout<<"0-1背包-贪心算法的最高价值："<<value<<endl;
 d = (double )(end.QuadPart- start.QuadPart) /(double )frequency.QuadPart* 1000.0; 
 cout<<"0-1背包-贪心算法："<<d<<"毫秒"<<endl;

for(int j=1;j<=n;j++)
{
	cout<<x[j]<<" ";      //输出物品的选择情况 
} 	cout<<endl;
QueryPerformanceCounter(&start);
procedure(0,wei,n);   //回溯法
 QueryPerformanceCounter(&end);
 d = (double)(end.QuadPart- start.QuadPart) /(double)frequency.QuadPart* 1000.0; 
   cout <<"0-1背包-回溯法的最高价值：" ;
   cout << best << endl;       
  cout<<"0-1背包回溯法："<<d<<"毫秒"<<endl;       
  for(int i=1;i<=n*(X/2+1);i++){
        cout<<y[i]<<" ";
        if(i%n==0)
        cout<<endl;
         }
         cout<<endl; 
  return 0;
}
