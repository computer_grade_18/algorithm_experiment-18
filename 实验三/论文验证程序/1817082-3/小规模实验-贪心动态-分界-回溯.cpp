//小规模数据，0-1背包问题各种算法比较 
#include<iostream>
#include<cstdlib>
#include<math.h>
#include<windows.h>
#include<time.h>
#include <stack>
#include"stdio.h"
#define Maxsize 10000 
#include <queue>
#include <string.h> 
#include<algorithm>
#include <stdlib.h> 
#define V 101    //物容量大小+1
#define N2 31    //物品个数+1 
using namespace std;
int n=N2-1;//物品数量
int c=V-1;//背包容量
int w[N2]={0};		//重量，下标0不用
int v[N2]={0}; 
int bag[N2];
int count1=0;//记录该装载方案的总价值
int best=0;//记录最佳装载方案的总价值
int wei=V-1;
int y[N2];//记录最佳装载的方案
int maxv=-9999;				//存放最大价值,初始为最小值
int bestx[N2];			//存放最优解,全局变量
int total=1;
int x[N2-1]={0};
struct Element{             
	double w1;
	double v1;
	double p;
};
int cmp(const Element&a,const Element &b)
{
	return a.p>b.p;
}
double value;
Element A[N2]={0}; 
void knapsack()                   //贪心算法 
{ 
	value=0;            //初始最大价值为0 
	double weight=wei;            
	memset(x,0,sizeof(x));     //初始化x向量 
	int i=1;
	while(A[i].w1<=weight)     //物品全能装入时 
	{
		x[i]=1;                 // 装入物品 
		weight-=A[i].w1;       //减少背包的容量 
		value+=A[i].v1;        //价值进行累加 
		i++;
	}

}
void solve(int v[],int w[],int c,int n,int m[][V]) //从下而上动态规划 
{ 
	int jmax=min(w[n]-1,c);
	for(int j=0;j<=jmax;j++)
	{
		m[n][j]=0;
	}
	for(int j=w[n];j<=c;j++)
	{
		m[n][j]=v[n];
	}
	for(int i=n-1;i>=1;i--)
	{
		int jmax=min(w[i]-1,c);
		for(int j=0;j<=jmax;j++)
	{
		m[i][j]=m[i+1][j];
	}
	for(int j=w[i];j<=c;j++)
	{
		m[i][j]=max(m[i+1][j],m[i+1][j-w[i]]+v[i]);
	}
	}
	cout<<m[1][c];
 } 
  void traceback1(int m[][V],int w[],int c,int n,int x[])
 {
 	for(int i=1;i<n;i++)
 	{
 		if(m[i][c]==m[i+1][c])
 		x[i]=0;
 		else
 		{
 			x[i]=1;c=c-w[i];
		}
	 }
	 x[n]=(m[n][c]>0?1:0);
 }
 void dynamic_top(int v[],int w[],int c,int n,int m[][V])      //从上而下动态规划 
{
	int i,j;
	for(i = 0;i <= n;i++)		//置边界条件m[i][0]=0
		m[i][0] = 0;
	for (j = 0;j <= c;j++)		//置边界条件m[0][r]=0
		m[0][j] = 0;
	for (i = 1;i <= n;i++)
	{  
		for (j = 1;j <= c;j++)
			if (j < w[i])
				m[i][j] = m[i-1][j];
			else
				m[i][j] = max(m[i-1][j],m[i-1][j-w[i]]+v[i]);
	}
	cout<<m[n][c]<<endl;
}
void traceback2(int m[][V],int w[],int c,int n,int x[])
 {
 	int i=n,r=c;

	while (i>0)				//判断每个物品
	{
		if (m[i][r] != m[i-1][r]) 
		{  
			x[i] = 1;		//选取物品i		//累计总价值
			r = r - w[i];
		}
		else
			x[i]=0;		//不选取物品i
		i--;
	}
 }
void procedure(int k,int wei,int n)    //回溯法 
 {
 
     for(int i=0;i<=1;i++){//0为装入1位不装入
           if(wei-w[k]*i>=0){//如果还有剩余空间,实现了剪枝
             bag[k]=i;//记录该物品是否被装入
             count1=count1+v[k]*i;//总价值
             if(k==n && count1 >best){//此时装载方案是目前已知的装载方案最优
                 best=count1;//更新这个最优总价值
                 for(int j=1;j<=n;j++){//记录此时背包装入状态
                     y[j]=bag[j];
                 }
             }
            if(k<n){//k==n递归
                 procedure(k+1,wei-w[k]*i,n);
             }
             count1-=v[k]*i;//k<n回溯上一步
           }
     }
 }
class object                                                //分支限界法 
{
	friend int knapsack(int *,int *,int ,int);
public:
	int operator < (object a)const
	{
		return (ratio>a.ratio);
	}
private:
	int ID;
	double ratio;
};

class node
{
	friend class knap;
	friend int knapsack(int *,int *,int ,int);
private:
	node *parent;
	bool lchild;
};

class queueNode
{
	friend class knap;
public:
	friend bool operator < (queueNode a,queueNode b)
	{
		if(a.uprofit<b.uprofit) return true;
		else return false;
	}
private:
	int uprofit;
	int profit;
	int weight;
	int level;
	node *ptr;
};

class knap
{
	friend int knapsack(int *,int *,int ,int);
public:
	int maxKnapsack();
private:
	priority_queue <queueNode> H;
	int bound(int i);
	void addLiveNode(int up,int cp,int cw,bool ch,int level);
	node *E;
	int c;
	int n;
	int w[N2];
	int p[N2];
	int cw;
	int cp;
	int bestx[N2];
};

int knap::bound(int i)
{
	int cleft = c-cw;
	int b = cp;
	while(i<=n&&w[i]<=cleft)
	{
		cleft -= w[i];
		b += p[i];
		i++;
	}
	if(i<=n) b += 1.0*cleft*p[i]/w[i];
	return b;
}

void knap::addLiveNode(int up,int cp,int cw,bool ch,int lev)
{
	node *b = new node;
	b->parent = E;
	b->lchild = ch;
	queueNode N;
	N.uprofit = up;
	N.profit = cp;
	N.weight = cw;
	N.level = lev;
	N.ptr = b;
	H.push(N);
}

int knap::maxKnapsack()
{	
	int i = 1;
	E = 0;
	cw = cp = 0;
	int bestp = 0;
	int up = bound(1);
	while(i!=n+1)
	{
		int wt = cw+w[i];
		if (wt<=c)
		{
			if(cp+p[i]>bestp) bestp = cp+p[i];
			addLiveNode(up,cp+p[i],cw+w[i],true,i+1);
		}
		up = bound(i+1);
		if(up>=bestp)
			addLiveNode(up,cp,cw,false,i+1);
		queueNode N;
		N = H.top();
		H.pop();
		E = N.ptr;
		cw = N.weight;
		cp = N.profit;
		up = N.uprofit;
		i = N.level;
	}
	for(int j = n;j>0;j--)
	{
		bestx[j] = E->lchild;
		E = E->parent;
	}
	return cp;
}

int knapsack(int v[],int w[],int c,int n)    //分支限界法 
{
	int i;
	int bestx[N2];
	object *Q = new object[n];
	for(i=0; i<n; i++)
	{
		Q[i].ID = i+1;
		Q[i].ratio = 1.0*v[i]/w[i];
	}
	stable_sort(Q,Q+n);
	knap K;
	for(i=1; i<=n; i++)
	{
		K.p[i] = v[Q[i-1].ID-1];
		K.w[i] = w[Q[i-1].ID-1];
	}
	K.cp = 0;
	K.cw = 0;
	K.c = c;
	K.n = n;
	int bestp = K.maxKnapsack();
	for(i=1; i<=n; i++)
		bestx[Q[i-1].ID] = K.bestx[i];
	for(i=1; i<=n; i++)
		if(bestx[i]==1) printf("%d ", i);
	printf("\n");
	delete [] Q;
	return bestp;
} 
 
int main(void) 
{   srand((unsigned)time(NULL)); 
    LARGE_INTEGER frequency,start,end;
 QueryPerformanceFrequency(&frequency);
    double  d;
 int m[N2][V]={0};
 for(int i=1;i<n;i++)                    //随机元素1-20 
 {
 	v[i]=(rand() % 19)+1;           
 }
  for(int i=1;i<n;i++)
 {
 	w[i]=(rand() % 19)+1;
 }
 for(int i=1;i<n;i++)                   
 {
 	A[i].v1=v[i];
 	A[i].w1=w[i];
 }
  QueryPerformanceCounter(&start);
 for(int i=1;i<=n;i++)
{
	A[i].p=A[i].v1/A[i].w1;
}
sort(A+1,A+n+1,cmp);
knapsack();
 QueryPerformanceCounter(&end);
cout<<"0-1背包-贪心算法的最高价值："<<value<<endl;
 d = (double )(end.QuadPart- start.QuadPart) /(double )frequency.QuadPart* 1000.0; 
 cout<<"0-1背包-贪心算法："<<d<<"毫秒"<<endl;
 	cout<<endl;

 cout<<"0-1背包-动态规划-从下而上的最高价值："; 
 QueryPerformanceCounter(&start);
 solve(v,w,c,n,m);   //动态规划  
 traceback1(m,w,c,n,x);
 QueryPerformanceCounter(&end);
 d = (double )(end.QuadPart- start.QuadPart) /(double )frequency.QuadPart* 1000.0; 
	cout<<endl;
 cout<<"0-1背包-动态规划-从下而上："<<d<<"毫秒"<<endl;
 /*for(int i=1;i<=n;i++)
{
	for(int j=0;j<=c;j++)
	{
		cout<<m[i][j]<<" ";
	}
	cout<<endl;

  for(int i=1;i<=n;i++)
{

		cout<<x[i]<<" ";
}}*/
  cout<<endl;
  cout<<endl;
  
  
  for(int i=0;i<=n;i++)
{
	for(int j=0;j<=c;j++)  //初始化m[][]数组 
	{
		m[i][j]=0;
	}
}
  cout<<"0-1背包-动态规划-从上而下的最高价值："; 
   QueryPerformanceCounter(&start);
 dynamic_top(v,w,c,n,m);   //动态规划 
 traceback2(m,w,c,n,x);
 QueryPerformanceCounter(&end);
 d = (double)(end.QuadPart- start.QuadPart) /(double)frequency.QuadPart* 1000.0; 
 cout<<"0-1背包-动态规划-从上而下："<<d<<"毫秒"<<endl;
/* for(int i=1;i<=n;i++)
{
	for(int j=0;j<=c;j++)
	{
		cout<<m[i][j]<<" ";
	}
	cout<<endl;

 for(int i=1;i<=n;i++)
{

		cout<<x[i]<<" ";
}
  cout<<endl;
 }*/
 
 
 cout<<endl; 
QueryPerformanceCounter(&start);
procedure(0,wei,n);   //回溯法

 QueryPerformanceCounter(&end);
 d = (double)(end.QuadPart- start.QuadPart) /(double)frequency.QuadPart* 1000.0; 
   cout <<"0-1背包-回溯法的最高价值：" ;
   cout << best << endl;       
  cout<<"0-1背包回溯法："<<d<<"毫秒"<<endl;       
         cout<<endl; 
		 
	  
cout << endl << "0-1背包-分支限界法的物品选择情况：" ;
QueryPerformanceCounter(&start);
cout<<"最终结果"<<knapsack(v,w,c,n)<<endl;
QueryPerformanceCounter(&end);
d = (double)(end.QuadPart- start.QuadPart) /(double)frequency.QuadPart* 1000.0; 
 cout<<"0-1背包-分支限界法："<<d<<"毫秒"<<endl; 
  return 0;

}
