#include <iostream>
#include <algorithm>
#include <iomanip>
#include <cstdio>
using namespace std;
struct name {
    int i;                       //为方便后续输出结果，设置物品序号
    int pi;
    int wi;
    float xi;
    int flag;                   //标志一整个物品被选取
    float floflag;              //标志物品被选取的那一部分
};
int bag, thingNum;               //全局变量 bag与thingnum 
int cmpPi( name a, name b )     //按效益值从大到小排序
{
    return a.pi > b.pi;
}
int cmpWi( name a, name b )     //按重量从小到大排序
{
    return a.wi < b.wi;
}
int cmpXi( name a, name b )     //按比例从大到小排序
{
    return a.xi > b.xi;
}
void show( name *things, int thingNum )
{
    cout<< "物品序号   ";
    for( int i = 0; i < thingNum; i++ )
        cout<<i<<"     ";
    cout<<"总效益\n           " ;
    float ans = 0;                                           //总效益，边打印边计算
    for( int i = 0; i < thingNum; i++ ) {
        for( int j = 0; j < thingNum; j++ ) {
            if( things[j].i == i ) {
                if( things[j].floflag != 0 ) {
                    cout<<things[j].floflag<<"     ";
                    ans += things[j].floflag * things[j].pi ;
                    things[j].floflag = 0;                       //置零初始化
                }
                else {
                    cout<<things[j].flag<<"     ";
                    if( things[j].flag == 1 )
                        ans += things[j].pi ;
                    things[j].flag = 0;                           //置零初始化
                }
                continue;
            }
        }
    }
    cout<<ans<<"     ";
    cout<<endl;
    for( int i = 0; i < 55; i++ )
        cout<<"-" ;
    cout<<endl;
}
void select( name *things )                                     //选取物品
{
    int res = bag;
    for( int i = 0; i < thingNum; i++ ) {
        if( things[i].wi < res ) {
            things[i].flag = 1;
            things[i].floflag = 0;
            res -= things[i].wi;
        }
        else {
            things[i].flag  = 0;
            things[i].floflag = ( 1.0 * res ) / things[i].wi;
            break;
        }
    }
}
void value( name *things )
{
    sort( things, things + thingNum, cmpPi );
    select( things );
    cout<<"按效益值\n" ;
    show( things, thingNum );
}
void weight( name *things )
{
    sort( things, things + thingNum, cmpWi );
    cout<< "\n" ;
    select( things );
    cout<< "按重量\n" ;
    show( things, thingNum );
}
void speVal( name *things )
{
    sort( things, things + thingNum, cmpXi );
    cout << endl;
    select( things );
    cout<< "按比值\n"  ;
    show( things, thingNum );
}
int main( ) {
    cout<< "请输入背包容量，物品数目：\n" ;
    cin>>bag>>thingNum ;
    cout<< "输入每个物品的效益和重量：\n" ;
    name things[thingNum];
    for( int i = 0; i < thingNum; i++ ) {
        cin>>things[i].pi>>things[i].wi ;
        things[i].i = i;
        things[i].xi = 1.0 * things[i].pi / things[i].wi;
        things[i].flag = things[i].floflag = 0;           //初始化
    }
    value( things );                                       //按效益值
    weight( things );                                        //按重量值
    speVal( things );                                        // 按比值
    return 0;
}

