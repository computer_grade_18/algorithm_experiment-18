#include<iostream>
using namespace std;
const int MAX = 100;
int main(){
    int N;
    int pokers[MAX];
    cin>>N;
    int total = 0;
    for (int i=0; i<N; i++) {
        cin>>pokers[i];
        total+=pokers[i];
    }
    int avg = total/N,times=0;//avg记录每堆纸牌目标，times记录移动的次数 
    for(int i=0;i<N;i++){
        if(pokers[i]!=avg){		//当该纸牌堆中纸牌数目不等于目标数量时 
            pokers[i+1] -= avg - pokers[i];//avg - pokers[i]小于0，用下一堆纸牌弥补
										  //avg - pokers[i]大于0，将多出的纸牌给下一组 
            times++;
        }
    }
    cout<<times<<endl; 
}
