#include<iostream>
#include<algorithm>
using namespace std;
const int MAX=100;
const int MAX_length=10000;

int n,m;
int G[MAX][MAX];//相邻两边的长
int d[MAX];
bool V[MAX]={false};

int prim(){
	int sum=0;
	V[1]=true;
	d[1]=0;
	for(int i=1; i<=n; i++)d[i]=G[1][i];
	for(int i=1;i<n;i++){//n-1条边 
		int index=0,MIN=MAX_length;
		for(int j=1;j<=n;j++){//每次从头开始搜索最短路径 
			if(V[j]==false && d[j]<MIN){//MIN不断改变求出当前最小值 
				index=j;
				MIN=d[j];
			}
		}
		V[index]=true;//true表示该点已被用 
		sum+=MIN;
		for(int v=1;v<=n;v++){
			if(V[v]==false &&  G[index][v]<d[v]){
				d[v]=G[index][v];//进行更新 ，如果刚刚更新的点连接的边更短，用更短的边 替换原来的边 
			}
		}
	}
	return sum;
}
int main()
{
    int u,v,w;
    cout<<"输入个数和边数：";
    cin>>n>>m;
    fill(G[0],G[0]+MAX*MAX,MAX_length);//把所有空间的值置为MAX 
    cout<<"输入两个点和边长："<<endl;
    for(int i=1;i<=m;i++){//把每条边长输入数组 ，有边则覆盖之前的MAX 
    	cin>>u>>v>>w;
    	G[u][v]=G[v][u]=w;
	}
	int sum=prim();
	cout<<"贪心求最小生成树值为："<<sum;
    
}


