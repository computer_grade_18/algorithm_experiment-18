function cal() {
	var weights = [];
	var values = [];
	for (var i = 1; i <= 5; i++) {
		weights.push(parseInt(document.getElementById("item" + i + "_weight").value));
		values.push(parseInt(document.getElementById("item" + i + "_value").value));
	}
	W = parseInt(document.getElementById("pack_weight").value)
	var n = weights.length;
	var f = new Array(n)
	f[-1] = new Array(W + 1).fill(0)
	var selected = [];
	for (var i = 0; i < n; i++) { //注意边界，没有等号
		f[i] = [] //创建当前的二维数组
		for (var j = 0; j <= W; j++) { //注意边界，有等号
			if (j < weights[i]) { //注意边界， 没有等号
				f[i][j] = f[i - 1][j] //case 1
			} else {
				f[i][j] = Math.max(f[i - 1][j], f[i - 1][j - weights[i]] + values[i]); //case 2
			}
		}
	}
	var j = W,
		w = 0
	for (var i = n - 1; i >= 0; i--) {
		if (f[i][j] > f[i - 1][j]) {
			selected.push(i+1)
			//console.log("物品", i+1, "其重量为", weights[i], "其价格为", values[i])
			j = j - weights[i];
			w += weights[i]
		}
	}
	var res = document.getElementById("result")
	res.style.display = "";
	res.innerHTML = "选择的物品有:<br/>"
	for(var i =0;i<selected.length;i++){
		res.innerHTML = res.innerHTML+"物品"+selected[i]+"&nbsp&nbsp&nbsp"
	}
	res.innerHTML = res.innerHTML+"<p>背包最大承重为"+ W+" 现在重量为"+w+" 最大价值为"+f[n - 1][W]+"</p>";
}
